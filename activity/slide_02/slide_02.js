(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:



(lib.Image = function() {
	this.initialize(img.Image);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,143,144);


(lib.Image_1 = function() {
	this.initialize(img.Image_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,57,242);


(lib.Image_2 = function() {
	this.initialize(img.Image_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,194,202);


(lib.Image_3 = function() {
	this.initialize(img.Image_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,64,121);


(lib.Image_4 = function() {
	this.initialize(img.Image_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,74,284);


(lib.Image_5 = function() {
	this.initialize(img.Image_5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,84,223);


(lib.Image_6 = function() {
	this.initialize(img.Image_6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,102,250);


(lib.Image_7 = function() {
	this.initialize(img.Image_7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,353,1056);


(lib.Image_8 = function() {
	this.initialize(img.Image_8);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,512,512);


(lib.Image_9 = function() {
	this.initialize(img.Image_9);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,387,267);


(lib.Image_10 = function() {
	this.initialize(img.Image_10);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,881,279);


(lib.Image_11 = function() {
	this.initialize(img.Image_11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,170,346);


(lib.Image_12 = function() {
	this.initialize(img.Image_12);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1011,643);


(lib.Image_0 = function() {
	this.initialize(img.Image_0);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,84,67);


(lib.Image_1_1 = function() {
	this.initialize(img.Image_1_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,146,152);


(lib.Image_1_2 = function() {
	this.initialize(img.Image_1_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,66,56);


(lib.Image_1_3 = function() {
	this.initialize(img.Image_1_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,86,88);


(lib.Image_1_4 = function() {
	this.initialize(img.Image_1_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,75,97);


(lib.Image_1_5 = function() {
	this.initialize(img.Image_1_5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,446,322);


(lib.Image_1_6 = function() {
	this.initialize(img.Image_1_6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,217,111);


(lib.Image_2_1 = function() {
	this.initialize(img.Image_2_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,191,313);


(lib.Image_2_2 = function() {
	this.initialize(img.Image_2_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,104,225);


(lib.Image_2_3 = function() {
	this.initialize(img.Image_2_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,123,295);


(lib.Image_2_4 = function() {
	this.initialize(img.Image_2_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,876,1059);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.r3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Image_1_3();
	this.instance.parent = this;
	this.instance.setTransform(-20.6,-21.1,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-20.6,-21.1,41.3,42.3);


(lib.r2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Image_4();
	this.instance.parent = this;
	this.instance.setTransform(-17.7,-68.1,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-17.7,-68.1,35.5,136.3);


(lib.r1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Image_2_2();
	this.instance.parent = this;
	this.instance.setTransform(-24.9,-54,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.9,-54,49.9,108);


(lib.pur2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#92327A").s().p("AmAEIIFSn3QARgYAdAAQAeAAARAYIFSH3g");
	this.shape.setTransform(0,61.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#131313").ss(0.8).p("AAAJvIAAzd");
	this.shape_1.setTransform(0,-25.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.4,-88.7,77,176.4);


(lib.pre = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgTAuQgJgDgFgFIAEgGIABgCIADgBIAEABIAEADIAHADQAEACAGAAQAEAAADgCIAGgDIAEgFIABgGQAAgEgCgDQgCgCgEgCIgIgEIgIgCIgJgEIgIgEQgEgDgCgEQgCgEAAgHQAAgFADgFQACgFAEgEQAFgEAGgCQAGgDAHAAQAKAAAHADQAIADAFAGIgDAGQAAAAgBABQAAAAgBAAQAAABgBAAQAAAAgBAAIgDgBIgEgCIgHgDIgIgBIgHABIgGADQgCACgBADQgCACAAADQAAAEACACIAGAEIAHAEIAJADIAJADQAFACADACIAGAHQACAEAAAGQAAAGgCAGQgDAGgEAEQgFAEgHACQgGADgIAAQgKAAgIgEg");
	this.shape.setTransform(32.3,1.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgVAuQgFgCgEgFQgFgFgCgGQgCgHAAgJIAAg8IARAAIAAA8QAAAMAFAFQAFAHAKAAQAHgBAGgDQAHgDAFgHIAAhGIARAAIAABfIgKAAQgEAAgBgDIgBgKQgGAGgIAFQgIAEgIAAQgJAAgGgDg");
	this.shape_1.setTransform(22.9,1.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgSAuQgJgDgFgHQgGgGgEgJQgDgKAAgLQAAgKADgJQAEgKAGgGQAFgGAJgEQAJgEAJAAQALAAAIAEQAJAEAGAGQAFAGAEAKQADAJAAAKQAAALgDAKQgEAJgFAGQgGAHgJADQgIAEgLAAQgJAAgJgEgAgLghQgGADgDAEQgEAFgCAHQgBAHAAAHQAAAJABAGQACAHAEAFQADAEAGADQAFACAGAAQAOAAAHgJQAHgKAAgRQAAgQgHgKQgHgJgOAAQgGAAgFACg");
	this.shape_2.setTransform(12.4,1.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgIBFIAAhfIAQAAIAABfgAgEgtIgEgDIgCgEIgBgEIABgFIACgEIAEgCIAEgBIAFABIADACIADAEIABAFIgBAEIgDAEIgDADIgFABIgEgBg");
	this.shape_3.setTransform(4.8,-0.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgGAwIgohfIAOAAIADABIADACIAXA9IADAHIAAAHIACgHIACgHIAZg9IABgDIAEAAIANAAIgoBfg");
	this.shape_4.setTransform(-2.6,1.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgOAuQgJgDgFgHQgHgGgDgKQgDgJgBgMQAAgKAEgJQACgIAHgHQAGgGAHgEQAJgEAJAAQAJAAAHADQAHADAGAGQAFAFADAIQAEAIAAALQAAAEgBABQgBAAAAAAQAAAAgBAAQAAAAgBAAQAAAAgBAAIg/AAQABAJACAHQACAHAEAEQAEAFAFACQAGACAGAAQAGAAAFgBIAIgDIAFgEIAEgBQAAAAABAAQAAAAABABQAAAAABAAQAAAAAAABIAFAGQgDAEgEACQgFADgEACIgLADIgLABQgIAAgJgEgAgQgdQgGAHgCANIAzAAQAAgGgBgFQgCgFgDgEQgDgDgFgCQgEgCgGAAQgLAAgIAHg");
	this.shape_5.setTransform(-12.2,1.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgcAxIAAhfIAJAAIAEAAIACAEIABAPQAFgKAHgGQAGgFAKAAIAHABIAGACIgCANQAAABgBAAQAAABAAAAQAAAAgBAAQAAABgBAAIgEgBIgIgBQgJAAgFAEQgFAGgEAKIAAA8g");
	this.shape_6.setTransform(-20.5,1.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgtBEIAAiHIApAAQAMAAAJADQAJADAHAGQAGAFADAHQAEAJgBAJQABAKgEAHQgEAIgFAFQgHAGgKADQgJADgLAAIgXAAIAAAzgAgbACIAXAAQAHAAAFgCQAHgBADgEQAFgDACgGQACgFAAgGQAAgNgIgHQgIgIgPAAIgXAAg");
	this.shape_7.setTransform(-30,-0.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f().s("#FFFFFF").ss(0.8).p("AIhCuIxBAAQgTAAgOgOQgOgOAAgTIAAj9QAAgUAOgNQAOgOATAAIRBAAQATAAAOAOQAOANAAAUIAAD9QAAATgOAOQgOAOgTAAg");

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#6D165B").s().p("AofCuQgUAAgOgOQgOgOAAgTIAAj9QAAgUAOgNQAOgOAUAAIRAAAQATAAAOAOQAOANAAAUIAAD9QAAATgOAOQgOAOgTAAg");

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#380030").s().p("AojDIQgTAAgOgOQgOgNAAgUIAAkwQAAgVAOgNQAOgOATAAIRHAAQAUAAAOAOQANANAAAVIAAEwQAAAUgNANQgOAOgUAAg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-60.2,-20,120.4,40);


(lib.or1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F1701F").s().p("AmAEIIFSn3QARgYAdAAQAeAAARAYIFSH3g");
	this.shape.setTransform(0,85.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#131313").ss(0.8).p("AAANxIAA7h");
	this.shape_1.setTransform(0,-24.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.5,-113.3,77,225.6);


(lib.msg = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgnBDIAAiDIANAAQAEAAABACQACABAAADIACAVQAHgOAKgIQAJgIANAAQAGABAEABQAFABADADIgDARQAAABAAAAQgBABAAAAQgBABAAAAQgBAAgBAAIgFgBQgEgBgHAAQgMAAgHAGQgIAIgGANIAABTg");
	this.shape.setTransform(72.7,-55.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgTA/QgMgEgIgJQgJgJgFgNQgEgOAAgQQgBgOAFgMQAEgMAIgIQAIgJAMgFQALgFANAAQAMAAALAEQAKAEAHAHQAHAIAEALQAEALABAOIgCAHQAAABAAAAQgBAAAAAAQgBAAgBAAQAAAAgBAAIhYAAQABANADAJQADAJAFAGQAGAHAIADQAIADAIAAQAJAAAGgCIALgFIAIgEQACgCADAAQABAAAAAAQABABAAAAQABAAAAAAQABABAAAAIAGAJQgDAFgHAEQgGAEgGACIgPAEIgOABQgNAAgLgFgAgVgpQgKAKgDASIBHAAQAAgJgBgGQgDgHgEgFQgFgFgGgDQgGgCgIAAQgQAAgJAJg");
	this.shape_1.setTransform(59.9,-55.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("ABDBDIAAhTQAAgQgHgIQgHgIgNAAQgGAAgFACQgFACgEAEQgEAEgCAGQgCAGAAAIIAABTIgXAAIAAhTQAAgQgGgIQgHgIgMAAQgIAAgIAFQgHAFgGAIIAABhIgYAAIAAiDIAOAAQAFAAABAFIACANQAIgJAJgGQAJgFAMAAQANAAAIAHQAIAIADAMQADgHAFgFQAEgFAGgEQAFgDAHgCIANgBQAKAAAIADQAIADAGAHQAGAGADAKQADAJAAAMIAABTg");
	this.shape_2.setTransform(42.5,-55.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgZA/QgMgFgIgIQgIgJgEgNQgFgMAAgQQAAgPAFgMQAEgNAIgJQAIgJAMgEQALgFAOAAQAPAAALAFQAMAEAIAJQAIAJAFANQAEAMAAAPQAAAQgEAMQgFANgIAJQgIAIgMAFQgLAFgPAAQgOAAgLgFgAgQguQgIAEgEAGQgFAHgDAJQgCAJAAALQAAAMACAJQADAJAFAHQAEAGAIADQAHAEAJAAQAUAAAJgNQAKgNAAgYQAAgXgKgNQgJgNgUAAQgJAAgHADg");
	this.shape_3.setTransform(24.4,-55.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgMBQQgJgJAAgQIAAhQIgQAAQgBAAAAAAQAAAAgBgBQAAAAgBAAQAAAAgBgBQAAAAAAAAQAAgBgBAAQAAgBAAAAQAAgBAAAAIAAgJIAWgDIAFgpQAAgBABAAQAAgBAAAAQAAgBAAAAQABAAAAgBIAEgBIAKAAIAAAuIAmAAIAAARIgmAAIAABOQAAAJAFAEQAEAEAGAAIAGgBIAFgDIADgCIADgBQAAAAAAAAQABABAAAAQABAAAAABQABAAAAABIAHALQgGAFgJAEQgIADgKAAQgOAAgIgJg");
	this.shape_4.setTransform(12.2,-57.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgbBAQgLgFgIgHIAFgJIADgCIAEgBIAFACIAGAEIAJAEQAFACAIAAQAGAAAFgCIAJgFIAFgHQABgEAAgEQAAgGgDgDQgDgEgEgCQgFgDgHgCIgLgEIgNgFQgGgBgFgEQgFgEgDgGQgDgGAAgJQAAgHAEgHQADgHAGgGQAGgFAIgDQAJgDAKAAQAOAAAKAEQAKAEAIAHIgFAJQgCACgDAAIgFgBIgGgDIgIgDQgFgCgHAAQgFAAgFACIgIAEIgFAGQgCADAAAEQAAAFADAEQADADAFADIAKAEIANAEIAMAFQAHACAEADQAFAEADAGQADAGAAAIQAAAJgDAIQgDAHgHAGQgGAGgJADQgKADgLAAQgOAAgLgEg");
	this.shape_5.setTransform(1.5,-55.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgdBAQgIgEgGgGQgGgHgCgJQgDgKAAgLIAAhTIAXAAIAABTQAAAPAHAIQAHAJAOAAQAJAAAJgFQAKgFAHgIIAAhhIAXAAIAACDIgNAAQgFAAgCgFIgCgOQgIAKgLAGQgLAFgNAAQgLAAgIgDg");
	this.shape_6.setTransform(-11.4,-55.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgYBZQgRgHgLgNQgNgMgGgSQgGgSAAgVQgBgUAIgSQAGgSANgMQANgNAQgHQASgHAUAAQAUAAAPAHQAPAGAMALIgIAMIgCACIgDABQgBAAAAAAQgBAAAAgBQgBAAAAAAQgBAAAAgBIgEgDIgHgEIgIgEIgLgDIgQgBQgOAAgMAFQgMAFgJAKQgJAKgGANQgEAOAAAQQAAASAEANQAGAOAJAKQAIAJAMAFQAMAFANAAIAPgBQAGgBAGgCIAKgFIAKgIQABgBABAAQAAAAABgBQAAAAABAAQAAAAABAAQAAAAABAAQAAAAABAAQAAABABAAQAAAAAAABIAKALQgLANgRAHQgQAIgXAAQgTAAgQgHg");
	this.shape_7.setTransform(-27.4,-58.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgnBDIAAiDIANAAQAEAAABACQACABAAADIACAVQAHgOAKgIQAJgIANAAQAGABAEABQAFABADADIgDARQAAADgEAAIgFgBQgEgBgHAAQgMAAgHAGQgIAIgGANIAABTg");
	this.shape_8.setTransform(-46.1,-55.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgdBCQgGgCgFgEQgFgEgDgHQgDgHAAgJQAAgHAEgIQAEgHAKgGQAKgFAPgDQAPgDAWAAIAAgLQAAgPgGgJQgHgHgNgBQgHAAgGACIgKAFIgIAGQgDACgDAAQAAAAgBgBQgBAAAAAAQAAAAgBAAQAAgBgBAAIgDgDIgEgIQALgKANgFQANgFAOgBQALABAJADQAIAEAGAHQAGAGADAJQADAKAAALIAABTIgKAAIgGgBQgCgBAAgDIgDgMIgKAIQgFAEgGADIgKADIgOABQgIAAgHgCgAACAJQgKACgHADQgIAEgDAEQgDAFAAAFQAAAGACADIAEAGQADADAEABIAJABIAKgBQAFgBAFgDQAEgCAEgDIAIgHIAAgcQgQAAgLACg");
	this.shape_9.setTransform(-58.9,-55.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgUA/QgLgEgIgJQgJgJgFgNQgEgOAAgQQgBgOAFgMQAEgMAIgIQAIgJAMgFQALgFAOAAQALAAAKAEQALAEAHAHQAHAIAEALQAFALAAAOIgBAHQgBABAAAAQgBAAAAAAQgBAAgBAAQAAAAgBAAIhYAAQABANADAJQADAJAGAGQAFAHAIADQAHADAJAAQAIAAAHgCIALgFIAIgEQADgCACAAQABAAAAAAQABABAAAAQABAAAAAAQABABAAAAIAHAJQgFAFgGAEQgFAEgIACIgOAEIgOABQgNAAgMgFgAgVgpQgKAKgDASIBHAAQAAgJgCgGQgCgHgEgFQgFgFgGgDQgGgCgIAAQgQAAgJAJg");
	this.shape_10.setTransform(-72.2,-55.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AhQBeIAAi7IBGAAQAUAAARAHQARAHAMAMQAMAMAHASQAGARAAAUQAAAVgGARQgHASgMAMQgMAMgRAHQgRAHgUAAgAg2BJIAsAAQAOAAAMgFQANgFAIgJQAJgJAFgOQAEgOAAgRQAAgQgEgOQgFgNgJgKQgIgJgNgFQgMgFgOAAIgsAAg");
	this.shape_11.setTransform(-88.3,-58.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgYA/QgLgFgHgHIACgFIACgBIADAAIADABIAHAFIAKAFQAGACAIAAQAIAAAGgDQAGgBAEgFQAFgEACgFQACgFAAgGQAAgGgDgFQgDgEgFgDQgFgDgHgCIgMgFIgNgDQgHgDgFgDQgFgEgEgFQgDgGAAgIQAAgHADgGQADgHAGgEQAFgFAJgDQAHgDAKAAQAMAAAIAEQAKACAHAIIgCAEQAAAAgBABQAAAAAAAAQgBABAAAAQgBAAAAAAIgEgCIgGgEIgJgDQgGgBgHAAQgHAAgFACQgGABgEADQgEAEgCAEQgCAFgBAFQABAGADAEQACAEAGADIAMAFIALAEIAOAFIALAFQAGAEACAFQADAGAAAIQAAAJgDAHQgCAHgHAFQgFAGgIADQgJADgKAAQgNAAgKgEg");
	this.shape_12.setTransform(82.5,13);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgJBSQgIgIAAgPIAAhYIgSAAIgCgBIgBgCIAAgFIAWgBIADgvIABgCIACgBIAGAAIAAAyIAoAAIAAAJIgoAAIAABXQAAAGACADQABAEABADQADACADABIAHACQAFAAAEgCIAFgDIAEgDIACgBIACABIAEAGQgFAFgIADQgHAEgJAAQgMAAgHgHg");
	this.shape_13.setTransform(72.3,10.8);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AAnBCIAAhSQAAgSgJgLQgIgKgRAAQgMAAgLAGQgLAHgJALIAABhIgMAAIAAiBIAHAAQADAAABADIABATQAJgLAMgHQALgGAOAAQALAAAIAEQAIADAFAGQAGAHACAJQADAJAAALIAABSg");
	this.shape_14.setTransform(60.7,12.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgFBdIAAiBIALAAIAACBgAgEhGIgDgDIgDgDIgBgFIABgEIADgEQAAAAABgBQAAAAAAAAQABgBAAAAQABAAAAAAIAEgBIAEABQABAAAAAAQABAAAAABQABAAAAAAQAAABABAAIADAEIABAEIgBAFIgDADIgEADIgEABIgEgBg");
	this.shape_15.setTransform(50.4,10.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgYA+QgLgFgIgIQgHgJgEgMQgEgNAAgPQAAgOAEgMQAEgNAHgJQAIgIALgFQALgFANAAQAOAAALAFQALAFAIAIQAHAJAEANQAEAMAAAOQAAAPgEANQgEAMgHAJQgIAIgLAFQgLAFgOAAQgNAAgLgFgAgTg0QgJAEgGAIQgGAHgDALQgDAKAAAMQAAANADALQADAKAGAIQAGAHAJAEQAIAEALAAQAMAAAIgEQAJgEAGgHQAGgIADgKQACgLAAgNQAAgMgCgKQgDgLgGgHQgGgIgJgEQgIgEgMAAQgLAAgIAEg");
	this.shape_16.setTransform(40.2,13);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("Ag3BdIAAi5IAvAAQAgAAAQAPQAQAOAAAaQAAAMgEALQgFAKgIAHQgIAHgMAEQgNAEgOAAIgjAAIAABLgAgrAHIAjAAQALAAAKgDQAJgEAHgEQAHgHAEgIQAEgIAAgKQAAgVgOgLQgNgMgZAAIgjAAg");
	this.shape_17.setTransform(26.9,10.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AglBCIAAiBIAHAAIADABQABAAAAABQAAAAAAAAQAAABAAAAQAAABAAAAIACAcQAGgQAKgJQAKgJAPAAQAGAAAFABQAFABAFADIgDAIQAAABAAAAQAAABAAAAQgBAAAAABQAAAAgBAAIgCgBIgEgBIgFgBIgIgBQgOAAgJAJQgJAJgGASIAABTg");
	this.shape_18.setTransform(8.5,12.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgYBBQgHgCgEgEQgGgEgCgGQgEgGAAgJQABgIAFgIQAEgHAKgFQAKgFAPgCQAQgDAWgBIAAgNQAAgTgIgJQgIgKgQAAQgIAAgHADIgLAFIgHAGQgDADgCAAQgBAAAAAAQgBgBAAAAQgBAAAAgBQgBAAAAgBIgCgDQALgLAKgFQAMgFANAAQALAAAHADQAIAEAGAGQAEAGADAJQACAJABALIAABTIgGAAQgEAAAAgDIgCgRIgLAJQgFAFgGACQgFADgHACQgFABgJAAQgGAAgGgCgAADAFQgMADgIADQgJAEgFAGQgDAFAAAHQAAAHACAEQACAFAEADQADADAEACIALABQAHAAAFgBIAMgFIAKgHIAKgJIAAghQgTAAgOACg");
	this.shape_19.setTransform(-3.9,13);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgJBSQgHgIgBgPIAAhYIgSAAIgCgBIgBgCIAAgFIAWgBIADgvIABgCIADgBIAFAAIAAAyIAoAAIAAAJIgoAAIAABXQAAAGACADQABAEABADQADACADABIAHACQAFAAADgCIAGgDIAEgDIADgBIABABIAEAGQgFAFgIADQgHAEgJAAQgMAAgHgHg");
	this.shape_20.setTransform(-14.8,10.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AghBYQgNgHgKgLIADgFQABgBAAAAQABgBAAAAQABAAAAAAQABgBABAAIACACIAFAEIAGAFQADADAFACIAMAEQAGACAIAAQAKAAAIgEQAJgDAGgGQAGgGADgHQADgIAAgJQAAgKgDgHQgEgGgHgEQgHgEgIgEIgQgFIgRgFQgIgDgHgGQgGgFgFgIQgEgIAAgMQABgJADgJQADgJAIgGQAGgGAKgFQAKgDAMAAQAOAAAMAEQALAFAJAJIgCAGQgBABAAAAQgBABAAAAQgBAAAAAAQAAABgBAAIgEgDIgIgFQgEgDgHgDQgHgCgKgBQgJAAgIADQgIAEgEAEQgGAGgCAGQgDAHAAAGQAAAKAEAGQAEAGAGAEQAHAFAJADIAQAFIAQAHQAIADAHAEQAHAGAEAIQAEAHAAAMQAAALgEAKQgEALgIAHQgHAIgLAEQgMAEgNABQgRgBgOgGg");
	this.shape_21.setTransform(-26.2,10.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgZBYQgMgFgJgMQgJgLgFgSQgFgSAAgYQAAgXAFgSQAFgSAJgMQAJgLAMgGQAMgFANAAQAOAAAMAFQAMAGAJALQAJAMAFASQAFASAAAXQAAAYgFASQgFASgJALQgJAMgMAFQgMAHgOAAQgNAAgMgHgAgThOQgKAEgHAKQgIALgEAPQgEAQAAAWQAAAWAEARQAEAQAIAKQAHAJAKAFQAJAFAKAAQALAAAJgFQAKgFAIgJQAHgKAEgQQAEgRAAgWQAAgWgEgQQgEgPgHgLQgIgKgKgEQgJgFgLAAQgKAAgJAFg");
	this.shape_22.setTransform(-45.7,10.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgZBYQgMgFgJgMQgJgLgFgSQgFgSAAgYQAAgXAFgSQAFgSAJgMQAJgLAMgGQAMgFANAAQAOAAAMAFQAMAGAJALQAJAMAFASQAFASAAAXQAAAYgFASQgFASgJALQgJAMgMAFQgMAHgOAAQgNAAgMgHgAgThOQgKAEgHAKQgIALgEAPQgEAQAAAWQAAAWAEARQAEAQAIAKQAHAJAKAFQAJAFAKAAQALAAAJgFQAKgFAIgJQAHgKAEgQQAEgRAAgWQAAgWgEgQQgEgPgHgLQgIgKgKgEQgJgFgLAAQgKAAgJAFg");
	this.shape_23.setTransform(-60.8,10.3);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgZBYQgMgFgJgMQgJgLgFgSQgFgSAAgYQAAgXAFgSQAFgSAJgMQAJgLAMgGQAMgFANAAQAOAAAMAFQAMAGAJALQAJAMAFASQAFASAAAXQAAAYgFASQgFASgJALQgJAMgMAFQgMAHgOAAQgNAAgMgHgAgThOQgKAEgHAKQgIALgEAPQgEAQAAAWQAAAWAEARQAEAQAIAKQAHAJAKAFQAJAFAKAAQALAAAJgFQAKgFAIgJQAHgKAEgQQAEgRAAgWQAAgWgEgQQgEgPgHgLQgIgKgKgEQgJgFgLAAQgKAAgJAFg");
	this.shape_24.setTransform(-76,10.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("Ag7BeIAAgFIABgDIACgCIA+hBIAOgPIAMgOQAFgHADgJQADgHAAgKQAAgJgEgIQgDgIgGgFQgGgEgHgDQgHgCgJAAQgHAAgIACQgHAEgFAEQgHAFgDAGQgEAHgCAIQgBAFgDAAIgBAAIgBAAIgGgCQABgLAFgKQAGgJAHgHQAIgGAJgDQAKgDAKAAQALAAAJACQAKADAHAHQAIAGAEAJQAEAKAAAMQAAALgDAJQgDAIgFAJQgGAHgIAIIgOAQIg6A7IAJgCIAJAAIBPAAIADABIABADIAAAIg");
	this.shape_25.setTransform(-91,10.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgoBQQgOgRAAghQABgOADgNQAEgLAHgJQAHgJALgGQALgFAMAAQAOAAAJAFQAKAFAHAJIAAhNIAMAAIAAC+IgHAAQgDAAAAgEIgBgUQgKALgLAHQgMAHgNAAQgYAAgNgQgAgPgXQgIAFgGAHQgGAIgDAKQgDAKAAANQAAAcALAOQALAOATAAQAMAAALgGQALgHAIgMIAAhEQgIgLgJgFQgJgEgLAAQgLAAgJAEg");
	this.shape_26.setTransform(93.3,-26.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgTA+QgKgEgJgJQgHgJgEgNQgFgNAAgPQAAgOAEgMQAEgMAHgJQAIgIALgFQALgFANAAQALAAAJAEQAJAEAIAHQAGAHAEALQAEAKAAAOIgBAEIgDABIhcAAIAAACQgBAOAEALQADALAGAIQAHAHAJAEQAIAEAKAAQAKAAAIgCQAGgCAFgDIAIgFQABAAABgBQAAAAABgBQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQAAAAABABQAAAAAAAAIADAEQgDAEgFAEQgFADgGADIgOAEIgOABQgNAAgLgFgAgNg1QgIADgGAGQgFAFgFAJQgDAIgBAKIBUAAQABgKgDgJQgDgIgFgGQgFgFgIgEQgHgDgJAAQgJAAgIAEg");
	this.shape_27.setTransform(80,-23.6);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgFBBIg2iBIAJAAIADABIACACIArBlIACAGIAAAGIABgGIACgGIArhlIACgCIADgBIAJAAIg3CBg");
	this.shape_28.setTransform(67.2,-23.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgFBdIAAiBIALAAIAACBgAgEhGIgDgDIgDgDIgBgFIABgEIADgEQAAAAABgBQAAAAAAAAQABgBAAAAQABAAAAAAIAEgBIAEABQABAAAAAAQABAAAAABQABAAAAAAQAAABABAAIADAEIABAEIgBAFIgDADIgEADIgEABIgEgBg");
	this.shape_29.setTransform(57.8,-26.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgTA+QgKgEgJgJQgIgJgDgNQgFgNAAgPQAAgOAEgMQAEgMAIgJQAHgIALgFQALgFANAAQAKAAAKAEQAJAEAIAHQAGAHAEALQAEAKAAAOIgBAEIgDABIhcAAIAAACQAAAOADALQADALAGAIQAHAHAJAEQAIAEALAAQAKAAAHgCQAGgCAFgDIAJgFQAAAAABgBQAAAAABgBQAAAAABAAQAAAAAAAAQABAAAAAAQABAAAAAAQAAAAABABQAAAAAAAAIADAEQgDAEgFAEQgFADgGADIgOAEIgOABQgNAAgLgFgAgNg1QgIADgGAGQgGAFgEAJQgDAIgBAKIBUAAQABgKgDgJQgDgIgFgGQgFgFgIgEQgHgDgJAAQgJAAgIAEg");
	this.shape_30.setTransform(48,-23.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgQA+QgKgEgIgJQgHgIgEgNQgEgMAAgQQAAgOAEgMQAEgMAHgJQAIgJAKgFQALgFANAAQANAAAKAEQAKAEAHAHIgDAFIgBABIgCAAIgEgCIgGgDIgJgEQgGgCgIAAQgKAAgJAEQgIAEgGAHQgGAIgDAKQgEALAAAMQAAAOAEAKQADALAGAHQAGAIAIADQAIAEAJAAQAKAAAGgCIALgFIAGgFQABAAAAgBQABAAAAgBQABAAAAAAQABAAAAAAQABAAAAAAQAAAAABAAQAAAAAAABQAAAAABAAIADAEIgHAIIgLAGIgMAEIgOABQgMAAgKgFg");
	this.shape_31.setTransform(35.6,-23.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgTA+QgLgEgIgJQgIgJgEgNQgEgNAAgPQAAgOAEgMQAEgMAHgJQAIgIALgFQALgFAMAAQALAAAKAEQAKAEAGAHQAIAHADALQAEAKAAAOIgBAEIgDABIhdAAIAAACQABAOADALQADALAGAIQAHAHAIAEQAKAEAJAAQALAAAGgCQAHgCAGgDIAHgFQABAAABgBQAAAAABgBQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQAAAAABABQAAAAAAAAIAEAEQgDAEgGAEQgFADgHADIgNAEIgPABQgMAAgLgFgAgNg1QgIADgGAGQgFAFgEAJQgEAIgBAKIBVAAQAAgKgDgJQgDgIgGgGQgEgFgIgEQgHgDgJAAQgJAAgIAEg");
	this.shape_32.setTransform(22.7,-23.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AglBCIAAiBIAHAAIADABQABAAAAABQAAAAAAAAQAAABAAAAQAAABAAAAIABAcQAHgQAKgJQAKgJAPAAQAGAAAFABQAFABAEADIgCAIQAAABAAAAQAAABAAAAQgBAAAAABQgBAAAAAAIgCgBIgEgBIgFgBIgHgBQgPAAgJAJQgJAJgGASIAABTg");
	this.shape_33.setTransform(11.5,-23.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgTA+QgLgEgIgJQgIgJgEgNQgEgNAAgPQAAgOAEgMQAEgMAHgJQAIgIALgFQALgFAMAAQALAAAKAEQAKAEAGAHQAIAHADALQAEAKAAAOIgBAEIgDABIhdAAIAAACQABAOADALQADALAGAIQAHAHAIAEQAKAEAJAAQALAAAGgCQAHgCAGgDIAHgFQABAAABgBQAAAAABgBQAAAAABAAQAAAAABAAQAAAAAAAAQABAAAAAAQAAAAABABQAAAAAAAAIAEAEQgDAEgGAEQgFADgHADIgNAEIgPABQgMAAgLgFgAgNg1QgIADgGAGQgFAFgEAJQgEAIgBAKIBVAAQAAgKgDgJQgDgIgGgGQgEgFgIgEQgHgDgJAAQgJAAgIAEg");
	this.shape_34.setTransform(-6.1,-23.6);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgFBBIg2iBIAJAAIADABIACACIArBlIACAGIAAAGIABgGIACgGIArhlIACgCIADgBIAJAAIg3CBg");
	this.shape_35.setTransform(-18.9,-23.5);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgZBBQgGgCgFgEQgEgEgEgGQgCgGAAgJQAAgIAEgIQAFgHAKgFQAKgFAQgCQAOgDAWgBIAAgNQAAgTgHgJQgIgKgPAAQgJAAgGADIgLAFIgIAGQgDADgCAAQgBAAAAAAQgBgBAAAAQgBAAAAgBQgBAAAAgBIgCgDQALgLALgFQALgFANAAQAKAAAJADQAHAEAFAGQAFAGADAJQACAJAAALIAABTIgEAAQgFAAAAgDIgCgRIgKAJQgGAFgFACQgHADgGACQgGABgHAAQgHAAgHgCgAADAFQgMADgJADQgIAEgEAGQgFAFAAAHQAAAHACAEQADAFADADQAEADAFACIAJABQAIAAAGgBIALgFIAKgHIAJgJIAAghQgSAAgOACg");
	this.shape_36.setTransform(-31.4,-23.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AAnBfIAAhTQAAgRgJgLQgIgKgRAAQgMAAgLAHQgLAGgJAMIAABgIgMAAIAAi9IAMAAIAABSQAJgLAMgGQAMgHANAAQALAAAIAEQAIADAFAGQAGAHACAJQADAIAAALIAABTg");
	this.shape_37.setTransform(-44.5,-26.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgaA/QgIgDgGgHQgFgGgDgKQgCgJAAgLIAAhSIAMAAIAABSQAAATAJAJQAIALARAAQAMAAALgHQALgGAJgMIAAhgIAMAAIAACBIgHAAQgEAAAAgDIgBgTQgJAKgMAIQgMAGgNAAQgLAAgIgDg");
	this.shape_38.setTransform(-64,-23.4);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgYA+QgLgFgIgIQgHgJgEgMQgEgNAAgPQAAgOAEgMQAEgNAHgJQAIgIALgFQALgFANAAQAOAAALAFQALAFAIAIQAHAJAEANQAEAMAAAOQAAAPgEANQgEAMgHAJQgIAIgLAFQgLAFgOAAQgNAAgLgFgAgTg0QgJAEgGAIQgGAHgDALQgDAKAAAMQAAANADALQADAKAGAIQAGAHAJAEQAIAEALAAQAMAAAIgEQAJgEAGgHQAGgIADgKQACgLAAgNQAAgMgCgKQgDgLgGgHQgGgIgJgEQgIgEgMAAQgLAAgIAEg");
	this.shape_39.setTransform(-78,-23.6);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgGBdIAAhNIhEhsIAMAAIADABIACADIA1BVIADAEIABAGIACgGIAEgEIA0hVIACgDIADgBIAMAAIhEBsIAABNg");
	this.shape_40.setTransform(-90.9,-26.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#1A237E").s().p("Ag1kiIDFBHIkfH+g");
	this.shape_41.setTransform(-96.3,59.8);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#1A237E").s().p("A0CKAIAAz/MAoFAAAIAAT/g");
	this.shape_42.setTransform(0,-24.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-128.3,-88.8,256.6,177.8);


(lib.l3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Image_1_4();
	this.instance.parent = this;
	this.instance.setTransform(-18,-23.2,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-18,-23.2,36,46.6);


(lib.l2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Image_5();
	this.instance.parent = this;
	this.instance.setTransform(-20.1,-53.5,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-20.1,-53.5,40.3,107.1);


(lib.l1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Image_2_3();
	this.instance.parent = this;
	this.instance.setTransform(-35.8,-67.8,0.48,0.48,-5.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-35.8,-73.1,71.7,146.4);


(lib.hh2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFC49E").s().p("AgWBpQgDgDgJgWQgXg3gDgTQgBgLAOghQAOgeALgPQAMgOAbgHQAdgIAJAMQAIAKgDARQgCAKgFAIIgKAZIglArIAFAaQAFAegEAOQgEARgMAGQgGACgEAAQgFAAgDgDg");
	this.shape.setTransform(-27.1,26.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFC49E").s().p("AieDFIAAgBIgKgdQgGAFgKADQgTAGgVgJQgWgJAEgZQAFgWAUgSQAOgNAsg5IApg1ICVj/IA0AGQA+APAyAuQAzAugDAHQgBADgLgGIhiBnQhlBsgTAaQgPAWAIATQAOAhAAAHQABAJgBAJQgJA1gLA3g");
	this.shape_1.setTransform(-5.9,6.3);

	this.instance = new lib.Image_0();
	this.instance.parent = this;
	this.instance.setTransform(-7,-36.9,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-33.2,-36.9,66.5,73.9);


(lib.hh1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Image_6();
	this.instance.parent = this;
	this.instance.setTransform(-24.4,-60,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-24.4,-60,49,120);


(lib.hd = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Image_3();
	this.instance.parent = this;
	this.instance.setTransform(-15.3,-29,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.3,-29,30.7,58.1);


(lib.ClipGroup_0 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AgDA1QgVgDgLgFIgOgHQgHgEgBgIQgDgNgBgNQgBgeALgMQAOgPAlADQAnAEANAUQAOAVgFAZQgFAXgQALQgJAFgPAAQgJAAgKgCg");
	mask.setTransform(6.2,5.5);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgIAJQgDgDAAgGQAAgEADgEQAEgEAEAAQAFAAAEAEQADAEAAAEQAAAGgDADQgEAEgFAAQgEAAgEgEg");
	this.shape.setTransform(6.9,4.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#401304").s().p("AgNAPQgGgFAAgKQAAgIAGgGQAGgHAHAAQAIAAAGAHQAGAGAAAIQAAAKgGAFQgFAHgJAAQgHAAgGgHg");
	this.shape_1.setTransform(8.6,5.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#932C09").s().p("AgCArQgRgBgMgNQgLgOACgRQABgSANgLQANgMAQABQARABALAOQALAOgBAQQgBASgNAMQgMAKgPAAIgCAAg");
	this.shape_2.setTransform(8.7,5.8);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_0, new cjs.Rectangle(4.6,1.6,7.9,8.6), null);


(lib.ClipGroup = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AANAzIgfgEQgXgGgCgNQgIggABgSQAEgjAuAGQAnAGAJAnQAFASgEATQgGAPgHAEQgEADgHAAIgMgCg");
	mask.setTransform(5.1,5.3);

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgHAJQgEgEAAgFQAAgFAEgDQADgEAEAAQAFAAAEAEQADAEAAAEQAAAFgDAEQgEAEgFAAQgEAAgDgEg");
	this.shape.setTransform(5.2,4.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#401304").s().p("AgNAQQgGgHAAgJQAAgIAGgHQAFgGAIAAQAIAAAGAGQAGAHAAAIQAAAJgGAHQgGAGgIAAQgHAAgGgGg");
	this.shape_1.setTransform(6.9,5.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#932C09").s().p("AgCArQgRgBgKgOQgKgNABgRQABgSAMgMQANgLAQABQAPABALAOQALANgBARQgCASgNAMQgLAKgNAAIgDAAg");
	this.shape_2.setTransform(6.9,5.6);

	var maskedShapeInstanceList = [this.shape,this.shape_1,this.shape_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup, new cjs.Rectangle(3,1.3,7.2,8.6), null);


(lib.ClipGroup_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("ArrHaIAAuzIXXAAIAAOzg");
	mask_1.setTransform(74.8,80.5);

	// Layer 3
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#C1272D","#FF2E7D"],[0,1],-14.9,0,15,0).s().p("AiQitIEmCmIkrC1g");
	this.shape_3.setTransform(115.5,108);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#C1272D","#FF2E7D"],[0,1],-22.6,-1.8,17.5,12.7).s().p("AiXjfIFjENImXCyg");
	this.shape_4.setTransform(33.7,35.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#F7931E","#F24E18"],[0,1],-35.8,-4.8,20,13.5).s().p("AkWkhIItC2ImtGMg");
	this.shape_5.setTransform(102.7,29);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#662D91","#7B278A","#B21779","#C01375"],[0,0.282,0.867,1],17.6,1.3,-30.3,-23).s().p("Aklj8IJLgDIkdH/g");
	this.shape_6.setTransform(155.8,101.7);

	var maskedShapeInstanceList = [this.shape_3,this.shape_4,this.shape_5,this.shape_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_1, new cjs.Rectangle(13.3,33.2,136.3,94.1), null);


(lib.ClipGroup_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("ArrHaIAAuzIXXAAIAAOzg");
	mask_2.setTransform(74.8,80.5);

	// Layer 3
	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#C1272D","#FF2E7D"],[0,1],-14.9,0,15,0).s().p("AiQitIEmCmIkrC1g");
	this.shape_7.setTransform(115.5,108);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#C1272D","#FF2E7D"],[0,1],-22.6,-1.8,17.5,12.7).s().p("AiXjfIFjENImXCyg");
	this.shape_8.setTransform(33.7,35.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#F7931E","#F24E18"],[0,1],-35.8,-4.8,20,13.5).s().p("AkWkhIIuC2ImuGMg");
	this.shape_9.setTransform(102.7,29);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#662D91","#7B278A","#B21779","#C01375"],[0,0.282,0.867,1],17.6,1.3,-30.3,-23).s().p("Aklj8IJLgDIkdH/g");
	this.shape_10.setTransform(155.8,101.7);

	var maskedShapeInstanceList = [this.shape_7,this.shape_8,this.shape_9,this.shape_10];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_2, new cjs.Rectangle(13.3,33.2,136.3,94.1), null);


(lib.ClipGroup_3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("EgtpAoOMAAAhQbMBbSAAAMAAABQbg");
	mask_3.setTransform(292.2,257.4);

	// Layer 3
	this.instance = new lib.Image_11();
	this.instance.parent = this;
	this.instance.setTransform(386.2,176.6,0.48,0.48);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgYAqIAAhRIAIAAIAEABIABADIABAMQAEgIAGgFQAFgFAJAAIAGABIAFACIgCALQAAAAAAABQAAAAgBAAQAAABAAAAQgBAAAAAAIgEgBIgGgBQgIAAgEAFQgFAEgDAIIAAA0g");
	this.shape_11.setTransform(379.7,273.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AgMAoQgHgDgFgGQgFgFgDgJQgDgIAAgJQAAgJACgIQADgHAFgFQAFgGAHgDQAHgDAIAAQAHAAAHACQAGADAEAEQAFAFADAHQACAHAAAJIgBAEIgCAAIg2AAQAAAIACAGQACAGADADQAEAEAFACQAEACAFAAQAFAAAEgBIAHgDIAFgDIADgBIADABIAEAGIgGAFIgIAEIgJADIgJAAQgIAAgHgCgAgNgZQgGAHgCAKIAsAAQAAgFgBgEQgCgEgCgDQgDgDgEgCQgEgCgFAAQgJAAgGAGg");
	this.shape_12.setTransform(371.8,273.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AApAqIAAg0QAAgJgEgFQgEgFgJAAQgDAAgDABQgDABgCADIgEAGQgCAEAAAEIAAA0IgNAAIAAg0QAAgJgFgFQgEgFgHAAQgFAAgFADQgEADgEAFIAAA8IgPAAIAAhRIAJAAQABAAAAAAQABAAAAAAQABABAAAAQABABAAABIABAIQAFgGAFgDQAFgEAIAAQAJAAAEAFQAFAFACAHQABgEADgDIAHgGIAHgDIAIgBQAHAAAEACQAFACAFAEQADAEACAGQACAGAAAHIAAA0g");
	this.shape_13.setTransform(361,273.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgPAnQgHgDgGgFQgEgGgDgHQgDgIAAgKQAAgIADgIQADgIAEgGQAGgFAHgDQAHgDAIAAQAJAAAHADQAHADAGAFQAEAGAEAIQACAIAAAIQAAAKgCAIQgEAHgEAGQgGAFgHADQgHADgJAAQgIAAgHgDgAgKgcQgEACgDAEQgEAEgBAGQgCAGABAGQgBAIACAFQABAGAEAEQADAEAEACQAFACAFAAQAMAAAGgIQAGgIAAgPQAAgNgGgJQgGgIgMAAQgFAAgFACg");
	this.shape_14.setTransform(349.8,273.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgHAyQgGgGABgKIAAgxIgKAAIgDgBIgBgCIAAgGIAOgCIADgZIABgCIADgBIAGAAIAAAcIAXAAIAAALIgXAAIAAAwQAAAFADADQACACAEAAIAEAAIACgCIADgBIACgBIACACIADAHQgDADgGACQgFACgGAAQgIAAgFgFg");
	this.shape_15.setTransform(342.3,271.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgQAnQgHgDgFgDIADgGIACgCIACAAIADABIAEACIAGADIAIABIAGgBIAGgDIADgEIABgGQAAgDgCgCQgCgCgDgCIgHgDIgHgDIgIgDIgGgCIgFgGQgCgFAAgEQAAgFACgFQACgEAEgDQADgEAGgBQAFgDAGAAQAIAAAHADQAGADAFAEIgDAFQAAABgBAAQAAAAgBABQAAAAAAAAQgBAAAAAAIgDgBIgEgBIgFgCIgIgBIgFABIgFACIgDADIgBAGQAAACABACIAFAEIAGADIAIACIAIAEIAHACQADADABAEQACADAAAFQAAAGgCAFQgCAEgEAEQgEADgGACQgFACgHABQgIAAgHgEg");
	this.shape_16.setTransform(335.6,273.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgSAnQgFgCgDgEQgDgEgCgGQgCgGAAgGIAAg0IAOAAIAAA0QAAAJAFAFQAEAFAIAAQAGAAAGgDQAFgDAFgFIAAg8IAOAAIAABRIgJAAQAAAAgBAAQgBAAAAgBQgBAAAAAAQAAgBAAgBIgBgJQgGAGgHAEQgGAEgHAAQgIAAgFgDg");
	this.shape_17.setTransform(327.6,273.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgPA3QgLgFgGgHQgIgIgEgLQgDgLgBgNQAAgMAFgLQAEgLAIgIQAHgIALgEQALgFAMABQAMAAAKADQAJAEAHAHIgFAHIgBACIgCAAIgCgBIgEgBIgDgCIgGgDIgGgCIgJgBQgKAAgGADQgJAEgFAFQgGAHgDAIQgCAIAAAKQAAALACAIQADAJAGAGQAFAFAIAEQAHACAHAAIAKAAIAHgCIAHgDIAHgFIACgBIACABIAHAHQgIAIgKAEQgKAGgPAAQgLgBgKgEg");
	this.shape_18.setTransform(317.7,271.5);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgYAqIAAhRIAIAAIAEABIABADIABAMQAEgIAGgFQAFgFAJAAIAGABIAFACIgCALQAAAAAAABQAAAAgBAAQAAABAAAAQgBAAAAAAIgEgBIgGgBQgIAAgEAFQgFAEgDAIIAAA0g");
	this.shape_19.setTransform(306.1,273.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgRApQgFgBgDgDQgDgDgBgEQgCgEAAgGQAAgFACgEQADgFAGgDQAGgDAKgBQAIgDAOAAIAAgGQAAgKgEgFQgEgFgIAAQgEAAgEACIgGACIgFADIgDACIgDgBIgCgCIgCgEQAGgHAIgDQAIgEAJAAQAHAAAFADQAFACAEAEQAEAEABAGQACAFAAAIIAAAzIgGAAIgDAAIgCgEIgCgHIgGAFIgHAEIgGADIgIABQgFgBgEgBgAABAGQgGAAgEADQgFACgBADQgCADAAADIABAGIACADQACACADABIAFAAIAGAAIAGgDIAFgDIAFgFIAAgRQgKAAgHACg");
	this.shape_20.setTransform(298.2,273.2);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgMAoQgHgDgFgGQgFgFgDgJQgDgIAAgJQAAgJACgIQADgHAFgFQAFgGAHgDQAHgDAIAAQAHAAAHACQAGADAEAEQAFAFADAHQACAHAAAJIgBAEIgCAAIg2AAQAAAIACAGQACAGADADQAEAEAFACQAEACAFAAQAFAAAEgBIAHgDIAFgDIADgBIADABIAEAGIgGAFIgIAEIgJADIgJAAQgIAAgHgCgAgNgZQgGAHgCAKIAsAAQAAgFgBgEQgCgEgCgDQgDgDgEgCQgEgCgFAAQgJAAgGAGg");
	this.shape_21.setTransform(289.9,273.2);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgxA6IAAhzIArAAQAMAAALAEQAKAEAIAIQAHAIAEAKQAEALAAAMQAAANgEALQgEALgHAHQgIAHgKAFQgLAEgMAAgAghAtIAbAAQAJABAHgEQAIgDAFgFQAFgHADgIQADgJAAgKQAAgKgDgIQgDgIgFgHQgFgFgIgEQgHgDgJAAIgbAAg");
	this.shape_22.setTransform(279.9,271.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgOAnQgHgDgFgEIACgDIABgBIACAAIACABIAEADIAGADIAJABQAEAAAEgCIAHgDQACgDABgDQACgDAAgEQAAgEgCgDIgFgEIgHgDIgIgDIgIgCQgEgBgDgDIgFgFQgCgEAAgFQAAgEACgEQACgEADgDQAEgDAEgCQAGgCAEAAQAIAAAGADQAFACAFAEIgBADIgCABIgDgBIgEgDIgFgCIgIgBIgHABIgGAEIgFAFIgBAGQAAADADADQABACADACIAIADIAHADIAIADIAHADQADACACADQACAEAAAFQAAAFgCAEQgCAFgEADQgDAEgFACQgFACgGAAQgJAAgFgDg");
	this.shape_23.setTransform(385.9,318.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgFAzQgFgFABgJIAAg3IgNAAIgBAAIAAgBIAAgDIAOgBIACgdIAAgCIABAAIAEAAIAAAfIAYAAIAAAFIgYAAIAAA2IABAGIABAEIAEACIAEABIAFgBIAEgCIADgCIABgBIABABIACAEIgHAFQgGACgFAAQgGAAgFgEg");
	this.shape_24.setTransform(379.6,317.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAYApIAAgyQAAgMgFgGQgFgHgLABQgHAAgHADQgHAFgFAHIAAA7IgIAAIAAhQIAFAAQAAAAABABQAAAAAAAAQABAAAAABQAAAAAAABIABAMQAFgIAIgDQAHgFAIAAQAHAAAFACQAFADADADQADAEACAGQACAFAAAIIAAAyg");
	this.shape_25.setTransform(372.4,318.4);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgDA6IAAhQIAHAAIAABQgAgCgrIgCgCIgCgCIAAgDIAAgCIACgDIACgBIACgBIADABIACABIACADIAAACIAAADIgCACIgCACIgDAAIgCAAg");
	this.shape_26.setTransform(366,316.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgOAnQgHgEgFgEQgFgGgCgIQgDgIAAgJQAAgJADgHQACgIAFgFQAFgGAHgDQAGgCAIAAQAJAAAHACQAGADAFAGQAFAFACAIQADAHAAAJQAAAJgDAIQgCAIgFAGQgFAEgGAEQgHADgJAAQgIAAgGgDgAgMgfQgFACgEAEQgDAFgCAHQgCAGAAAHQAAAIACAHQACAGADAFQAEAEAFADQAGADAGgBQAHABAGgDQAFgDAEgEQADgFACgGQACgHAAgIQAAgHgCgGQgCgHgDgFQgEgEgFgCQgGgEgHAAQgGAAgGAEg");
	this.shape_27.setTransform(359.7,318.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgiA5IAAhxIAeAAQASAAALAIQAKAJAAARQAAAHgDAGQgCAHgGADQgFAFgHADQgIACgIAAIgWAAIAAAugAgaAEIAWAAQAGAAAGgCQAGgCAFgCQAEgEABgFQADgFAAgGQAAgOgIgHQgJgHgOAAIgWAAg");
	this.shape_28.setTransform(351.5,316.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgWApIAAhQIAEAAIACABIABACIAAARQAEgKAHgFQAFgGAKAAIAGABIAGACIgBAFIgCABIgBAAIgCgBIgDAAIgFgBQgJAAgFAHQgGAFgDAKIAAA0g");
	this.shape_29.setTransform(340,318.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FFFFFF").s().p("AgPAoQgEgBgDgCQgCgDgCgEQgDgEAAgFQABgFADgFQACgEAHgDQAGgEAKgBIAWgCIAAgIQAAgLgEgGQgGgGgJAAQgFAAgEABIgHAEIgEADIgEACQAAAAAAAAQgBAAAAgBQAAAAAAAAQgBAAAAgBIgBgCQAGgGAHgDQAHgEAIAAQAHAAAEACQAFACADAEQADAEACAGQABAFAAAHIAAAzIgCAAQgBAAgBAAQAAAAgBAAQAAgBAAAAQAAgBgBAAIgBgKIgGAGIgHAEIgHADIgJABIgIgCgAACADIgNAEQgEACgDAEQgDADAAAEQAAAEABADIAEAFIAFADIAGABIAIgBIAHgDIAHgEIAFgGIAAgVIgUACg");
	this.shape_30.setTransform(332.3,318.4);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AgFAzQgFgFABgJIAAg3IgNAAIgBAAIAAgBIAAgDIAOgBIACgdIAAgCIABAAIAEAAIAAAfIAYAAIAAAFIgYAAIAAA2IABAGIABAEIAEACIAEABIAFgBIAEgCIADgCIABgBIABABIACAEIgHAFQgGACgFAAQgGAAgFgEg");
	this.shape_31.setTransform(325.6,317.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgUA2QgIgDgGgHIACgEIACgBIACABIADACIAEADIAFADIAHADIAJABQAGAAAFgCQAFgCAEgEQAEgEACgFQACgEAAgGQAAgGgDgEQgCgEgEgDQgEgCgGgCIgJgDIgLgDQgFgCgEgEQgEgDgCgFQgDgFAAgHQAAgGACgFQACgGAFgEQAEgEAGgCQAGgDAHAAQAJAAAHADQAHADAGAGIgCAEIgCABIgDgCIgEgDIgHgDQgFgCgGAAQgFAAgFACQgEACgEADQgDADgBAEQgCAEAAAEQAAAGACAEIAHAHIAJAEIAKAEIAKAEQAFABAEADQAEADADAFQACAFAAAHQAAAHgCAGQgDAHgEAEQgFAFgHADQgHADgHAAQgMAAgIgFg");
	this.shape_32.setTransform(318.5,316.7);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AgPA3QgHgDgGgIQgGgGgDgMQgDgLAAgPQAAgOADgLQADgLAGgHQAGgIAHgDQAIgDAHAAQAJAAAHADQAHADAGAIQAFAHAEALQADALAAAOQAAAPgDALQgEAMgFAGQgGAIgHADQgHADgJABQgHgBgIgDgAgLgwQgHADgFAGQgEAHgCAKQgDAJAAANQAAAOADAKQACAKAEAGQAFAHAHADQAGACAFAAQAHAAAFgCQAHgDAEgHQAEgGADgKQADgKAAgOQAAgNgDgJQgDgKgEgHQgEgGgHgDQgFgDgHAAQgFAAgGADg");
	this.shape_33.setTransform(306.4,316.7);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgPA3QgHgDgGgIQgGgGgDgMQgDgLAAgPQAAgOADgLQADgLAGgHQAGgIAHgDQAHgDAIAAQAJAAAHADQAHADAGAIQAGAHADALQADALAAAOQAAAPgDALQgDAMgGAGQgGAIgHADQgHADgJABQgIgBgHgDgAgMgwQgGADgEAGQgFAHgDAKQgCAJAAANQAAAOACAKQADAKAFAGQAEAHAGADQAHACAFAAQAGAAAHgCQAFgDAFgHQAFgGADgKQACgKAAgOQAAgNgCgJQgDgKgFgHQgFgGgFgDQgHgDgGAAQgFAAgHADg");
	this.shape_34.setTransform(297,316.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgPA3QgHgDgGgIQgFgGgEgMQgDgLAAgPQAAgOADgLQAEgLAFgHQAGgIAHgDQAHgDAIAAQAIAAAIADQAHADAGAIQAFAHAEALQADALAAAOQAAAPgDALQgEAMgFAGQgGAIgHADQgIADgIABQgIgBgHgDgAgLgwQgHADgFAGQgEAHgCAKQgDAJAAANQAAAOADAKQACAKAEAGQAFAHAHADQAFACAGAAQAGAAAGgCQAHgDAEgHQAEgGADgKQADgKAAgOQAAgNgDgJQgDgKgEgHQgEgGgHgDQgGgDgGAAQgGAAgFADg");
	this.shape_35.setTransform(287.6,316.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AgkA6IAAgDIAAgBIABgCIAngoIAJgKIAHgIIAFgJQABgFAAgGQABgGgDgFQgCgEgEgDQgDgDgFgCQgEgBgFgBQgFAAgEACQgFACgDADQgEADgCAEQgDAEgBAFQAAABAAAAQAAABgBAAQAAABAAAAQgBAAAAAAIgBAAIAAAAIgEgBQABgIADgFQADgGAFgEQAEgEAGgCQAHgCAFAAQAHAAAGACQAGACAEAEQAFAEADAFQACAGAAAIQAAAGgCAGIgFAKIgIAJIgKALIgjAkIAGgBIAFgBIAxAAIACACIABACIAAAEg");
	this.shape_36.setTransform(278.3,316.7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgYAxQgJgKAAgUQAAgJADgIQACgGAEgHQAFgFAGgDQAHgDAHAAQAJgBAGADQAGAEAEAFIAAgvIAIAAIAAB0IgFAAQAAAAgBAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIgBgOQgFAIgHAFQgHADgIAAQgPAAgIgKgAgJgNQgFACgEAFQgDAEgCAGQgCAGAAAJQAAARAHAJQAGAJAMAAQAIAAAGgFQAHgEAFgHIAAgqQgFgHgFgCQgGgDgHAAQgGAAgGADg");
	this.shape_37.setTransform(392.6,294);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FFFFFF").s().p("AgLAnQgHgDgEgGQgFgFgDgIQgDgIAAgKQAAgIACgHQADgIAFgFQAEgFAHgEQAHgDAIABQAGAAAGACQAGACAEAFQAEAEADAHQACAHAAAIIgBACIgBABIg5AAIAAABQAAAJACAGQACAIAEAEQAEAEAGADQAFADAGAAQAFAAAFgCIAIgDIAEgDIADgBIACABIABACQgBADgDABIgHAEIgJACIgJABQgHAAgHgCgAgHggQgGACgDADQgDADgDAFIgDAMIA1AAQgBgGgCgGQgCgFgCgDQgEgEgEgBQgFgCgFAAQgGAAgEACg");
	this.shape_38.setTransform(384.3,295.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FFFFFF").s().p("AgDAoIghhPIAFAAIACAAIACACIAaA+IABAEIAAADIABgDIABgEIAbg+IABgCIABAAIAHAAIgjBPg");
	this.shape_39.setTransform(376.4,295.8);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgDA6IAAhQIAHAAIAABQgAgCgrIgCgCIgCgCIAAgDIAAgCIACgDIACgBIACgBIADABIACABIACADIAAACIAAADIgCACIgCACIgDAAIgCAAg");
	this.shape_40.setTransform(370.5,294);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgLAnQgHgDgEgGQgFgFgDgIQgDgIAAgKQAAgIADgHQACgIAFgFQAFgFAGgEQAHgDAIABQAGAAAGACQAGACAEAFQAEAEADAHQACAHAAAIIgBACIgBABIg5AAIAAABQAAAJACAGQACAIAEAEQAEAEAFADQAGADAGAAQAFAAAFgCIAIgDIAEgDIADgBIACABIABACQgBADgEABIgGAEIgJACIgJABQgHAAgHgCgAgIggQgFACgDADQgDADgDAFIgDAMIA0AAQAAgGgCgGQgCgFgCgDQgEgEgEgBQgFgCgFAAQgGAAgFACg");
	this.shape_41.setTransform(364.4,295.8);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgKAnQgGgDgEgGQgFgEgDgIQgCgIAAgKQAAgIACgHQADgJAEgFQAFgFAHgEQAHgDAHABQAIAAAGACQAGACAFAFIgCACIgBABIgBAAIgCgBIgEgCIgGgCQgDgBgFAAQgGAAgGACQgFADgEAFQgDAEgCAGQgCAHAAAHQAAAIACAHQACAHADAFQAEAEAFACQAFADAGAAQAFAAAEgCIAHgDIAEgDIACgBIACABIACACIgFAEIgGAFIgIABIgIABQgHABgHgDg");
	this.shape_42.setTransform(356.8,295.8);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AgLAnQgHgDgEgGQgGgFgCgIQgDgIAAgKQAAgIADgHQACgIAFgFQAEgFAHgEQAHgDAIABQAGAAAGACQAGACAEAFQAEAEADAHQACAHAAAIIgBACIgBABIg5AAIAAABQAAAJACAGQACAIAEAEQAEAEAGADQAFADAGAAQAFAAAFgCIAIgDIAEgDIADgBIACABIABACQgBADgDABIgHAEIgJACIgJABQgHAAgHgCgAgHggQgGACgDADQgDADgDAFIgDAMIA1AAQgBgGgCgGQgCgFgCgDQgEgEgEgBQgFgCgFAAQgGAAgEACg");
	this.shape_43.setTransform(348.8,295.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgWApIAAhPIAEAAIACAAIABACIAAARQAEgKAHgGQAFgFAKAAIAGABIAGACIgBAFIgCACIgBAAIgCgCIgDAAIgFgBQgJABgFAFQgGAGgDALIAAAzg");
	this.shape_44.setTransform(341.8,295.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AgLAnQgHgDgFgGQgEgFgDgIQgDgIAAgKQAAgIACgHQADgIAFgFQAEgFAHgEQAHgDAIABQAGAAAGACQAGACAEAFQAEAEADAHQACAHAAAIIgBACIgBABIg5AAIAAABQAAAJACAGQACAIAEAEQAEAEAGADQAFADAFAAQAGAAAFgCIAHgDIAGgDIACgBIABABIACACQgBADgDABIgHAEIgJACIgJABQgHAAgHgCgAgHggQgGACgDADQgDADgDAFIgDAMIA1AAQAAgGgDgGQgCgFgCgDQgEgEgEgBQgFgCgFAAQgGAAgEACg");
	this.shape_45.setTransform(330.9,295.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgDAoIghhPIAFAAIADAAIABACIAaA+IABAEIAAADIABgDIABgEIAbg+IABgCIABAAIAGAAIgiBPg");
	this.shape_46.setTransform(323,295.8);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgPAoQgEgBgDgCQgCgDgCgEQgDgEAAgFQAAgFAEgFQACgEAHgDQAGgEAJgBIAXgCIAAgIQAAgLgEgGQgGgGgJAAQgFAAgEABIgHAEIgEADIgEACQAAAAAAAAQgBAAAAgBQAAAAAAAAQgBAAAAgBIgBgCQAGgGAHgDQAHgEAIAAQAGAAAFACQAFACADAEQADAEACAGQABAFABAHIAAAzIgDAAQgBAAgBAAQAAAAgBAAQAAgBAAAAQAAgBgBAAIgBgKIgGAGIgHAEIgHADIgJABIgIgCgAADADIgOAEQgEACgEAEQgCADAAAEQAAAEABADIAEAFIAFADIAGABIAIgBIAHgDIAGgEIAGgGIAAgVIgTACg");
	this.shape_47.setTransform(315.1,295.8);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("AAYA7IAAgzQAAgLgFgGQgFgHgLAAQgHAAgHAFQgHADgFAIIAAA7IgIAAIAAh1IAIAAIAAAzQAGgGAHgFQAHgEAIAAQAHAAAFADQAFACADADQADAFACAFQACAEAAAIIAAAzg");
	this.shape_48.setTransform(307,293.9);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FFFFFF").s().p("AgQAmQgFgBgDgFQgDgEgCgFQgCgGAAgGIAAgzIAIAAIAAAzQAAALAFAGQAFAHALAAQAHgBAHgEQAHgEAFgGIAAg8IAIAAIAABPIgFAAQAAAAgBAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIgBgMQgFAGgHAFQgIAEgIAAQgHAAgFgDg");
	this.shape_49.setTransform(294.9,295.9);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgOAnQgHgEgFgFQgFgFgCgIQgDgIAAgJQAAgJADgHQACgHAFgGQAFgFAHgEQAGgDAIABQAJgBAHADQAGAEAFAFQAFAGACAHQADAHAAAJQAAAJgDAIQgCAIgFAFQgFAFgGAEQgHACgJAAQgIAAgGgCgAgMggQgFACgEAGQgDAEgCAGQgCAHAAAHQAAAIACAHQACAHADAEQAEAEAFADQAGADAGAAQAHAAAGgDQAFgDAEgEQADgEACgHQACgHAAgIQAAgHgCgHQgCgGgDgEQgEgGgFgCQgGgCgHAAQgGAAgGACg");
	this.shape_50.setTransform(286.3,295.8);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgDA5IAAgvIgrhCIAIAAIABAAIACACIAhA0IABADIABADIABgDIADgDIAgg0IABgCIACAAIAHAAIgqBCIAAAvg");
	this.shape_51.setTransform(278.4,294.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#1E88E5").s().p("AsbGMIAAsXIY3AAIAAMXg");
	this.shape_52.setTransform(336.1,292.9);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#606971").s().p("AilA9QgWABgQgTQgQgSAAgZQAAgZAQgRQAQgTAWABIFLAAQAWgBAQATQAQARAAAZQAAAZgQASQgQATgWgBg");
	this.shape_53.setTransform(336.2,397.5);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#606971").s().p("AiBAqQgRAAgNgNQgMgMAAgRQAAgQAMgMQANgNARAAIEEAAQAQAAANANQAMAMAAAQQAAARgMAMQgNANgQAAg");
	this.shape_54.setTransform(336.2,40.8);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#EBEFF0").s().p("AtZXzMAAAgvlIazAAMAAAAvlg");
	this.shape_55.setTransform(336.2,217);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#3E464E").s().p("EgKsAgjQh7AAhZhYQhXhXAAh8MAAAg3vQAAh7BXhZQBZhXB7AAIVZAAQB7AABYBXQBYBZAAB7MAAAA3vQAAB8hYBXQhXBYh8AAg");
	this.shape_56.setTransform(336.6,216.2);

	this.instance_1 = new lib.Image_1_5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(257,94.6,0.48,0.48);

	this.instance_2 = new lib.Image_2_4();
	this.instance_2.parent = this;
	this.instance_2.setTransform(47.8,152.1,0.48,0.48);

	var maskedShapeInstanceList = [this.instance,this.shape_11,this.shape_12,this.shape_13,this.shape_14,this.shape_15,this.shape_16,this.shape_17,this.shape_18,this.shape_19,this.shape_20,this.shape_21,this.shape_22,this.shape_23,this.shape_24,this.shape_25,this.shape_26,this.shape_27,this.shape_28,this.shape_29,this.shape_30,this.shape_31,this.shape_32,this.shape_33,this.shape_34,this.shape_35,this.shape_36,this.shape_37,this.shape_38,this.shape_39,this.shape_40,this.shape_41,this.shape_42,this.shape_43,this.shape_44,this.shape_45,this.shape_46,this.shape_47,this.shape_48,this.shape_49,this.shape_50,this.shape_51,this.shape_52,this.shape_53,this.shape_54,this.shape_55,this.shape_56,this.instance_1,this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_3, new cjs.Rectangle(47.8,7.9,423.4,507), null);


(lib.ClipGroup_4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask_4 = new cjs.Shape();
	mask_4._off = true;
	mask_4.graphics.p("A08ONIAA8ZMAp5AAAIAAcZg");
	mask_4.setTransform(166.5,170.8);

	// Layer 3
	this.instance_3 = new lib.Image_8();
	this.instance_3.parent = this;
	this.instance_3.setTransform(0,0,0.655,0.655);

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_4;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_4, new cjs.Rectangle(32.4,79.9,268.2,181.7), null);


(lib.ClipGroup_0_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AgDA1QgWgDgKgFIgNgHQgIgEgBgIQgDgNgBgNQgBgeALgMQAOgPAlADQAnAEANAUQAOAVgFAZQgFAXgQALQgJAFgPAAQgJAAgKgCg");
	mask_1.setTransform(6.2,5.5);

	// Layer 3
	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgIAJQgDgDAAgGQAAgEADgEQAEgEAEAAQAFAAAEAEQADAEAAAEQAAAGgDADQgEAEgFAAQgEAAgEgEg");
	this.shape_3.setTransform(6.9,4.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#401304").s().p("AgOAPQgFgFAAgKQAAgIAFgGQAHgHAHAAQAIAAAHAHQAFAGAAAIQAAAKgFAFQgHAHgIAAQgHAAgHgHg");
	this.shape_4.setTransform(8.6,5.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#932C09").s().p("AgCArQgRgBgMgNQgLgOACgRQABgSANgLQANgMAQABQARABAMAOQALAOgCAQQgBASgNAMQgMAKgPAAIgCAAg");
	this.shape_5.setTransform(8.6,5.8);

	var maskedShapeInstanceList = [this.shape_3,this.shape_4,this.shape_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_0_1, new cjs.Rectangle(4.5,1.6,7.9,8.6), null);


(lib.ClipGroup_5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask_5 = new cjs.Shape();
	mask_5._off = true;
	mask_5.graphics.p("AANAzIgfgEQgXgGgCgNQgIggABgSQAEgjAuAGQAoAGAJAnQAEASgDATQgGAPgIAEQgEADgHAAIgMgCg");
	mask_5.setTransform(5.1,5.3);

	// Layer 3
	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgHAJQgEgEAAgFQAAgEAEgEQADgEAEAAQAFAAAEAEQADAEAAAEQAAAFgDAEQgEAEgFAAQgEAAgDgEg");
	this.shape_57.setTransform(5.2,4.1);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#401304").s().p("AgNAQQgGgHAAgJQAAgIAGgHQAFgGAIAAQAIAAAGAGQAGAHAAAIQAAAJgGAHQgGAGgIAAQgHAAgGgGg");
	this.shape_58.setTransform(6.9,5.4);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#932C09").s().p("AgCArQgRgBgKgOQgLgNACgRQABgSAMgMQANgLAPABQAQABALAOQALANgCARQgBASgNAMQgLAKgOAAIgCAAg");
	this.shape_59.setTransform(6.9,5.6);

	var maskedShapeInstanceList = [this.shape_57,this.shape_58,this.shape_59];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_5;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_59},{t:this.shape_58},{t:this.shape_57}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup_5, new cjs.Rectangle(3,1.3,7.2,8.6), null);


(lib.enterans2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AghBSIgEgBIgDAAIgCgBIAAgQIABAAIACAAIABAAIABAAIAJgBIAHgDQADgCADgEIADgJIAFgMIgqhyIAWAAIAcBXIAchXIAUAAIguCGIgEAJQgCAFgEAFIgKAHQgGADgIAAIgCAAg");
	this.shape.setTransform(134.7,-13.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAGBIQgFgCgDgDQgDgEgCgGQgDgGAAgJIAAhHIgVAAIAAgPIAVAAIAAgcIATAAIAAAcIAWAAIAAAPIgWAAIAABHIABAIIADAEIAEACIAFAAIAFAAIAFgBIAAAQIgHACIgJAAQgFAAgFgBg");
	this.shape_1.setTransform(125.5,-17.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgJBQIAAh0IASAAIAAB0gAgIg7QgDgDAAgFQAAgFADgDQADgDAFAAQAFAAADADQAEADAAAFQAAAFgEADQgDADgFAAQgFAAgDgDg");
	this.shape_2.setTransform(119.6,-18.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgGA6IgqhzIAUAAIAcBYIAehYIAUAAIgqBzg");
	this.shape_3.setTransform(111.5,-16.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgJBQIAAh0IASAAIAAB0gAgIg7QgDgDAAgFQAAgFADgDQADgDAFAAQAGAAADADQADADAAAFQAAAFgDADQgDADgGAAQgFAAgDgDg");
	this.shape_4.setTransform(103.6,-18.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAGBIQgFgCgDgDQgDgEgCgGQgDgGAAgJIAAhHIgVAAIAAgPIAVAAIAAgcIATAAIAAAcIAWAAIAAAPIgWAAIAABHIABAIIADAEIAEACIAFAAIAFAAIAFgBIAAAQIgHACIgJAAQgFAAgFgBg");
	this.shape_5.setTransform(96.9,-17.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgTA4QgKgFgHgIQgFgIgEgKQgDgLAAgMIAAgDQAAgMADgLQAEgKAFgIQAHgIAKgFQAJgEANAAQAKAAAJADQAHADAGAGQAHAFADAIQADAIABAJIgSAAQgBgGgCgEIgGgIQgEgEgEgCQgGgCgFAAQgJAAgFAEQgHADgDAGQgEAGgCAIIgBAPIAAADQAAAIABAIQACAHAEAGQADAGAHADQAFAEAJAAQAFAAAFgCQAFgBAEgDIAFgHQADgFABgEIASAAQgBAHgDAHQgEAHgGAGQgHAFgHADQgJADgJAAQgNAAgJgEg");
	this.shape_6.setTransform(88,-16.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgYA6QgHgDgFgFQgFgEgDgGQgDgHAAgHQAAgJAEgIQADgHAHgEQAHgEAKgDQAJgCALAAIAUAAIAAgKQAAgKgGgGQgGgGgMAAQgFAAgFABIgIAEIgFAGQgCADAAAEIgUAAQAAgGAEgGQADgGAGgFQAGgFAJgDQAIgDAKAAQAJAAAIACQAIACAGAFQAGAFADAIQADAHAAAKIAAA1IABAMIADAMIAAABIgVAAIgBgFIgCgHIgGAGIgHAEIgJADIgKABQgJAAgHgCgAgSAHQgJAFAAALIABAIQACAEADACQACADAFACQAEABAFAAQAFAAAEgBQAFgCAEgCIAHgGIAEgGIAAgYIgQAAQgRAAgJAFg");
	this.shape_7.setTransform(76.1,-16.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgRA4QgKgEgHgHQgHgIgDgKQgEgLgBgMIAAgEQAAgOAFgLQAEgLAHgIQAHgHAJgEQAKgEAIAAQANAAAJAEQAJAEAGAIQAGAIACAKQADAKAAANIAAAIIhNAAQAAAIACAHQADAHAEAFQAFAFAGADQAGADAHAAQAKAAAIgEQAHgEAGgIIAMAKIgHAIIgJAIIgNAFQgGABgJAAQgMAAgJgEgAgIgpQgFACgEAFQgEAEgEAGQgCAGgBAIIA5AAIAAgBIgCgMQgBgFgDgFQgDgEgFgDQgGgDgIAAQgEAAgFACg");
	this.shape_8.setTransform(58.9,-16.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAaBTIAAhNQAAgGgBgEQgCgFgDgDQgDgDgEgCIgKgBQgJAAgHAFQgIAFgEAIIAABTIgUAAIAAilIAUAAIAAA/QAGgHAJgFQAJgEAKAAQAIAAAHACQAHADAFAFQAFAFACAIQADAIAAAKIAABNg");
	this.shape_9.setTransform(46.9,-18.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAGBIQgFgCgDgDQgDgEgCgGQgDgGAAgJIAAhHIgVAAIAAgPIAVAAIAAgcIATAAIAAAcIAWAAIAAAPIgWAAIAABHIABAIIADAEIAEACIAFAAIAFAAIAFgBIAAAQIgHACIgJAAQgFAAgFgBg");
	this.shape_10.setTransform(36.9,-17.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgRA4QgKgEgHgHQgHgIgDgKQgEgLgBgMIAAgEQAAgOAFgLQAEgLAHgIQAHgHAJgEQAJgEAJAAQANAAAJAEQAJAEAGAIQAGAIACAKQADAKAAANIAAAIIhNAAQAAAIACAHQADAHAEAFQAFAFAGADQAGADAHAAQAKAAAIgEQAHgEAGgIIAMAKIgHAIIgJAIIgNAFQgGABgJAAQgMAAgJgEgAgIgpQgFACgEAFQgEAEgEAGQgCAGgBAIIA5AAIAAgBIgCgMQgBgFgDgFQgDgEgFgDQgGgDgIAAQgEAAgFACg");
	this.shape_11.setTransform(22.5,-16.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAGBIQgFgCgDgDQgDgEgCgGQgDgGAAgJIAAhHIgVAAIAAgPIAVAAIAAgcIATAAIAAAcIAWAAIAAAPIgWAAIAABHIABAIIADAEIAEACIAFAAIAFAAIAFgBIAAAQIgHACIgJAAQgFAAgFgBg");
	this.shape_12.setTransform(12.6,-17.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgRA4QgKgEgHgHQgHgIgDgKQgEgLgBgMIAAgEQAAgOAFgLQAEgLAHgIQAHgHAJgEQAKgEAIAAQANAAAJAEQAJAEAGAIQAGAIACAKQADAKAAANIAAAIIhNAAQAAAIACAHQADAHAEAFQAFAFAGADQAGADAHAAQAKAAAIgEQAHgEAGgIIAMAKIgHAIIgJAIIgNAFQgGABgJAAQgMAAgJgEgAgIgpQgFACgEAFQgEAEgEAGQgCAGgBAIIA5AAIAAgBIgCgMQgBgFgDgFQgDgEgFgDQgGgDgIAAQgEAAgFACg");
	this.shape_13.setTransform(3.6,-16.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgJBTIAAilIATAAIAAClg");
	this.shape_14.setTransform(-5.1,-18.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgwBSIAAihIASAAIABANQAGgHAIgEQAJgEAKAAQALAAAIAEQAJAFAFAIQAGAHADALQADALAAAOIAAACQAAANgDAJQgDALgGAIQgFAIgJAEQgIAFgLAAQgKAAgIgEQgIgDgGgHIAAA5gAgKg/QgEABgEADIgGAFIgEAGIAAA3IAEAGIAGAFIAIADQAEACAFAAQAHAAAGgEQAGgDAEgGQAEgGABgHQACgHAAgJIAAgCQAAgJgCgIQgBgIgEgFQgEgGgGgEQgGgDgHAAIgJABg");
	this.shape_15.setTransform(-13.7,-14);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AA+A7IAAhLQAAgIgCgFQgCgEgDgDQgDgDgFgBIgKgBQgGAAgFABQgFACgDAEQgEADgCAEQgCAFAAAFIAABMIgTAAIAAhLQAAgHgCgFQgCgFgDgDQgDgDgFgBIgKgBQgLAAgGAEQgHAFgDAHIAABUIgUAAIAAhzIATAAIABANQAGgHAIgEQAJgEALAAQALAAAJAEQAIAFAEAJQAGgIAKgFQAJgFANAAQAJAAAHACQAHACAFAGQAFAFADAIQADAIAAALIAABLg");
	this.shape_16.setTransform(-29.8,-16.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgVA4QgKgFgHgHQgHgIgDgLQgEgLAAgNIAAgBQAAgNAEgLQADgKAHgIQAHgIAKgFQAJgEAMAAQAMAAAKAEQAKAFAGAIQAIAIADAKQAEALAAANIAAABQAAANgEALQgDALgIAIQgGAHgKAFQgKAEgMAAQgLAAgKgEgAgNgnQgHADgEAGQgEAGgCAIQgCAHAAAJIAAABQAAAJACAIQACAIAEAFQAEAGAHAEQAFADAIAAQAIAAAHgDQAGgEAEgGQADgFADgIQACgIAAgJIAAgBQAAgJgCgHQgDgIgDgGQgEgGgHgDQgGgEgIAAQgHAAgGAEg");
	this.shape_17.setTransform(-45.7,-16.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgTA4QgKgFgHgIQgFgIgEgKQgDgLAAgMIAAgDQAAgMADgLQAEgKAFgIQAHgIAKgFQAJgEANAAQAKAAAJADQAHADAGAGQAHAFADAIQADAIABAJIgSAAQgBgGgCgEIgGgIQgEgEgEgCQgFgCgGAAQgJAAgGAEQgFADgEAGQgEAGgCAIIgBAPIAAADQAAAIABAIQACAHAEAGQADAGAGADQAHAEAIAAQAFAAAFgCQAFgBAEgDIAFgHQADgFABgEIASAAQgBAHgDAHQgEAHgGAGQgHAFgHADQgJADgJAAQgNAAgJgEg");
	this.shape_18.setTransform(-57.7,-16.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgRA4QgKgEgHgHQgHgIgEgKQgDgLAAgMIAAgEQAAgOAEgLQAEgLAHgIQAHgHAJgEQAKgEAJAAQAMAAAJAEQAJAEAGAIQAGAIADAKQADAKAAANIAAAIIhPAAQABAIACAHQACAHAFAFQAEAFAHADQAGADAHAAQALAAAHgEQAHgEAFgIIANAKIgHAIIgJAIIgMAFQgIABgIAAQgLAAgKgEgAgJgpQgEACgFAFQgDAEgDAGQgDAGgBAIIA6AAIAAgBIgDgMQgBgFgDgFQgEgEgFgDQgFgDgHAAQgFAAgGACg");
	this.shape_19.setTransform(-74.8,-16.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgSA5QgJgDgGgFQgGgGgDgGQgEgHABgIIATAAQABAHACAEQADAEAEACIAJAEIAIABQAGAAAFgBQAFgBADgDIAFgFIACgHIgBgGQgBgDgEgCQgCgDgGgCIgNgEIgRgFQgIgCgFgDQgFgEgDgGQgEgFAAgHQABgHADgGQADgHAGgEQAFgFAIgDQAIgCAJAAQAKAAAIADQAIACAHAFQAFAFADAHQADAGAAAIIgUAAIgBgHQgCgEgDgDIgIgEQgFgCgFAAQgFAAgFABIgHAEQgDADgBADQgCADAAADIACAGIAEAFIAIAEIAMADIATAGQAIACAFAEQAGAEACAFQADAGgBAHQAAAIgDAGQgDAGgGAFQgGAEgIADQgIACgLAAQgKAAgJgDg");
	this.shape_20.setTransform(-86.4,-16.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgYA6QgHgDgFgFQgFgEgDgGQgDgHAAgHQAAgJAEgIQADgHAHgEQAHgEAKgDQAJgCALAAIAUAAIAAgKQAAgKgGgGQgGgGgMAAQgFAAgFABIgIAEIgFAGQgCADAAAEIgUAAQAAgGAEgGQADgGAGgFQAGgFAJgDQAIgDAKAAQAJAAAIACQAIACAGAFQAGAFADAIQADAHAAAKIAAA1IABAMIADAMIAAABIgVAAIgBgFIgCgHIgGAGIgHAEIgJADIgKABQgJAAgHgCgAgSAHQgJAFAAALIABAIQACAEADACQACADAFACQAEABAFAAQAFAAAEgBQAFgCAEgCIAHgGIAEgGIAAgYIgQAAQgRAAgJAFg");
	this.shape_21.setTransform(-98.1,-16.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AgRA4QgKgEgHgHQgHgIgDgKQgEgLgBgMIAAgEQAAgOAFgLQAEgLAHgIQAHgHAJgEQAJgEAJAAQANAAAJAEQAJAEAGAIQAGAIACAKQADAKAAANIAAAIIhNAAQAAAIACAHQADAHAEAFQAFAFAGADQAGADAHAAQAKAAAIgEQAHgEAGgIIAMAKIgHAIIgJAIIgNAFQgGABgJAAQgMAAgJgEgAgIgpQgFACgEAFQgEAEgEAGQgCAGgBAIIA5AAIAAgBIgCgMQgBgFgDgFQgDgEgFgDQgGgDgIAAQgEAAgFACg");
	this.shape_22.setTransform(-109.8,-16.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgJBTIAAilIATAAIAAClg");
	this.shape_23.setTransform(-118.5,-18.6);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("Ag4BPIAAidIA6AAQANAAAKAFQALADAGAHQAHAGAEAIQADAJAAAKQAAAMgDAJQgEAJgHAEQgGAHgLADQgKADgNAAIgkAAIAAA+gAgiAAIAkAAQAJAAAGgBQAHgDAEgDQAEgEACgGQACgGAAgGQAAgGgCgGQgCgGgEgEQgEgEgHgDQgGgDgJAAIgkAAg");
	this.shape_24.setTransform(-127.6,-18.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#7C1118").s().p("AgyizICqAAIjvFng");
	this.shape_25.setTransform(5.8,15.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#7C1118").s().p("A52DdQgxAAgjgjQgjgkgBgxIAAjJQABgxAjgjQAjgkAxAAMAzsAAAQAyAAAjAkQAjAjAAAxIAADJQAAAxgjAkQgkAjgxAAg");
	this.shape_26.setTransform(2.2,-18);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-175.3,-40,354.9,73.7);


(lib.drop1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Image_7();
	this.instance.parent = this;
	this.instance.setTransform(-84.7,-253.4,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.drop1, new cjs.Rectangle(-84.7,-253.4,169.5,506.9), null);


(lib.Birthday = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgVCRIAMg8IgQAAIgxjlIAmAAQAfCZAFAwQAHgyAKgxIAUhmIAmAAIg9Ehg");
	this.shape.setTransform(166.2,6.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhEA9IAAgNQAAgWAMgPQAMgMAZgIIAzgNIAAggQAAgOgIgHQgHgGgZgBQgbAAgWAJIAAgjQAYgIAhAAQBFABAAA9IAACpIgkAAIAAgQQgRARgdAAQg3ABAAg4gAgFASQgOAFgGAGQgGAIAAANIAAALQAAANAHAHQAGAGASABQASAAAOgNIAAhDg");
	this.shape_1.setTransform(148.9,3.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ag1CEQgOgPAAgbIAAhvQAAgdASgRQARgRAfAAIAfAAIAAg9IAmAAIAAEiIgmAAIAAgOQgPAPgbAAQgbAAgOgOgAgdgUIAABqQAAAaAaAAQAVAAAMgLIAAiXIgcAAQgfAAAAAeg");
	this.shape_2.setTransform(131.7,0.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAdCRIAAirQAAgXgYgBQgQAAgRAJIAAC6IgmAAIAAkhIAmAAIAABEQATgJAXAAQAYAAAPAOQAOANAAAZIAACyg");
	this.shape_3.setTransform(113.8,0.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgfBSIAAiQIgYAAIAAghIAZAAIALgpIAZAAIAAApIAuAAIAAAhIguAAIAACNQAAAYAYAAQAMAAAOgFIAAAiIgLACQgMADgJAAQg3AAAAg3g");
	this.shape_4.setTransform(98.5,2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgtBzIAAjlIAkAAIAAARIAFgDIALgIIANgFIAFgBIAUgBIAAAkIgLAAQgUAAgVAPIAACzg");
	this.shape_5.setTransform(88,3.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgSCTIAAjiIAlAAIAADigAgVh8QAAgWAVAAQAWAAAAAWQAAAWgWAAQgVAAAAgWg");
	this.shape_6.setTransform(76.6,0.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhHCHIAAkNIBLAAQAhAAAQAPQARAPAAAfIAAAXQAAAmgdAMQAfAIAAAoIAAAaQAAAegRAPQgQAQggAAgAgiBnIAkAAQAhAAAAgfIAAgaQAAgeghAAIgkAAgAgigRIAiAAQAhAAgBgfIAAgXQABgfghgBIgiAAg");
	this.shape_7.setTransform(62.9,1.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("Ag9BvIAAgjQAUAHAZAAQAXAAAJgHQAKgGAAgNIAAgHQAAgJgFgGQgFgGgPgIIgggRQgfgPgBgiIAAgNQAAgdARgPQASgOAlAAQAaAAAUAGIAAAjQgTgIgYAAQgWAAgIAHQgHAGAAAOIAAAIQAAAKAEAGQAEAFANAHIAhATQAjARAAAfIAAALQAAA7hNAAQgZAAgXgGg");
	this.shape_8.setTransform(38.3,3.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgXArIARhVIANAAQARAAAAAQQAAAVgcAwg");
	this.shape_9.setTransform(27.5,-9.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAdBzIAAirQAAgXgYgBQgQABgSAIIAAC6IglAAIAAjlIAkAAIAAALQAagMAUAAQAXAAAOAOQAOANAAAZIAACyg");
	this.shape_10.setTransform(15,3.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgyBmQgRgSAAgcIAAhvQAAgcARgSQAQgRAiAAQAjAAAQARQARARAAAdIAABvQAAAdgRARQgQARgjAAQgiAAgQgRgAgeg2IAABuQAAAeAeAAQAfAAAAgeIAAhuQAAgfgfAAQgeAAAAAfg");
	this.shape_11.setTransform(-3.1,3.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("Ag+BvIAAgjQAVAHAYAAQAZAAAIgHQAKgFAAgOIAAgHQAAgKgFgFQgGgGgOgIIgfgRQgggPAAgiIAAgNQAAgeARgOQARgOAlAAQAaAAAUAGIAAAjQgUgIgXAAQgXAAgHAHQgHAGAAAOIAAAIQAAALAEAFQAFAGAMAGIAhATQAiARAAAfIAAALQAAA7hLAAQgaAAgYgGg");
	this.shape_12.setTransform(-19.3,3.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AhEA9IAAgNQAAgWAMgPQAMgMAZgIIAzgNIAAggQAAgOgIgHQgHgGgZgBQgbAAgWAJIAAgjQAYgIAhAAQBFABAAA9IAACpIgkAAIAAgQQgRARgdAAQg3ABAAg4gAgFASQgOAFgGAGQgGAIAAANIAAALQAAAOAHAGQAGAGASABQASAAAOgNIAAhDg");
	this.shape_13.setTransform(-35.9,3.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AggClIANgqQAPg0gBgtIAAi+IAlAAIAAC9QAAA0gNAvIgMApg");
	this.shape_14.setTransform(-49.6,4.8);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("Ag9BvIAAgjQAUAHAZAAQAXAAAKgHQAIgFABgOIAAgHQgBgKgFgFQgDgFgQgJIgggRQgfgPAAgiIAAgNQgBgeASgOQARgOAlAAQAaAAAUAGIAAAjQgUgIgXAAQgXAAgGAHQgJAGABAOIAAAIQAAALAEAFQAEAGANAGIAhATQAiARABAfIAAALQAAA7hNAAQgaAAgWgGg");
	this.shape_15.setTransform(-67.6,3.9);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgSCTIAAjiIAlAAIAADigAgVh8QAAgWAVAAQAWAAAAAWQAAAWgWAAQgVAAAAgWg");
	this.shape_16.setTransform(-79.3,0.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgVCRIAMg8IgQAAIgxjlIAmAAQAeCWAGAzQAGguALg1IAVhmIAlAAIg8Ehg");
	this.shape_17.setTransform(-99.3,6.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AhEA9IAAgNQAAgXAMgOQALgMAagIIAzgNIAAggQAAgOgIgHQgIgGgZgBQgaAAgWAJIAAgjQAXgIAiAAQBFABAAA9IAACpIgkAAIAAgQQgSARgcAAQg3ABAAg4gAgFASQgPAFgGAGQgFAHAAAOIAAALQAAAOAGAGQAHAGASABQASAAAOgNIAAhDg");
	this.shape_18.setTransform(-116.6,3.9);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("Ag1CEQgOgOABgcIAAhvQgBgdASgRQARgRAfAAIAfAAIAAg9IAmAAIAAEiIglAAIAAgOQgRAPgZAAQgcAAgOgOgAgdgUIAABqQAAAaAbAAQAUAAAMgLIAAiXIgbAAQggAAAAAeg");
	this.shape_19.setTransform(-133.8,0.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgzBmQgQgRAAgdIAAhvQAAgdAQgRQARgRAiAAQAiAAARARQARASAAAcIAABvQAAAcgRASQgRARgiAAQgiAAgRgRgAgeg2IAABuQAAAeAeAAQAeAAAAgeIAAhuQAAgfgeAAQgeAAAAAfg");
	this.shape_20.setTransform(-151.5,3.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgSCHIAAjsIg0AAIAAghICNAAIAAAhIg0AAIAADsg");
	this.shape_21.setTransform(-167.7,1.9);

	this.instance = new lib.Image_10();
	this.instance.parent = this;
	this.instance.setTransform(-211.4,-66.9,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-211.4,-66.9,422.9,133.9);


(lib.ar = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAbAtIAAg3QAAgOgGgGQgGgIgMABQgIAAgHAEQgIAEgGAJIAABBIgIAAIAAhYIAEAAQABAAAAAAQABAAAAABQABAAAAAAQAAABAAAAIABAOQAGgIAIgEQAIgGAJAAQAIABAFACQAGACAEAEQADAFACAGQACAGAAAJIAAA3g");
	this.shape.setTransform(89.5,15.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgQArQgIgDgFgHQgFgFgDgJQgDgIAAgLQAAgKADgIQADgJAFgGQAFgFAIgEQAHgDAJAAQAKAAAHADQAIAEAFAFQAFAGADAJQADAIAAAKQAAALgDAIQgDAJgFAFQgFAHgIADQgHADgKAAQgJAAgHgDgAgNgjQgGACgEAGQgEAFgCAHQgCAIAAAHQAAAJACAIQACAHAEAFQAEAFAGADQAGACAHAAQAIAAAGgCQAGgDAEgFQAEgFACgHQACgIAAgJQAAgHgCgIQgCgHgEgFQgEgGgGgCQgGgDgIAAQgHAAgGADg");
	this.shape_1.setTransform(79.7,16);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgQArQgIgDgFgFIACgDIABgBIACAAIACABIAFADIAHADQAEACAGAAQAFAAAEgCIAHgEQADgDACgDQABgEAAgEQAAgFgCgDIgGgFIgIgDIgIgDIgJgCIgIgEQgDgDgDgEQgCgDAAgGQAAgFACgEQACgFAEgDQAEgDAFgCQAGgCAGAAQAIAAAGACQAGACAGAFIgCADQAAABAAAAQgBAAAAAAQAAABAAAAQgBAAAAAAIgDgCIgEgCIgGgDIgJgBQgEAAgEACIgHADIgEAGQgCADAAADQAAAFADACQACADADACIAIAEIAIACIAJAEIAIADIAGAGQACAEAAAGQAAAFgCAFQgDAFgEAEQgDAEgGACQgGACgGAAQgKAAgGgDg");
	this.shape_2.setTransform(70.9,16);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AAbBBIgCAAIgCgBIglgpIgDgCIgEgBIgEAAIAAAtIgIAAIAAiBIAIAAIAABPIAEAAIADgBIACgCIAjghIACgCIADAAIAHAAIgmAkIgCACIgCACIADABIACACIAoAtg");
	this.shape_3.setTransform(63.2,13.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgLArQgHgDgFgGQgFgGgDgIQgDgJAAgLQAAgJADgIQADgJAFgGQAFgGAHgEQAIgDAJAAQAIAAAIADQAGADAFAFIgCACIgBABIgBAAIgDgBIgDgCIgHgDIgKgBQgHAAgGADQgFACgFAGQgDAFgCAHQgDAHAAAIQAAAKADAHQACAHADAFQAFAFAFADQAGACAGAAQAGABAFgCIAHgDIAEgEIAEgCIABABIACADIgEAFIgIAEIgIADIgKABQgHAAgIgDg");
	this.shape_4.setTransform(54.3,16);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgRAtIgHgEQgEgDgCgEQgCgFAAgGQAAgGAEgFQADgEAHgEQAHgEALgBQAJgCAQAAIAAgKQAAgMgGgHQgFgGgLAAQgFAAgFABIgHAEIgGAEIgDACQAAAAgBAAQAAAAAAAAQgBAAAAgBQAAAAgBAAIgBgDQAHgHAIgEQAHgDAJAAQAHAAAGACQAFACAEAFQADAEACAGQACAGAAAHIAAA6IgEAAQAAAAgBAAQAAgBgBAAQAAAAAAgBQgBAAAAgBIgBgLIgHAGIgIAFIgJADIgJABIgJgBgAADAEQgJABgGADQgGACgDAEQgCAEAAAFQAAAEABADQACAEACACIAGADIAHABIAIgBIAIgDIAHgFIAHgGIAAgXQgNAAgJACg");
	this.shape_5.setTransform(45.5,16);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgSBAIgIgCIAAgDIAAgCIABgBIACgBIABAAIADABIAEAAIAEAAQAIABAEgCQAGgCADgFQAEgFACgGQACgHAAgKIAAhTIAKAAIAABTQAAALgEAIQgCAJgFAFQgFAGgHADQgGADgIAAIgJgBg");
	this.shape_6.setTransform(36.7,14.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgQArQgIgDgFgHQgFgFgDgJQgDgIAAgLQAAgKADgIQADgJAFgGQAFgFAIgEQAHgDAJAAQAKAAAHADQAIAEAFAFQAFAGADAJQADAIAAAKQAAALgDAIQgDAJgFAFQgFAHgIADQgHADgKAAQgJAAgHgDgAgNgjQgGACgEAGQgEAFgCAHQgCAIAAAHQAAAJACAIQACAHAEAFQAEAFAGADQAGACAHAAQAIAAAGgCQAGgDAEgFQAEgFACgHQACgIAAgJQAAgHgCgIQgCgHgEgFQgEgGgGgCQgGgDgIAAQgHAAgGADg");
	this.shape_7.setTransform(24.8,16);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgFA4QgGgFAAgKIAAg9IgMAAIgCAAIgBgCIAAgDIAPgBIACggIACgCIABAAIAEAAIAAAiIAbAAIAAAGIgbAAIAAA8IABAGIACAFQAAAAAAABQABAAAAAAQABABAAAAQABAAABAAIAEABIAGgBIAEgCIADgCIABgBIACABIACAEQgDAEgFACQgGACgGAAQgHAAgFgFg");
	this.shape_8.setTransform(16.7,14.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAbAtIAAg3QAAgOgGgGQgGgIgMABQgIAAgHAEQgIAEgGAJIAABBIgIAAIAAhYIAEAAQABAAAAAAQABAAAAABQABAAAAAAQAAABAAAAIABAOQAGgIAIgEQAIgGAJAAQAIABAFACQAGACAEAEQADAFACAGQACAGAAAJIAAA3g");
	this.shape_9.setTransform(8.7,15.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgQArQgIgDgFgHQgFgFgDgJQgDgIAAgLQAAgKADgIQADgJAFgGQAFgFAIgEQAHgDAJAAQAKAAAHADQAIAEAFAFQAFAGADAJQADAIAAAKQAAALgDAIQgDAJgFAFQgFAHgIADQgHADgKAAQgJAAgHgDgAgNgjQgGACgEAGQgEAFgCAHQgCAIAAAHQAAAJACAIQACAHAEAFQAEAFAGADQAGACAHAAQAIAAAGgCQAGgDAEgFQAEgFACgHQACgIAAgJQAAgHgCgIQgCgHgEgFQgEgGgGgCQgGgDgIAAQgHAAgGADg");
	this.shape_10.setTransform(-1.1,16);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgQArQgIgDgFgHQgFgFgDgJQgDgIAAgLQAAgKADgIQADgJAFgGQAFgFAIgEQAHgDAJAAQAKAAAHADQAIAEAFAFQAFAGADAJQADAIAAAKQAAALgDAIQgDAJgFAFQgFAHgIADQgHADgKAAQgJAAgHgDgAgNgjQgGACgEAGQgEAFgCAHQgCAIAAAHQAAAJACAIQACAHAEAFQAEAFAGADQAGACAHAAQAIAAAGgCQAGgDAEgFQAEgFACgHQACgIAAgJQAAgHgCgIQgCgHgEgFQgEgGgGgCQgGgDgIAAQgHAAgGADg");
	this.shape_11.setTransform(-14.3,16);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgSA8QgGgCgFgDQgFgDgDgFQgCgEAAgFQAAgHAEgFQAFgFAJgDQgFgBgDgDQgCgDAAgFIAAgEIACgDIAEgEIAFgDQgHgEgDgGQgEgGAAgJQAAgGADgFQACgGAEgDQAEgEAGgCQAGgCAHAAQAFAAAFABIAJAEIAXAAIAAADQAAABAAAAQAAABgBAAQAAAAAAAAQgBABgBAAIgNABIADAHIACAJQAAAHgDAFQgCAFgEAEQgEAEgGACQgGACgGAAQgIAAgGgCIgGAFQgCACAAADQAAADADADQADACAEAAIAKACIAKAAIAMABIAKADQAFACACAEQADAEABAGQgBAFgDAFQgCAFgGAEQgFAEgHADQgIACgIAAQgJAAgIgCgAgWAXIgFAEIgDAEIgBAGQAAAEACAEQABADAFACIAJAEQAGACAHAAQAGAAAFgCQAHgBAEgDQAEgDADgEQADgEAAgEQAAgEgDgDQgCgDgDgBIgJgCIgKgBIgKAAIgKgCIgGAEgAgMg1QgFABgDADQgDADgCAEQgBAFAAAFQAAAFABAEQACAEADADQADADAFABQAFACAFAAQAFAAAEgCQAFgBADgDQADgDABgEQACgEAAgFQAAgFgCgFQgBgEgDgDQgEgDgEgBQgFgCgEAAQgFAAgFACg");
	this.shape_12.setTransform(-23.6,17.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgQArQgIgDgFgHQgFgFgDgJQgDgIAAgLQAAgKADgIQADgJAFgGQAFgFAIgEQAHgDAJAAQAKAAAHADQAIAEAFAFQAFAGADAJQADAIAAAKQAAALgDAIQgDAJgFAFQgFAHgIADQgHADgKAAQgJAAgHgDgAgNgjQgGACgEAGQgEAFgCAHQgCAIAAAHQAAAJACAIQACAHAEAFQAEAFAGADQAGACAHAAQAIAAAGgCQAGgDAEgFQAEgFACgHQACgIAAgJQAAgHgCgIQgCgHgEgFQgEgGgGgCQgGgDgIAAQgHAAgGADg");
	this.shape_13.setTransform(-33.2,16);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgDBBIAAiBIAHAAIAACBg");
	this.shape_14.setTransform(-40.2,13.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgRArQgGgDgGgFIACgDIABgBIABAAIAEABIAEADIAHADQAEACAGAAQAEAAAFgCIAHgEQADgDACgDQABgEAAgEQAAgFgDgDIgFgFIgIgDIgIgDIgJgCIgIgEQgEgDgCgEQgCgDAAgGQAAgFACgEQACgFAEgDQAEgDAFgCQAGgCAFAAQAJAAAGACQAGACAGAFIgCADQAAABAAAAQAAAAgBAAQAAABAAAAQgBAAAAAAIgCgCIgFgCIgGgDIgJgBQgEAAgEACIgHADIgFAGQgBADAAADQAAAFACACQACADAEACIAIAEIAIACIAJAEIAIADIAFAGQADAEAAAGQAAAFgCAFQgCAFgEAEQgEAEgGACQgGACgHAAQgJAAgHgDg");
	this.shape_15.setTransform(-49.7,16);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgGA4QgEgFAAgKIAAg9IgOAAIgBAAIgBgCIAAgDIAQgBIACggIAAgCIACAAIAEAAIAAAiIAbAAIAAAGIgbAAIAAA8IABAGIABAFQABAAAAABQABAAAAAAQABABAAAAQABAAAAAAIAFABIAGgBIAEgCIACgCIADgBIABABIACAEQgEAEgFACQgFACgFAAQgIAAgGgFg");
	this.shape_16.setTransform(-56.7,14.5);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAbAtIAAg3QAAgOgGgGQgGgIgMABQgIAAgHAEQgIAEgGAJIAABBIgIAAIAAhYIAEAAQABAAAAAAQABAAAAABQABAAAAAAQAAABAAAAIABAOQAGgIAIgEQAIgGAJAAQAIABAFACQAGACAEAEQADAFACAGQACAGAAAJIAAA3g");
	this.shape_17.setTransform(-64.7,15.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgDBAIAAhYIAHAAIAABYgAgCgwIgCgCIgDgCIAAgDIAAgDIADgDIACgCIACAAIADAAIADACIABADIABADIgBADIgBACIgDACIgDABIgCgBg");
	this.shape_18.setTransform(-71.7,14);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgQArQgIgDgFgHQgFgFgDgJQgDgIAAgLQAAgKADgIQADgJAFgGQAFgFAIgEQAHgDAJAAQAKAAAHADQAIAEAFAFQAFAGADAJQADAIAAAKQAAALgDAIQgDAJgFAFQgFAHgIADQgHADgKAAQgJAAgHgDgAgNgjQgGACgEAGQgEAFgCAHQgCAIAAAHQAAAJACAIQACAHAEAFQAEAFAGADQAGACAHAAQAIAAAGgCQAGgDAEgFQAEgFACgHQACgIAAgJQAAgHgCgIQgCgHgEgFQgEgGgGgCQgGgDgIAAQgHAAgGADg");
	this.shape_19.setTransform(-78.8,16);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgmBAIAAh/IAhAAQAVAAALAKQAMAKAAASQAAAJgDAHQgDAHgGAEQgGAFgIADQgJACgJAAIgXAAIAAA0gAgcAFIAXAAQAIAAAGgCQAGgDAFgDQAFgEACgGQADgFAAgHQAAgOgJgIQgJgIgRAAIgXAAg");
	this.shape_20.setTransform(-88,14.1);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgZAtIAAhYIAFAAIACAAIABADIABATQAEgLAHgGQAGgHALAAIAHABQAEABADACIgBAFQAAABgBAAQAAAAAAABQAAAAgBAAQAAAAAAAAIgCAAIgCgBIgEgBIgFAAQgKAAgGAGQgGAHgEAMIAAA4g");
	this.shape_21.setTransform(83.8,-9.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgRAtIgHgEQgEgDgCgEQgCgFAAgGQAAgGAEgFQADgEAHgEQAHgEALgBQAJgCAQAAIAAgKQAAgMgGgHQgFgGgLAAQgFAAgFABIgHAEIgGAEIgDACQAAAAgBAAQAAAAAAAAQgBAAAAgBQAAAAgBAAIgBgDQAHgHAIgEQAHgDAJAAQAHAAAGACQAFACAEAFQADAEACAGQACAGAAAHIAAA6IgEAAQAAAAgBAAQAAgBgBAAQAAAAAAgBQgBAAAAgBIgBgLIgHAGIgIAFIgJADIgJABIgJgBgAADAEQgJABgGADQgGACgDAEQgCAEAAAFQAAAEABADQACAEACACIAGADIAHABIAIgBIAIgDIAHgFIAHgGIAAgXQgNAAgJACg");
	this.shape_22.setTransform(75.3,-9.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgGA4QgEgFAAgKIAAg9IgOAAIgBAAIgBgCIAAgDIAQgBIACggIABgCIABAAIAEAAIAAAiIAbAAIAAAGIgbAAIAAA8IABAGIACAFQAAAAAAABQABAAAAAAQABABAAAAQABAAAAAAIAFABIAGgBIAEgCIACgCIACgBIACABIACAEQgEAEgEACQgGACgFAAQgJAAgFgFg");
	this.shape_23.setTransform(67.8,-10.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgWA9QgKgFgGgHIADgEQAAgBAAAAQAAAAABAAQAAgBAAAAQABAAAAAAIACABIADADIAFADIAFAEIAIACQAEACAFAAQAIAAAFgDQAGgCAEgEQAFgEACgFQABgGAAgGQAAgHgCgEQgDgFgEgCIgKgFIgMgEIgLgDQgGgDgFgDQgEgEgDgGQgCgFAAgIQgBgHADgGQADgFAEgFQAFgFAHgCQAGgDAIAAQAKAAAIADQAIADAHAHIgDAEQAAAAAAAAQAAABgBAAQAAAAAAAAQgBAAAAAAIgDgBIgFgEIgIgEQgFgBgHAAQgGAAgEACQgGACgDADQgEADgCAFQgCAEABAFQgBAHADAEQADAEAEADIAKAFIALAEIAMAFQAGABAEAEQAFADADAFQACAGAAAIQAAAIgCAHQgDAHgFAFQgGAFgHADQgIADgJAAQgMAAgJgEg");
	this.shape_24.setTransform(60,-11.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgNArQgHgDgGgHQgFgGgDgIQgDgJAAgKQAAgKADgIQADgJAFgGQAFgFAIgEQAHgDAJAAQAHAAAHADQAGACAFAFQAFAFACAHQADAIAAAJIgBADIgCAAIg/AAIAAADQAAAIACAIQACAIAFAFQAEAFAGADQAGACAHAAQAHABAFgCIAIgDIAFgEIADgCIACABIACADIgGAFIgHAEIgKADIgKABQgIAAgIgDgAgJgkQgFACgEAEQgEAEgDAFQgCAGgBAHIA6AAQAAgHgCgGQgCgFgDgFQgEgDgFgCQgFgDgGAAQgGAAgGADg");
	this.shape_25.setTransform(47.2,-9.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AAbBBIAAg4QAAgNgGgGQgGgIgMABQgIAAgHAEQgIAEgGAJIAABBIgIAAIAAiBIAIAAIAAA4QAHgIAIgEQAIgEAIgBQAIABAFACQAGACAEAEQADAFACAGQACAFAAAJIAAA4g");
	this.shape_26.setTransform(37.8,-11.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgFA4QgGgFAAgKIAAg9IgMAAIgCAAIgBgCIAAgDIAPgBIACggIABgCIACAAIAEAAIAAAiIAbAAIAAAGIgbAAIAAA8IABAGIABAFQABAAAAABQABAAAAAAQABABAAAAQABAAABAAIAEABIAGgBIAEgCIADgCIACgBIABABIACAEQgDAEgGACQgFACgGAAQgHAAgFgFg");
	this.shape_27.setTransform(29.6,-10.7);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AglA9IAAh4IAFAAIACABIABABIABAOQAFgIAJgFQAHgEAKAAQAQAAAKALQAIAMABAWQgBAKgCAIQgCAIgFAGQgGAHgHADQgHAEgJAAQgIAAgHgDQgHgEgFgGIAAArgAgPgxQgIAFgFAIIAAAuQAFAIAHADQAGADAIAAQAGAAAHgDQAGgDAEgFQADgFADgIQACgGAAgJQAAgUgIgJQgHgJgOAAQgIAAgHAEg");
	this.shape_28.setTransform(18.6,-7.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgQArQgIgDgFgHQgFgFgDgJQgDgIAAgLQAAgKADgIQADgJAFgGQAFgFAIgEQAHgDAJAAQAKAAAHADQAIAEAFAFQAFAGADAJQADAIAAAKQAAALgDAIQgDAJgFAFQgFAHgIADQgHADgKAAQgJAAgHgDgAgNgjQgGACgEAGQgEAFgCAHQgCAIAAAHQAAAJACAIQACAHAEAFQAEAFAGADQAGACAHAAQAIAAAGgCQAGgDAEgFQAEgFACgHQACgIAAgJQAAgHgCgIQgCgHgEgFQgEgGgGgCQgGgDgIAAQgHAAgGADg");
	this.shape_29.setTransform(8.5,-9.2);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgZAtIAAhYIAFAAIACAAIABADIABATQAEgLAHgGQAGgHALAAIAHABQAEABADACIgBAFQAAABgBAAQAAAAAAABQAAAAgBAAQAAAAAAAAIgCAAIgCgBIgEgBIgFAAQgKAAgGAGQgGAHgEAMIAAA4g");
	this.shape_30.setTransform(0.5,-9.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgbA3QgJgMAAgWQAAgKACgJQADgHAFgHQAEgGAHgEQAIgDAJAAQAJAAAGADQAHADAFAHIAAg1IAJAAIAACCIgGAAQAAAAAAAAQgBAAAAgBQAAAAgBAAQAAgBAAgBIgBgNQgGAHgIAFQgIAFgJAAQgQAAgJgLgAgKgPQgGACgEAGQgEAFgCAGQgCAIAAAJQAAATAHAKQAIAJANAAQAIAAAIgEQAHgFAGgIIAAguQgFgIgHgDQgGgEgIAAQgHAAgGAEg");
	this.shape_31.setTransform(-8.8,-11.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgcA3QgIgMgBgWQAAgKADgJQACgHAFgHQAFgGAIgEQAHgDAJAAQAIAAAHADQAHADAFAHIAAg1IAJAAIAACCIgGAAQAAAAAAAAQgBAAAAgBQAAAAgBAAQAAgBAAgBIgBgNQgGAHgIAFQgIAFgJAAQgQAAgKgLgAgKgPQgGACgEAGQgEAFgCAGQgCAIAAAJQAAATAHAKQAIAJANAAQAIAAAIgEQAHgFAGgIIAAguQgGgIgGgDQgGgEgIAAQgHAAgGAEg");
	this.shape_32.setTransform(-22,-11.2);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AAbAtIAAg3QAAgOgGgGQgGgIgMABQgIAAgHAEQgIAEgGAJIAABBIgIAAIAAhYIAEAAQABAAAAAAQABAAAAABQABAAAAAAQAAABAAAAIABANQAGgHAIgEQAIgGAJAAQAIABAFACQAGACAEAEQADAFACAGQACAGAAAJIAAA3g");
	this.shape_33.setTransform(-31.4,-9.3);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgRAtIgHgEQgEgDgCgEQgCgFAAgGQAAgGAEgFQADgEAHgEQAHgEALgBQAJgCAQAAIAAgKQAAgMgGgHQgFgGgLAAQgFAAgFABIgHAEIgGAEIgDACQAAAAgBAAQAAAAgBAAQAAAAAAgBQgBAAAAAAIgBgDQAHgHAIgEQAHgDAJAAQAHAAAGACQAFACAEAFQADAEACAGQACAGAAAHIAAA6IgEAAQAAAAgBAAQAAgBgBAAQAAAAAAgBQgBAAAAgBIgBgLIgHAGIgIAFIgJADIgJABIgJgBgAADAEQgJABgGADQgGACgDAEQgCAEAAAFQAAAEABADQACAEACACIAGADIAHABIAIgBIAIgDIAHgFIAHgGIAAgXQgNAAgJACg");
	this.shape_34.setTransform(-40.9,-9.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgSA8QgGgCgFgDQgFgDgDgFQgCgEAAgFQAAgHAEgFQAFgFAJgDQgFgBgDgDQgCgDAAgFIAAgEIADgDIADgEIAFgDQgHgEgDgGQgEgGAAgJQAAgGADgFQABgGAFgDQAEgEAGgCQAGgCAHAAQAFAAAFABIAJAEIAXAAIAAADQAAABAAAAQAAABgBAAQAAAAAAAAQgBABgBAAIgNABIADAHIACAJQAAAHgDAFQgCAFgEAEQgEAEgGACQgGACgGAAQgIAAgGgCIgGAFQgCACAAADQAAADADADQADACAEAAIAKACIALAAIALABIAKADQAFACACAEQADAEAAAGQAAAFgDAFQgCAFgGAEQgFAEgHADQgIACgIAAQgJAAgIgCgAgWAXIgFAEIgDAEIgBAGQAAAEACAEQABADAEACIAKAEQAFACAIAAQAGAAAFgCQAHgBAEgDQAEgDADgEQADgEAAgEQAAgEgDgDQgCgDgDgBIgJgCIgKgBIgKAAIgKgCIgGAEgAgMg1QgEABgDADQgEADgCAEQgBAFAAAFQAAAFABAEQACAEAEADQADADAEABQAFACAFAAQAFAAAEgCQAFgBADgDQADgDABgEQACgEAAgFQAAgFgCgFQgBgEgDgDQgDgDgFgBQgEgCgFAAQgFAAgFACg");
	this.shape_35.setTransform(-52.9,-7.6);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgRAtIgHgEQgEgDgCgEQgCgFAAgGQAAgGAEgFQADgEAHgEQAHgEALgBQAJgCAQAAIAAgKQAAgMgGgHQgFgGgLAAQgFAAgFABIgHAEIgGAEIgDACQAAAAgBAAQAAAAAAAAQgBAAAAgBQgBAAAAAAIgBgDQAHgHAIgEQAHgDAJAAQAHAAAGACQAFACAEAFQADAEACAGQACAGAAAHIAAA6IgEAAQAAAAgBAAQAAgBgBAAQAAAAAAgBQgBAAAAgBIgBgLIgHAGIgIAFIgJADIgJABIgJgBgAADAEQgJABgGADQgGACgDAEQgCAEAAAFQAAAEABADQACAEACACIAGADIAHABIAIgBIAIgDIAHgFIAHgGIAAgXQgNAAgJACg");
	this.shape_36.setTransform(-62.2,-9.2);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AgZAtIAAhYIAFAAIACAAIABADIABATQAEgLAHgGQAGgHALAAIAHABQAEABADACIgBAFQAAABgBAAQAAAAAAABQAAAAgBAAQAAAAAAAAIgCAAIgCgBIgEgBIgFAAQgKAAgGAGQgGAHgEAMIAAA4g");
	this.shape_37.setTransform(-69.1,-9.3);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("Ag0BAIAAh/IAuAAQAMAAAMAFQALAEAIAJQAHAIAFAMQAFALAAAOQAAAPgFAMQgFALgHAJQgIAIgLAEQgMAFgMAAgAgrA4IAlAAQAKAAAJgDQAKgEAHgHQAHgIADgKQAEgLAAgNQAAgMgEgLQgDgKgHgHQgHgIgKgDQgJgEgKAAIglAAg");
	this.shape_38.setTransform(-79.5,-11.1);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FBB040").s().p("A24GZIAAsxMAn9AAAIF0GYIl0GZg");

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-146.4,-40.8,293,81.8);


(lib.light = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// pur2
	this.instance = new lib.pur2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(58.8,-106,1,1,0,0,0,0,-80.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:0.1,rotation:2.7,x:58.9},24).to({regX:0,rotation:0,x:58.8},25).to({rotation:-3.2,x:58.9},25).to({rotation:0,x:58.8},26).wait(1));

	// or1
	this.instance_1 = new lib.or1("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(-58.8,-103.3,1,1,0,0,0,-0.1,-104.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({rotation:2.5,y:-103.4},24).to({rotation:0,y:-103.3},25).to({rotation:-2,y:-103.4},25).to({rotation:0,y:-103.3},26).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-97.2,-114.4,194.6,227.9);


(lib.hand = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.hd("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(-2,25.8,1,1,0,0,0,-2,25.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:-15,x:-1.9},4).to({rotation:0,x:-2},5).to({rotation:3.9},3).to({rotation:0},4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.3,-29,30.7,58.1);


(lib.eyes = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AB5AvQg+gQgdAGQgeAFAHg1QAGg3ALgOQALgPAwgHQAwgHAcAeQAdAegBAcIgCA3QgBASgbAAQgOAAgWgFgAhWAmQg0gJgbAEQgaADAGgxQAHgyAHgQQAIgQAwgHQAvgHAcAeQAbAegGAaIgKAvQgEAQgcAAQgLAAgOgCg");
	var mask_graphics_4 = new cjs.Graphics().p("AB5AvQg+gQgdAGQgeAFAHg1QAGg3ALgOQALgPAwgHQAwgHAcAeQAdAegBAcIgCA3QgBASgbAAQgOAAgWgFgAhWAmQg0gJgbAEQgaADAGgxQAHgyAHgQQAIgQAwgHQAvgHAcAeQAbAegGAaIgKAvQgEAQgcAAQgLAAgOgCg");
	var mask_graphics_5 = new cjs.Graphics().p("AB5A/Qg+gQgdAGQgeAGAHg2QAGg2ALgPQALgPAwgHQAwgHAcAfQAdAegBAbIgCA3QgBATgbAAQgOAAgWgGgAhWA2Qg0gIgbADQgaAEAGgyQAHgxAHgQQAIgRAwgHQAvgHAcAeQAbAegGAbIgKAvQgEAQgcAAQgLAAgOgDg");
	var mask_graphics_6 = new cjs.Graphics().p("AB5BJQg+gQgdAGQgeAFAHg2QAGg2ALgOQALgPAwgHQAwgHAcAeQAdAegBAbIgCA4QgBASgbAAQgOAAgWgFgAhWBAQg0gJgbAEQgaADAGgyQAHgxAHgQQAIgQAwgHQAvgHAcAeQAbAegGAZIgKAwQgEAQgcAAQgLAAgOgCg");
	var mask_graphics_7 = new cjs.Graphics().p("AB5BJQg+gQgdAGQgeAFAHg2QAGg2ALgOQALgPAwgHQAwgHAcAeQAdAegBAbIgCA4QgBASgbAAQgOAAgWgFgAhWBAQg0gJgbAEQgaADAGgyQAHgxAHgQQAIgQAwgHQAvgHAcAeQAbAegGAZIgKAwQgEAQgcAAQgLAAgOgCg");
	var mask_graphics_8 = new cjs.Graphics().p("AB5BJQg+gQgdAGQgeAFAHg2QAGg2ALgOQALgPAwgHQAwgHAcAeQAdAegBAbIgCA4QgBASgbAAQgOAAgWgFgAhWBAQg0gJgbAEQgaADAGgyQAHgxAHgQQAIgQAwgHQAvgHAcAeQAbAegGAZIgKAwQgEAQgcAAQgLAAgOgCg");
	var mask_graphics_9 = new cjs.Graphics().p("AB5BJQg+gQgdAGQgeAFAHg2QAGg2ALgOQALgPAwgHQAwgHAcAeQAdAegBAbIgCA4QgBASgbAAQgOAAgWgFgAhWBAQg0gJgbAEQgaADAGgyQAHgxAHgQQAIgQAwgHQAvgHAcAeQAbAegGAZIgKAwQgEAQgcAAQgLAAgOgCg");
	var mask_graphics_10 = new cjs.Graphics().p("AB5BJQg+gQgdAGQgeAFAHg2QAGg2ALgOQALgPAwgHQAwgHAcAeQAdAegBAbIgCA4QgBASgbAAQgOAAgWgFgAhWBAQg0gJgbAEQgaADAGgyQAHgxAHgQQAIgQAwgHQAvgHAcAeQAbAegGAZIgKAwQgEAQgcAAQgLAAgOgCg");
	var mask_graphics_11 = new cjs.Graphics().p("AB5A/Qg+gQgdAGQgeAGAHg2QAGg2ALgPQALgPAwgHQAwgHAcAfQAdAegBAbIgCA3QgBATgbAAQgOAAgWgGgAhWA2Qg0gIgbADQgaAEAGgyQAHgxAHgQQAIgRAwgHQAvgHAcAeQAbAegGAbIgKAvQgEAQgcAAQgLAAgOgDg");
	var mask_graphics_12 = new cjs.Graphics().p("AB5AvQg+gQgdAGQgeAFAHg1QAGg3ALgOQALgPAwgHQAwgHAcAeQAdAegBAcIgCA3QgBASgbAAQgOAAgWgFgAhWAmQg0gJgbAEQgaADAGgxQAHgyAHgQQAIgQAwgHQAvgHAcAeQAbAegGAaIgKAvQgEAQgcAAQgLAAgOgCg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:1.1,y:-10.4}).wait(4).to({graphics:mask_graphics_4,x:1.1,y:-10.4}).wait(1).to({graphics:mask_graphics_5,x:1.1,y:-8.8}).wait(1).to({graphics:mask_graphics_6,x:1.1,y:-6.4}).wait(1).to({graphics:mask_graphics_7,x:1.1,y:-3.1}).wait(1).to({graphics:mask_graphics_8,x:1.1,y:0.2}).wait(1).to({graphics:mask_graphics_9,x:1.1,y:-3.1}).wait(1).to({graphics:mask_graphics_10,x:1.1,y:-6.4}).wait(1).to({graphics:mask_graphics_11,x:1.1,y:-8.8}).wait(1).to({graphics:mask_graphics_12,x:1.1,y:-10.4}).wait(28));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFC49E").s().p("ABnBBQgdgDgWgKQgWgLAAgjQAAgiARgQQARgQAnAHQAmAIAKAHQAKAIACAcQABAcgQAVQgNATgZAAIgHgBgAheAzQgcgDgPgJQgOgJgIgeQgHgeAMgUQALgUAiAHQAhAHAMANQALAOADAYQADAWgKASQgIAQgWAAIgHAAg");
	this.shape.setTransform(0,-0.1);

	var maskedShapeInstanceList = [this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(40));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#401304").s().p("Ag9AjQgEgKABgJQAAgMAGgOQAGgRAJgHQASgNAyAKQATAEAKALQAMALAAAQQAAAigHgCQgDgCgBgLQgBgJABgKQAAgPgLgIQgKgHgYgFQgmgIgSAXQgJALgBAXIAAAXIgBAAQgCAAgCgHg");
	this.shape_1.setTransform(9.6,-1.3);

	this.instance = new lib.ClipGroup_0_1();
	this.instance.parent = this;
	this.instance.setTransform(9.9,0.8,1,1,0,0,0,6.4,5.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgDA1QgWgDgKgFIgNgHQgIgEgBgIIgEgaQgBgeALgMQAOgPAlADQAnAEANAUQAOAVgFAZQgFAXgQALQgJAFgPAAQgJAAgKgCg");
	this.shape_2.setTransform(9.6,0.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#401304").s().p("AAwAuQADgpgLgQQgJgNgQgIQgPgHgPACQgeADABAbQAGAkgEACQgHAEgEgkQgCgRAHgMQAIgNAPgCQAQgCASAEQATAFAJAIQAIAHAFARQAFAOAAAMQAAAJgDAKQgCAHgBAAIgBAAg");
	this.shape_3.setTransform(-10.6,-1.7);

	this.instance_1 = new lib.ClipGroup_5();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-10.3,-0.4,1,1,0,0,0,5.4,5.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAOAzIgfgEQgYgGgBgNIgHgyQADgjAuAGQAoAGAJAnIABAlQgGAPgHAEQgEADgHAAIgMgCg");
	this.shape_4.setTransform(-10.7,-0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_4},{t:this.instance_1},{t:this.shape_3},{t:this.shape_2},{t:this.instance},{t:this.shape_1}]}).wait(40));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-16.3,-6.6,32.6,13);


(lib.enterans = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_30 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(30).call(this.frame_30).wait(1));

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#FF0000").ss(1,1,1).p("ABLAAQAAAegWAVQgWAWgfAAQgeAAgWgWQgWgVAAgeQAAgdAWgVQAWgWAeAAQAfAAAWAWQAWAVAAAdg");
	this.shape.setTransform(287.6,-157.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("Ag0AzQgWgVAAgeQAAgdAWgWQAWgUAegBQAfABAWAUQAWAWAAAdQAAAegWAVQgWAWgfgBQgeABgWgWg");
	this.shape_1.setTransform(287.6,-157.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(31));

	// Layer 1
	this.instance = new lib.enterans2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(282.5,-249.5,0.137,0.137);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.58,scaleY:0.58,y:-269.5,alpha:1},6).wait(18).to({startPosition:0},0).to({scaleX:0.14,scaleY:0.14,y:-249.5,alpha:0},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(258.4,-254.9,48.7,105.4);


(lib.drag1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.ClipGroup_4();
	this.instance.parent = this;
	this.instance.setTransform(0.2,-17,1,1,0,0,0,167.7,167.7);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#92327A").s().p("AgWA2QgKgEgHgGIAFgHIACgDIADAAIAFABIAEADIAIAEQAFABAGAAQAFAAAFgBQAEgCACgCIAFgGIACgHQgBgFgCgDQgDgDgDgCIgKgEIgJgDIgLgEIgJgFQgFgDgCgFQgCgFAAgHQAAgGACgGQADgGAFgFQAEgEAIgDQAIgCAIAAQALAAAIADQAJAEAHAGIgFAHQAAAAAAABQgBAAAAABQgBAAAAAAQgBAAAAAAIgFgBIgEgDIgIgDIgKgBIgIABIgHAEIgDAFQgCADAAADQAAAFADACQACADAEACIAIAEIALADIALAFQAEACAEACQAEADADAFQACAFAAAHQAAAHgCAHQgDAGgFAFQgGAFgIADQgHACgKAAQgLAAgJgDg");
	this.shape.setTransform(111.3,94.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#92327A").s().p("AgLBEQgGgIgBgOIAAhDIgNAAIgDgBIgBgEIAAgHIASgDIAFgiIABgDIADAAIAJAAIAAAmIAgAAIAAAOIggAAIAABBQAAAIAEADQADAEAFAAIAFgBIAFgCIACgCIACgBQABAAAAABQABAAAAAAQAAAAAAAAQABABAAAAIAGAJQgFAFgIADQgHACgHAAQgMAAgIgGg");
	this.shape_1.setTransform(102.5,92.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#92327A").s().p("AAaA5IAAhGQAAgNgFgHQgGgHgMAAQgIAAgHAEQgHAEgHAIIAABRIgUAAIAAhvIAMAAQAEAAACAEIABAMQAHgIAKgFQAIgEALgBQAJAAAHAEQAHADAFAFQAEAFADAJQACAHAAAKIAABGg");
	this.shape_2.setTransform(92.4,94.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#92327A").s().p("AgJBQIAAhuIATAAIAABugAgEg0IgFgDIgDgFIgBgFIABgGIADgEIAFgDIAEgBIAFABIAFADIADAEIABAGIgBAFIgDAFIgFADIgFABIgEgBg");
	this.shape_3.setTransform(83.3,91.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#92327A").s().p("AgVA1QgKgEgHgHQgHgIgEgKQgDgLAAgNQAAgMADgLQAEgKAHgIQAHgHAKgEQAKgEALAAQAMAAAKAEQAKAEAHAHQAHAIADAKQAEALAAAMQAAANgEALQgDAKgHAIQgHAHgKAEQgKAEgMAAQgLAAgKgEgAgNgmQgGADgEAFQgFAFgCAIQgCAIAAAJQAAAKACAIQACAIAFAFQAEAFAGADQAGADAHAAQAQAAAJgLQAIgLAAgUQAAgTgIgLQgJgLgQAAQgHAAgGADg");
	this.shape_4.setTransform(74.4,94.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#92327A").s().p("AgzBPIAAidIAuAAQAOAAALADQALAEAHAGQAIAHADAJQAEAJgBALQABALgEAJQgEAJgHAGQgIAGgLAEQgLAEgNAAIgaAAIAAA7gAgfADIAaAAQAIAAAHgDQAHgBAFgEQAEgFADgGQACgGAAgHQAAgPgJgJQgJgIgSAAIgaAAg");
	this.shape_5.setTransform(62.6,91.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#92327A").s().p("AghA5IAAhvIAMAAIADABQABABAAAAQAAABABAAQAAABAAAAQAAABAAABIACARQAFgMAJgGQAHgHALAAIAJABIAHADIgCAPQgBAAAAABQAAABgBAAQAAAAgBABQAAAAgBAAIgFgBIgJgBQgKAAgGAFQgGAGgFAMIAABGg");
	this.shape_6.setTransform(47.1,94);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#92327A").s().p("AgYA4QgFgCgFgDQgEgEgDgGQgCgGAAgHQAAgHAEgFQADgHAJgEQAHgFANgCQAMgEAUAAIAAgIQAAgNgGgHQgFgHgMAAQgFAAgFACIgJAEIgGAFIgFABIgEgBIgCgDIgDgFQAIgKAMgEQAKgFAMABQAKAAAGADQAIACAFAGQAFAGACAHQADAIAAAJIAABHIgJAAIgFgBQAAAAAAgBQgBAAAAAAQAAgBAAAAQAAgBgBAAIgCgLIgIAHQgFADgEACIgJADIgMABQgGAAgGgBgAACAIQgJACgGACQgFADgEADQgCAEAAAFQAAAEABAEIAEAFIAGADIAHABIAIgBIAIgDIAIgEIAHgHIAAgXQgOAAgJACg");
	this.shape_7.setTransform(36.3,94.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#92327A").s().p("AgLBEQgHgIAAgOIAAhDIgNAAIgDgBIgBgEIAAgHIASgDIAFgiIABgDIADAAIAJAAIAAAmIAgAAIAAAOIggAAIAABBQAAAIAEADQADAEAFAAIAFgBIAFgCIACgCIACgBQABAAAAABQABAAAAAAQAAAAAAAAQABABAAAAIAGAJQgFAFgHADQgIACgHAAQgMAAgIgGg");
	this.shape_8.setTransform(26.8,92.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#92327A").s().p("AgdBLQgMgGgJgJIAGgKIACgCIADgBQADAAADADIAHAFIALAFQAGADAJAAQAHAAAFgCQAHgCADgEQAFgEACgFQACgGAAgGQAAgHgDgFQgEgEgEgDIgMgFIgOgFIgOgFQgGgCgGgFQgFgEgDgIQgEgGABgLQgBgJAEgHQADgIAGgGQAHgGAJgEQAJgDAKAAQANAAAMAEQAKAEAJAIIgGAKIgCADIgCAAIgFgBIgGgFIgJgEQgGgCgHAAQgGAAgGACQgFACgEADQgEAEgCAEQgCAFAAAEQAAAIADADQAEAFAFADIAMAGIANAEIAOAFQAHAEAFAEQAFAEAEAGQADAIAAAJQAAAKgDAJQgEAJgHAIQgGAGgLAEQgJADgLAAQgQABgNgGg");
	this.shape_9.setTransform(16.9,91.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#92327A").s().p("AAbA5IAAhGQgBgNgFgHQgGgHgMAAQgHAAgIAEQgIAEgGAIIAABRIgTAAIAAhvIALAAQAFAAABAEIABAMQAIgIAJgFQAIgEALgBQAJAAAHAEQAHADAFAFQAEAFADAJQADAHgBAKIAABGg");
	this.shape_10.setTransform(0.9,94.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#92327A").s().p("AgVA1QgKgEgGgHQgIgIgDgKQgEgLAAgNQAAgMAEgLQADgKAIgIQAGgHAKgEQAKgEALAAQANAAAKAEQAJAEAHAHQAHAIAEAKQADALAAAMQAAANgDALQgEAKgHAIQgHAHgJAEQgKAEgNAAQgLAAgKgEgAgNgmQgGADgFAFQgEAFgCAIQgCAIAAAJQAAAKACAIQACAIAEAFQAFAFAGADQAGADAHAAQARAAAIgLQAIgLAAgUQAAgTgIgLQgIgLgRAAQgHAAgGADg");
	this.shape_11.setTransform(-11.5,94.1);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#92327A").s().p("AgXA2QgJgEgGgGIAEgHIACgDIAEAAIADABIAGADIAIAEQADABAIAAQAEAAAFgBQAEgCADgCIAEgGIABgHQAAgFgCgDQgDgDgEgCIgJgEIgKgDIgKgEIgJgFQgFgDgDgFQgBgFAAgHQgBgGADgGQADgGAEgFQAFgEAIgDQAHgCAJAAQALAAAIADQAKAEAFAGIgDAHQgBAAAAABQgBAAAAABQgBAAAAAAQgBAAgBAAIgDgBIgGgDIgHgDIgKgBIgIABIgGAEIgFAFQgBADAAADQAAAFACACQADADAEACIAIAEIALADIAKAFQAGACADACQAEADADAFQACAFAAAHQAAAHgCAHQgDAGgGAFQgFAFgHADQgJACgIAAQgMAAgKgDg");
	this.shape_12.setTransform(-22.5,94.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#92327A").s().p("AAeBRIgEgBIgDgDIgkgtIgDgEIgFgBIgGAAIAAA2IgTAAIAAihIATAAIAABfIAFAAIAEgBIADgDIAiglIAEgCIAEgBIASAAIgpAqIgDADIgDADIADADIADAEIArA3g");
	this.shape_13.setTransform(-32.3,91.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#92327A").s().p("AgOA1QgJgDgHgIQgHgHgDgLQgEgKAAgOQAAgLADgLQAEgLAGgHQAHgIAKgEQAKgEALAAQAMAAAJADQAJAEAHAHIgFAHIgCACIgCABIgEgCIgFgDIgHgDQgEgBgHAAQgIAAgFADQgGADgEAFQgFAGgCAHQgCAIAAAJQAAAKACAIQADAIAEAFQAEAGAGACQAFADAHAAQAIAAAEgCIAIgDIAFgEQABgBAAAAQAAAAABAAQAAgBABAAQAAAAABAAQAAAAABAAQAAAAABABQAAAAABAAQAAAAABABIAFAHQgHAJgLAEQgLAEgMAAQgJAAgJgEg");
	this.shape_14.setTransform(-43.6,94.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#92327A").s().p("AgYA4QgFgCgFgDQgEgEgDgGQgCgGAAgHQAAgHAEgFQADgHAJgEQAHgFAOgCQAMgEASAAIAAgIQABgNgGgHQgFgHgMAAQgGAAgEACIgJAEIgGAFIgFABIgEgBIgCgDIgEgFQAKgKALgEQAKgFAMABQAJAAAHADQAIACAFAGQAFAGACAHQADAIAAAJIAABHIgJAAIgFgBQAAAAAAgBQgBAAAAAAQAAgBAAAAQAAgBAAAAIgDgLIgIAHQgFADgEACIgJADIgMABQgGAAgGgBgAACAIQgJACgGACQgGADgDADQgCAEAAAFQAAAEABAEIAEAFIAGADIAHABIAIgBIAJgDIAHgEIAGgHIAAgXQgNAAgJACg");
	this.shape_15.setTransform(-54.7,94.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#92327A").s().p("AgjBMIABgGIABgGIABgDIADgBIAGABIAIABQAHAAAGgDQAFgCADgEQAFgFABgHQACgHAAgKIAAhmIAWAAIAABlQAAAOgEAKQgDALgGAHQgGAHgKAEQgHADgNAAQgLAAgLgDg");
	this.shape_16.setTransform(-65.6,91.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#92327A").s().p("AgQA1QgKgEgHgHQgHgIgEgLQgEgLAAgNQAAgMADgKQAEgKAHgIQAGgHAKgEQAKgEALAAQAKAAAIADQAJADAGAHQAGAGAEAJQADAKAAALIgBAHQAAAAAAAAQgBAAAAAAQgBAAAAAAQgBAAgBAAIhJAAQAAALADAHQADAIAEAFQAFAGAGACQAHADAHAAQAHAAAFgCIAKgDIAGgEIAEgCQABAAAAAAQABAAAAABQABAAAAAAQABAAAAABIAGAHQgEAFgFADIgLAFIgMADIgMABQgLAAgJgEgAgSgiQgIAIgCAPIA7AAQAAgHgBgGQgCgFgEgEQgDgFgGgCQgFgCgHAAQgNAAgIAIg");
	this.shape_17.setTransform(-79.9,94.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#92327A").s().p("AgIA4IgthvIAQAAQAAAAABAAQAAAAABABQABAAAAAAQAAAAABABIACACIAcBHIADAIIAAAIIACgIIADgIIAchHIACgDIAEgBIAPAAIgtBvg");
	this.shape_18.setTransform(-90.9,94.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#92327A").s().p("AgJBQIAAhuIATAAIAABugAgEg0IgFgDIgDgFIgBgFIABgGIADgEIAFgDIAEgBIAFABIAFADIADAEIABAGIgBAFIgDAFIgFADIgFABIgEgBg");
	this.shape_19.setTransform(-99.4,91.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#92327A").s().p("AgVBLQgPgGgKgKQgKgMgHgOQgFgPAAgSQAAgRAFgOQAHgPAJgMQALgKAPgGQAPgFASAAQAJAAAIABIAPAEIANAGQAFAEAGAFIgHAJQgBADgDAAIgFgBIgGgEIgIgEIgMgDQgGgCgJAAQgMAAgLAEQgLAEgHAIQgIAJgEAMQgEALgBANQABAPAEAMQAEALAIAJQAIAIAKAEQAKAFAMAAIAMgBIAKgCIAJgDIAJgDIAAgjIgZAAIgDgBIgBgEIAAgMIAwAAIAAA9QgGAEgGADQgHADgIACIgPADIgSABQgRABgNgGg");
	this.shape_20.setTransform(-110.4,91.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#3F8D3B").s().p("AgBAJQACgQABgNIABAoIgCABQgFAAADgMg");
	this.shape_21.setTransform(-149.9,7.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#3F8D3B").s().p("AAGgHIAAAIIgLAHg");
	this.shape_22.setTransform(-151.6,-32);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#3F8D3B").s().p("AAAgPIAEADIgHAcQABgYACgHg");
	this.shape_23.setTransform(-150.5,-80);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#3F8D3B").s().p("AgOgGQAFADAYgCIgVAMg");
	this.shape_24.setTransform(141.4,128);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#3F8D3B").s().p("AgRAAIANgDIAWAHg");
	this.shape_25.setTransform(134.8,129);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#3F8D3B").s().p("AgdAHQgLgHAKgDIAkgEQgGACAUACQAVACgHACQgZgCgFAFQgEAEgOAAIgPgBg");
	this.shape_26.setTransform(31.2,-118.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#3F8D3B").s().p("AAEgJIACATIgLAAg");
	this.shape_27.setTransform(92.2,-123.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#3F8D3B").s().p("AAAAUIgDgWIgBgUQAAgBABAAQAAAAAAABQABAAAAABQABABAAABIADAOIADALQgCASgBAAIgCgEg");
	this.shape_28.setTransform(152.4,-110);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#3F8D3B").s().p("AgFgKIALABIgEAJIgCALg");
	this.shape_29.setTransform(153.3,-96.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#3F8D3B").s().p("AACAUIgDgSIgEgVQAAgHADACIAGAYIACAYIgCACQgBAAgBgGg");
	this.shape_30.setTransform(155.3,-112.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#3F8D3B").s().p("AgBAQIACgrQACAAgBASIgBAlQgBgNgBABg");
	this.shape_31.setTransform(155.6,-60.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#3F8D3B").s().p("AgCgMIAHAPIgJAKg");
	this.shape_32.setTransform(155.5,-42.5);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#3F8D3B").s().p("AACAVQACgDgEgDQgFgDABgHQgEgwAJAkQAIAegEAAIgDgCg");
	this.shape_33.setTransform(160.2,-72.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#3F8D3B").s().p("AAAANIAAgZIABATQABAGgCAAIAAAAg");
	this.shape_34.setTransform(157.4,-36.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#3F8D3B").s().p("AgDgIQAAgQAEgDQAEAeAAAJQAAAQgHgFIAAAFIgBgkg");
	this.shape_35.setTransform(157.5,-40.7);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#3F8D3B").s().p("AgCgBQABgSABAHQACAHABATIgDABQgBgMgBgEg");
	this.shape_36.setTransform(155.2,-12.9);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#3F8D3B").s().p("AABAOQAAABgBAAQAAABAAAAQgBAAAAAAQgBAAAAAAIACgNQABgJAAgLIACAkQAAgCAAgBQgBgBAAAAQAAgBAAAAQgBAAAAAAg");
	this.shape_37.setTransform(160.5,-49.1);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#3F8D3B").s().p("AgLgCQAKADANACQgYgBABgEg");
	this.shape_38.setTransform(97.7,-119.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#3F8D3B").s().p("AAAgCQABADAEACQgOgDAJgCg");
	this.shape_39.setTransform(-112.7,-119.9);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#3F8D3B").s().p("AvqUFQAfgHgxgEQgCAEgbAGQgeAGgPgFQAdAAAEgEQADgEgOgDIg8gBIAUAOQhCAAjFgFIhEgCQgWgBgVgHQgXgHgQgMQglgZgSgoQgJgVgDgVIgCgVIAAgSQAGABACg/QABg8ALAJIgCAHQABAhAHgbQAGgcgBgcQgCAMgEgFIgGgPQgIgUgCA0IgJhIQgUgWAHhnQAJiGgEgqQABAIADACQgHg4AHhcQAGhVgCgIIAKgIQgMhAgBgaQgCgeAFhIIADAEQACACABgJQgHgYgDgSQgEgdABgtIAEADIgChpQAOAAgIhfQgJhnARgQQgFgNgEgwIgHhIQADAAABgIQgJgYAGg4QAHhIAAgLQgDgUgEACQgCABgGANQAIghgJhIQgJhGABgHQABgZAHAAQAKgBABgCQgCgcgMAAQgIAAAGguIACARQACAJABgFQABgMASgxQAPgsgEgSIgFAWQgEgcALhCQAIgzgLgFIgKAFIAJg4IAIgZQAGgPAGgIQAHgLAJgHIANgLIASgLIAAADQAZgRAigBIAWgCQAJgBAIgGIgQgCIB2ABQAHgCAOAAQAQgBAGACQACgLCbAGQgdACACADIARAIQAUgIARgBQARgBAbAGQAGgQAMgHQATgLAxAAIgSADQgIACAEACQAIAEAWgGQADACgHADQgIAEAEADQAcgJAqACQA8ACAagCQgOADAFAEIAOAHQADgJAsAHQAlAGgOgMQBEAHCoABQCRABAcAHQAIAEAbgDIAqgFIgDAIQAjgIAbgCQAggDAngHQgXAGAFAFQAEAEAcAGIA3gQIAKAMQAYgFAwAEIgHABIACACIAFgDQANgHAsgFQA7gFAFgDQAMAJAggDQAegCAJAIQAMgEASgCQgKgGAAgEQABgFAPADIAFAAIAKAJQAngDAWAAQgNgCgKgDIAbAFIAIAAIgFAAQAYAFAYgFQAlAHBbgJQBYgIAuAKQgegEgGAFQAegBBLAGQA8AEAagEIgEgJQBegHAsABIACgKIB/gCQBVgCAXAMQAJgEAsACQA3ADAOgBIAJAIQAnACBAgFIAWAEIAYAGQAWAIAUAMQAnAZAXAoQAJAQAIAaIAEAWIABATIACA/IgDABIAGBTIgCgGIABAJQAFAYgBAOQAGAqgJA7IgBA1QgDgjgGAQQgJAagCABIAHAiQgCAGgDgJQgDgHgBANQACAHAEAxQADArAFAMQADgFADgRIAAgCIAAABIABgCIAAADQABAGACADQgBAbACACQABABAEgFQgHAWgDAmQgCAdgHASQAHgPAAA0QAAA+AJgQQgIATAAAKQAAAIgFALQACgggGgqIgLhDIgJAoQgJgPAFgrQAHgugCgJQACAMgBAUQgBAQgDAEIALgKQAEglgFgoIgKhBQgCASgEgXQgFgcgFAPIACAeQgDAIgDgNIgGgcQAFgtAAiBQABhvAFgRQgCANABAYQABAUADAPIAFgtQAAANgEAoQgFAjADAeQABgCAMAJQAKAGAGgYQAGg0gLgrQgOg0ACg8QACABADAAQAGgBgDgoIgGghQgGgRgIgMQgOgWgXgNQgRgJgOABQgMAAgCAHQAJACAQAIQAOAJAKANQAKAMADANQADAMgEAEQgJgagTgRQgJgIgIgDQgIgEgDADQgEgJAEABIAHACIASANQABAAAAAAQAAAAAAAAQABAAAAgBQAAAAAAgBIgFgJQgDgDgJgFIgJgDQgJgCgHAAQAKgBgMgIIgUgKIhlgEIhrgEQgOgGADgEQggADggAMQggANALAGQgWAAgbgCQgVgCgWACIAZAFIgIAAQgigBgDAHQgYABgCgEQgCgGgGgBQgYAFgBAFQgNgFAYgOQAYgOgQgGQgNgEgzgJIg2gJQgPAJAFAbQAEAbAWALQgBAFgWgGQgdgIgGAAQgegHglAGQgpAIgSgCIAigHQgGgDgjADQgjACgIADIAOgOQAMgLgMgDQg4gCg0AOQgvANAEALIB6gOQACAFgrAGQgrAGACAHIhxgEQAQAAARgCQATgDgLgEQgcAAgHgCIgWgGQAogKBAgWQA9gWAcgOIg6AIIgOgHQgxAGg3AaQgTAJgqAYQgdgCAAgNQgBgMAMgFQgqAAgrAQQgqARgtgCIAhAFQgJABgtgBQglgBAEADQgOgGgGgIQgGgIAJgDIhIAVQAFgCABgJQAAgMABgBQgWgBgNAMQgMAKANAEIgxgKQgOgDgcgJQgNAAAIAKQAIAKALACQgoAFgrgEQAogFgEgWIgGgRQgDgJAEgFIiPA5QAEgCAGgKQAGgKgBgCQgIAMheAEQhNAEhMgDIAMAAQAMgEgkgDQgTgBgPAAQgagFgjAMQgjAMgcgHQA+ACgFgOIhQAEQgvADAMAMIgigEQgOgCABgGIhjAMQAHgCgBgDQgDgDgRgBQAXAAA3gFQArgFAQAHIgGgQQgCgHAQgHQhDAEhCAOQhEARgiAHIhfgJIgEAGIgQgPQgUAGgCgBQgDgCAJgDQgUACgEABQgEgIAjgQIAaAIQAQgGADgEQAEgGgRgDIgNADQAUgJgKgDIhIAHIAiALQguAKgOAFQgXAJAbAGIg3gIQgsAJgRADIhIAJQgCgCAHgFQAFgDgHgCQgUAFgbgBQgRgBgegFIgSgDIgOAAQgNABgCAHQgQAJABAJIABAGQgBADgDAFQgCgFgEAEQgEAOABAKQgBABAAAAQgBAAAAAAQAAAAgBAAQAAgBAAAAIgBgGQAAgIgJAOIgBAQIAFA3QACAogDAmQABArAKgVQAEgLACAAQACAAgBAXQgJALgDAHQgHAPAAAcIAIAXQAFAMADAWIgNgCIAAAtQgBASgGABQAHAigEAdQAEAiAMgjIAPgyQgEARgEAzQgDAigJARIgDgvQgIAxAHApQAFAbgNAXQABgbgFgUQgEgWgFAOIAGAyQgBACgBgLQgCgJgCAMQAFAcAJAcIgBgBIACBPQAAgBAAgBQAAgBgBAAQAAgBAAAAQAAgBgBAAQgCgCgBANIAHA6QgCgMgDAJQgDAJgBAVQgCAlANAcQALAYgGAdQgCgKABgIIgGAeQAOADABBYQABBZAOADQgHAYgCgNIgLg/IgHAtQABghgDgUQgHAmAFAtQADAUAJAsQgBAHgEABIgGAAQgHABACAmQADAzAKgSIAGgLQADgCABASIgOAoQAHArgCA0IgFBXQABASADAFQgDAiAHA+QAHA+gCAWQgCgGgEARIgHAWIAJBQQgDgTgFgIIgLgKQAAAPADARIADAaIgEgOQgDgEgDAJIAOBbQABgFABgUIACAwQABAkgEAUQgCgBgDgHIgEgJQAFgSgCgPQgCgIgGgPQABAjgCAAQgFgBgCALQAAAfAHAPQAFAOAGgKIgDA6QAAAYAGAiIgLgJIADAuIgNglQgEArAKALQAKALgDAyIgKAAQACAOANANQAKAJABAMQAHAFAAgEIgCgKQAAgBAAgBQAAgBAAgBQAAgBABAAQAAAAABABIAOAMQADAJAJAGQALAHALADQAGgBgBgDQgBgCgFgBIgMgGQAJACAGABIADAAIABAAIABABIAVACQAOADAVAHQASAHAOACQAWADAogFQgNgCgBgEIADgGQADgGg2AAQAUgDAwAGQArAGAagGIgfANQAJAAAkgGIgDAJQAbgIA1ADQAtADASgLQgNAJAdgDQApgDAKACQgWAGhCABQg1AAgDAJQALACAVgBQAVgCALACQgjAGgIAEQAtAGAugIIBGgLIhQADQAEgIATgBQAZAAAKgGQAWAFAggDQAjgDAOADIgOABQAwAAAoAMQArAOAKAAQAbABAggFQAggFgQgFIgpgBQg2gCAdgMQAZAMA6gGQBJgHA1AIQgYAEAPAHQAPAGgUADQAhgCAPgDIAcgHIAPAHIAugPQALgDA8gGQgSADAXAGQAWAFgMACQAtABAfgHIgEABQAwgCAsAEQAkAEAxgFIgJgEQBGgHCBAAQCggBAsgCIgtgEIA1gBQAxgDAXAEQgWADgHgBQABAFALAEQAOAEAVABIALgIIANAGQAXAAgHgFQgHgGAXAAQAOAMBlAAQBqABATAJQAQgIgsAAQgqgBARgHIATAEQANACATAAIgLgJIClAIQBPAEBLgBIgFACQA8AGAcgMQAEACgKAEQgJADAKACQAqgNBHALQBGAKAVgHQgDAIArAUQAkAQgXAIIAggCQgFAGgzAAQg1ABgKAGIhlgNQABACgEAGQgDAEAJABQgYABgFABQgHACAWAEQiGgRjoADQkKADhhgHQgLABACADIAIAFQgagIgOAGQgWAJgEABIhAgSQgQADADAFIAGAGQgzACh/gGQhrgFg1AGQgOgDABgHQABgFgWgBQAeAJhAADQg8ADAIAFQgTgIhCgEQg2gDAKgPQghgBgEAGQgEAGAOADQgzgBgcABQgoABggAGQgHgBiOAAQiFgBgegLQAUAIgOABIgsADIAfAMIhMgCQgUACAAgCQgEAAgDgEQgEgEgSgCQggAAgSAEQgSAFgOADIAHACIgjABQgDAAAdgHgAt+UBIAMACIAHgDIgDAAIgQABgA3yqmQAAAMAHAKQAEgGgCgRIgFgaIgEAbgAwNy2QAeAGAMgEQgQgCgOAAIgMAAgAohyvIAKAAIgTACQADgCAGAAg");
	this.shape_40.setTransform(-0.9,-0.5);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#3F8D3B").s().p("AALgPIAVAYQgTgCgKABQgPABgTAHg");
	this.shape_41.setTransform(-17.7,-120);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#3F8D3B").s().p("AgIABQAjgEggAFg");
	this.shape_42.setTransform(-107.5,121.2);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#3F8D3B").s().p("AgDAAIACAAIAFABIgHgBg");
	this.shape_43.setTransform(123.5,-122.8);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#3F8D3B").s().p("AAZACIgVgEQgMgCgGAAIgRABIgNgCIASgDQAQgBAKACQAWAEAXANIgUgIg");
	this.shape_44.setTransform(145.7,-127.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#3F8D3B").s().p("AAJAHQgSgVgVgOQAmAUAXAlQgFgCgRgUg");
	this.shape_45.setTransform(153.4,-123.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#3F8D3B").s().p("AgDgKQACABADgBQADAAAAAVg");
	this.shape_46.setTransform(-151,40.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#3F8D3B").s().p("AgNABIAbgEIgSAHg");
	this.shape_47.setTransform(-109.2,-123);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#3F8D3B").s().p("AgSAFIgJgHQgCgGASAFIAnAJQgYgDgWACg");
	this.shape_48.setTransform(29.7,-128.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#3F8D3B").s().p("AgBgDIADADIgBAEQgBAAgBgHg");
	this.shape_49.setTransform(-152.5,-50.4);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#3F8D3B").s().p("AgDAIIADgSIAEAVg");
	this.shape_50.setTransform(160.4,-32.4);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#3F8D3B").s().p("AgCAKIgDgcIALAlg");
	this.shape_51.setTransform(160.4,-24.1);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#3F8D3B").s().p("AAAAcQgBgGABgWIgFAAIAHggIAEBBQgEgCgCgDg");
	this.shape_52.setTransform(156.9,9.4);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#3F8D3B").s().p("AgFAqIgFgUIAEAFQACAEABgKQABgLgCgIIgDgJQAHgVALgOIgEAQQgHAagBAVIABAFQgCARgCAAIgBgBg");
	this.shape_53.setTransform(156.5,31.8);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#3F8D3B").s().p("AAHAQQgDAdgEgUQgDgNACAnQgBgJgGgPQgEgMAAgbQAFgOAGAPIAJAbIADADIgBgNQAAgEgCAOQgCgyACgYIAFBOIABgCIgFApIgCgrg");
	this.shape_54.setTransform(158.8,-6.8);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#3F8D3B").s().p("AgFAdIABAHIABhRQAEADAFAgQgIAigCAWg");
	this.shape_55.setTransform(160.1,-17.2);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#3F8D3B").s().p("AgTA5QAGgcgDgdQgDgcgHgDQABgIADAFIAEAGIgFgvQAIgGAIAmQAHAjAFgKQgCANgDAKQgDAMACAhQAEAVAFgeQAFgnACgGQAEAaADAIQABAFAEABQgMAdgPAJQgDADgEAAQgJAAgDgUg");
	this.shape_56.setTransform(156.9,-25.6);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#3F8D3B").s().p("AgCgOIAFAXIgDAGQgBgOgBgPg");
	this.shape_57.setTransform(159.9,11.8);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#3F8D3B").s().p("AAAgQIAAgEIAAABQAAAVACATQgEgUACgRg");
	this.shape_58.setTransform(159.4,8.2);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#3F8D3B").s().p("AAAABIABgCIAAADg");
	this.shape_59.setTransform(159.6,-22.1);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#3F8D3B").s().p("AgDAhQAAggADgmQAAAoADASQABAQgDABIgEgFg");
	this.shape_60.setTransform(159.8,2.9);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#3F8D3B").s().p("AgBgJIADAFIgBAOg");
	this.shape_61.setTransform(158.7,25.5);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#3F8D3B").s().p("AAAAQQgCADgBAAIACgnIAFApQgBgGgDABg");
	this.shape_62.setTransform(159,28.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#3F8D3B").s().p("AAAADQgFAAgDgFQAFgdAEAQQADAJAEAbQgEgTgEABg");
	this.shape_63.setTransform(158,38.7);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#3F8D3B").s().p("AgDAAIAHgOQgCATgFAKg");
	this.shape_64.setTransform(160.3,22.7);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#3F8D3B").s().p("AgGAjQAGgogDghQAJApABAbQgCgIgEALQgCAGgCAAQAAAAAAAAQgBgBAAAAQAAgBgBAAQAAgBgBgBg");
	this.shape_65.setTransform(162,5.3);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#3F8D3B").s().p("AgEgYIAAgNIAJBLg");
	this.shape_66.setTransform(158.6,53.4);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#3F8D3B").s().p("AgBgSIAEAlIgBAAQgEAAABglg");
	this.shape_67.setTransform(159.9,32.7);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#3F8D3B").s().p("AgFgFIAGgJQADgGACABQgDABgCAPIgCAXg");
	this.shape_68.setTransform(160.8,35.2);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#3F8D3B").s().p("AgCACIADgZQADgBgCAVQgBAWABAFg");
	this.shape_69.setTransform(156.3,78.8);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#3F8D3B").s().p("AgEgRQADAWACgGIAEgaQABAxgFAFIgBABQgDAAgBgtg");
	this.shape_70.setTransform(159.6,62);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#3F8D3B").s().p("AAAABQAAgBAAAAQAAAAAAAAQAAgBAAAAQAAAAABABQgEgIAAgEIADgGIAEAlIgEgSg");
	this.shape_71.setTransform(159.9,55.2);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#3F8D3B").s().p("AgFgIQAFgRABAAQADAAACAZQgCgGgBAMQgCARgBADg");
	this.shape_72.setTransform(161.2,55.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#3F8D3B").s().p("AgBgQIADAEIAAAYIgBAFgAgBgRIAAABIAAgBg");
	this.shape_73.setTransform(160.8,65.8);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#3F8D3B").s().p("AgFA9IADgHQACgRgEg6QgDguAHgNQgBAXACA0IAFBWg");
	this.shape_74.setTransform(157.9,85.9);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#3F8D3B").s().p("AABAPIgEgiIAHAng");
	this.shape_75.setTransform(157.8,101.3);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#3F8D3B").s().p("AgJAAQADgEAMAAIAGACQABAFgYACIACgFg");
	this.shape_76.setTransform(144.1,129.2);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#FFFFFF").s().p("A2fTTQgyAAgjgjQgjgjAAgyMAAAgi1QAAgxAjgkQAjgjAyAAMAs/AAAQAyAAAjAjQAjAkAAAxMAAAAi1QAAAygjAjQgjAjgyAAg");
	this.shape_77.setTransform(-1.1,0.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.drag1, new cjs.Rectangle(-167.5,-184.7,335.4,335.4), null);


(lib.aro = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.ar("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({x:8},0).wait(10).to({x:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-146.4,-40.8,293,81.8);


(lib.mov2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		var p = Object(this.parent);
		
		this.pre.addEventListener("click", pre_btn.bind(this));
		
		
		function pre_btn(evt) {
			
			p.gotoAndStop(0);
			p.mov1.relocate();
			
		    var csound = createjs.Sound.play("click");	
			
		}
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(41).call(this.frame_41).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("AgIBsIgHgFIgFgHQgCgEAAgFIACgJIAFgHIAHgFQAEgCAEABIAJABIAHAFIAFAHQACAFAAAEQAAAFgCAEIgFAHIgHAFIgJACIgIgCgAgLAfIgEgcQgBgMAAgOIAAhVIAiAAIAABVQAAAOgCAMIgDAcg");
	this.shape.setTransform(457.7,42.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AgTBtIAAhUIhPiEIAjAAQAFAAAEACIAFAHIAnBKIAHAMIADALIAFgMIAFgLIAohKIAFgGQADgDAFAAIAkAAIhPCEIAABUg");
	this.shape_1.setTransform(442.5,42.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("ABLBtQgGAAgDgDQgDgDgCgDIgQgtIhZAAIgQAtQgBADgEACQgDAEgGAAIgfAAIBVjYIApAAIBVDYgAgDg3IgEAMIgbBGIBFAAIgbhGIgEgMIgEgNIgDANg");
	this.shape_2.setTransform(424.8,42.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AhfBtIAAjYIBSAAQAYAAAUAHQAVAIAOAPQAOAOAIAUQAIAUAAAXQAAAYgIAUQgIAUgOAOQgOAOgVAJQgUAHgYABgAg3BMIAqAAQAPAAANgFQANgFAJgKQAIgKAFgOQAFgOAAgSQAAgRgFgOQgFgOgIgKQgJgKgNgFQgNgFgPgBIgqAAg");
	this.shape_3.setTransform(404.3,42.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AAzBtIAAhgIhlAAIAABgIgoAAIAAjYIAoAAIAABdIBlAAIAAhdIAoAAIAADYg");
	this.shape_4.setTransform(381.1,42.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AgTBtIAAi3IhCAAIAAghICrAAIAAAhIhCAAIAAC3g");
	this.shape_5.setTransform(360.8,42.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AAxBtQgGAAgDgDQgEgCgDgDIgthGQgCgEgDgCQgDgCgGAAIgRAAIAABWIgpAAIAAjYIBDAAQAUgBARAFQAPAEALAJQAJAIAFAMQAFALAAAOQAAALgDAKQgDAKgHAIQgGAGgIAGQgKAHgLADQAIAEAFAJIA3BQgAgrgFIAZAAQALAAAIgCQAIgDAFgFQAGgFADgHQACgHAAgIQAAgQgLgKQgKgIgVAAIgaAAg");
	this.shape_6.setTransform(343.9,42.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AgTBtIAAjYIAnAAIAADYg");
	this.shape_7.setTransform(328.4,42.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AhPBtIAAjYIBKAAQAVAAAPAEQAPADAKAIQAJAIAFAKQAEALAAANQAAAIgCAHQgCAHgFAGQgFAHgHAEQgHAFgKADQAsAKAAAmQAAAOgFALQgFAMgKAJQgKAIgPAEQgOAFgTABgAgnBNIAoAAQALAAAIgCQAHgDAFgFQAEgFADgFQACgHAAgGQAAgHgDgGQgCgGgFgEQgEgEgIgCQgIgCgKAAIgoAAgAgngNIAgAAQAUgBAKgHQALgIAAgPQAAgSgKgHQgJgHgUAAIgiAAg");
	this.shape_8.setTransform(314.4,42.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AgTBtIAAhUIhPiEIAjAAQAFAAAEACIAFAHIAnBKIAHAMIADALIAFgMIAFgLIAohKIAFgGQADgDAFAAIAkAAIhPCEIAABUg");
	this.shape_9.setTransform(288.6,42.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AhMBtIAAjYIBGAAQAVgBAQAGQAQAFAKAJQAKAJAFANQAFANAAAPQAAAPgFANQgFAMgLAKQgKAKgQAFQgQAFgUAAIgeAAIAABNgAgkABIAeAAQAKAAAIgCQAJgDAFgFQAGgFACgIQADgHAAgJQAAgJgDgHQgCgHgGgFQgFgFgJgDQgIgCgKAAIgeAAg");
	this.shape_10.setTransform(270.1,42.4);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AhMBtIAAjYIBGAAQAVgBAQAGQAQAFAKAJQAKAJAFANQAFANAAAPQAAAPgFANQgFAMgLAKQgKAKgQAFQgQAFgUAAIgeAAIAABNgAgkABIAeAAQAKAAAIgCQAJgDAFgFQAGgFACgIQADgHAAgJQAAgJgDgHQgCgHgGgFQgFgFgJgDQgIgCgKAAIgeAAg");
	this.shape_11.setTransform(251.3,42.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("ABLBtQgGAAgDgDQgDgDgCgDIgQgtIhZAAIgQAtQgBADgEACQgDAEgGAAIgfAAIBVjYIApAAIBVDYgAgDg3IgEAMIgbBGIBFAAIgbhGIgEgMIgEgNIgDANg");
	this.shape_12.setTransform(230.6,42.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AAzBtIAAhgIhlAAIAABgIgpAAIAAjYIApAAIAABdIBlAAIAAhdIAoAAIAADYg");
	this.shape_13.setTransform(208.6,42.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AgIBKQgEgCgDgCIgFgHQgCgEAAgFIACgJIAFgIQADgCAEgCQAEgCAEAAQAFAAAEACIAHAEIAFAIQACAEAAAFQAAAFgCAEIgFAHIgHAEQgEACgFAAQgEAAgEgCgAgIggQgEgCgDgCIgFgIQgCgEAAgFQAAgEACgEQACgFADgDQADgDAEgBQAEgCAEAAQAFAAAEACQAEABADADIAFAIQACAEAAAEQAAAFgCAEIgFAIIgHAEQgEACgFAAQgEAAgEgCg");
	this.shape_14.setTransform(187.5,45.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AgzBtIAAgcIArAAIAAh9IABgQIgfAaIgEACIgEABQgDAAgDgBIgDgDIgMgQIBCg4IAeAAIAAC8IAnAAIAAAcg");
	this.shape_15.setTransform(175.4,42.4);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF0000").s().p("AgfBoQgOgHgLgNQgLgOgFgVQgHgUAAgdQAAgbAHgWQAFgUALgOQALgOAOgGQAPgHAQAAQAQAAAPAHQAPAGAKAOQAMAOAFAUQAHAWAAAbQAAAdgHAUQgFAVgMAOQgKANgPAHQgPAHgQAAQgQAAgPgHgAgPhMQgHAEgGAKQgGAIgDAQQgEAPAAAXQAAAYAEAPQADAQAGAJQAGAJAHADQAIAEAHAAQAIAAAHgEQAIgDAFgJQAGgJAEgQQAEgPAAgYQAAgXgEgPQgEgQgGgIQgFgKgIgEQgHgDgIAAQgHAAgIADg");
	this.shape_16.setTransform(157.5,42.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF0000").s().p("AgSBaQgKgLAAgUIAAhVIgQAAIgFgCQgCgCAAgEIAAgOIAYgEIAIgqQABgEACgBQACgCADAAIATAAIAAAxIAoAAIAAAaIgoAAIAABSQAAAIADAEQAEAEAGAAIAGgBIAEgCIADgCIACAAIADAAIACADIALASQgIAHgKADQgLAEgLAAQgTAAgLgMg");
	this.shape_17.setTransform(137.2,43.3);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF0000").s().p("AgRBvIAAiZIAjAAIAACZgAgIhBQgFgCgDgDQgDgDgBgEQgDgFAAgFQAAgFADgEQABgEADgDQADgDAFgCQAEgCAEAAQAFAAAEACQAFACADADQADADACAEQACAEAAAFQAAAFgCAFQgCAEgDADQgDADgFACQgEACgFAAQgEAAgEgCg");
	this.shape_18.setTransform(127.3,42.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FF0000").s().p("AgaBuIAAh7IgNgCQgFgBgCgCQgDgDAAgEIAAgQIAXAAIAAgMQAAgNAEgKQAEgLAHgHQAIgIAKgDQAKgEAOAAQAKAAAJADIAAASQgBAEgEABIgJABQgGAAgFACQgFABgEAEQgEADgCAGQgBAGAAAJIAAAKIAoAAIAAAaIgnAAIAAB9g");
	this.shape_19.setTransform(118.1,42.2);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FF0000").s().p("AgXBKQgOgFgKgLQgLgKgGgPQgFgQAAgTQAAgQAFgOQAFgOAJgKQAKgKAOgGQANgGARAAQAOAAAMAFQANAEAJAJQAIAJAFANQAFANAAARIAAAHQAAAAAAABQAAAAgBAAQAAABAAAAQAAAAgBABQAAAAAAABQAAAAgBAAQAAABAAAAQgBAAAAAAIgEABIheAAQABAYALALQALALASAAQAJAAAGgCIAMgFIAIgFQAEgCADAAIAEABIADADIALANQgGAHgIAFQgIAFgIADQgIADgJABIgQABQgQAAgNgFgAgUgqQgJAJgDAQIBFAAQAAgHgCgGQgCgGgEgFQgEgFgGgDQgGgDgJAAQgPAAgJAKg");
	this.shape_20.setTransform(104.6,45.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF0000").s().p("AAeBOIAAhhQAAgOgHgHQgGgIgNAAQgJAAgIAFQgJAEgHAJIAABsIglAAIAAiZIAXAAQAGAAADAHIACAMIAKgIIALgHIAMgFQAGgBAIAAQANAAAKAEQAJAEAHAIQAGAIAEALQADAKAAANIAABhg");
	this.shape_21.setTransform(88.3,45.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FF0000").s().p("AgXBKQgOgFgKgLQgLgKgGgPQgFgQAAgTQAAgQAFgOQAFgOAJgKQAKgKAOgGQANgGARAAQAOAAAMAFQANAEAJAJQAIAJAFANQAFANAAARIAAAHQAAAAAAABQAAAAgBAAQAAABAAAAQAAAAgBABQAAAAAAABQAAAAgBAAQAAABAAAAQgBAAAAAAIgEABIheAAQABAYALALQALALASAAQAJAAAGgCIAMgFIAIgFQAEgCADAAIAEABIADADIALANQgGAHgIAFQgIAFgIADQgIADgJABIgQABQgQAAgNgFgAgUgqQgJAJgDAQIBFAAQAAgHgCgGQgCgGgEgFQgEgFgGgDQgGgDgJAAQgPAAgJAKg");
	this.shape_22.setTransform(71.5,45.5);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FF0000").s().p("AhPBtIAAjYIBKAAQAVAAAPAEQAPADAKAIQAJAIAFAKQAEALAAANQAAAIgCAHQgCAHgFAGQgFAHgHAEQgHAFgKADQAsAKAAAmQAAAOgFALQgFAMgKAJQgKAIgPAEQgOAFgTABgAgnBNIAoAAQALAAAIgCQAHgDAFgFQAEgFADgFQACgHAAgGQAAgHgDgGQgCgGgFgEQgEgEgIgCQgIgCgKAAIgoAAgAgngNIAgAAQAUgBAKgHQALgIAAgPQAAgSgKgHQgJgHgUAAIgiAAg");
	this.shape_23.setTransform(54.2,42.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFF59D").s().p("Eg6HAGfIAAs9MB0PAAAIpxKyQg7BDhSAkQhRAkhZAAg");
	this.shape_24.setTransform(372,41.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(42));

	// Layer 1
	this.pre = new lib.pre();
	this.pre.parent = this;
	this.pre.setTransform(1172.7,669.5);
	new cjs.ButtonHelper(this.pre, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.pre).wait(42));

	// Layer 1
	this.instance = new lib.msg("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(698.7,473.5,0.256,0.256);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({scaleX:0.32,scaleY:0.32,x:706.7,y:468},0).wait(1).to({scaleX:0.38,scaleY:0.38,x:714.6,y:462.5},0).wait(1).to({scaleX:0.44,scaleY:0.44,x:722.6,y:456.9},0).wait(1).to({scaleX:0.5,scaleY:0.5,x:730.5,y:451.4},0).wait(1).to({scaleX:0.57,scaleY:0.57,x:738.5,y:445.9},0).wait(1).to({scaleX:0.63,scaleY:0.63,x:746.5,y:440.4},0).wait(1).to({scaleX:0.69,scaleY:0.69,x:754.4,y:434.9},0).wait(1).to({scaleX:0.75,scaleY:0.75,x:762.4,y:429.3},0).wait(1).to({scaleX:0.81,scaleY:0.81,x:770.3,y:423.8},0).wait(1).to({scaleX:0.88,scaleY:0.88,x:778.3,y:418.3},0).wait(1).to({scaleX:0.94,scaleY:0.94,x:786.2,y:412.8},0).wait(1).to({scaleX:1,scaleY:1,x:794.2,y:407.3},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.ClipGroup_3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(644.4,573.2,1,1,0,0,0,292.2,330.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(42));

	// Layer 1
	this.instance_2 = new lib.light();
	this.instance_2.parent = this;
	this.instance_2.setTransform(207.8,96.6,1,1,0,0,0,0,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(42));

	// Layer 1
	this.instance_3 = new lib.ClipGroup_1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(1223.1,-12.6,1,1.131,0,0,0,92.6,63.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.lf(["#F7931E","#F24E18"],[0,1],-0.3,-1.9,3.9,4.4).s().p("AgnAFIBIgwIAHBXg");
	this.shape_25.setTransform(1254.1,-33.5,1,1.131);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#EE1E80").s().p("AgsgNIBZgjIgMBhg");
	this.shape_26.setTransform(1250.3,-20.9,1,1.131);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.lf(["#662D91","#7B278A","#B21779","#C01375"],[0,0.282,0.867,1],-4.8,0,4.9,0).s().p("AgwgQIBhgiIgVBlg");
	this.shape_27.setTransform(1261.8,-23.6,1,1.131);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FF0000").s().p("AgmB5QgQgGgLgNQgLgNgGgSIAtAAQAFAHAKAEQAJAEANAAQAPAAAKgGQAKgFADgJQAFgJAAgWQgLAMgMAFQgLAFgOAAQggAAgWgaQgWgaAAgnQAAgrAYgaQAUgYAeAAQANAAAMAGQAMAGANANIAAgUIAnAAIAACZQAAAugQAWQgWAdgsAAQgWAAgQgHgAgdhGQgNAOAAAWQAAAWANAOQAMAOATAAQASAAANgOQAMgNgBgXQABgWgMgOQgNgOgTAAQgSAAgMAOg");
	this.shape_28.setTransform(1240.8,-2.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FF0000").s().p("Ag6BDQgYgbgBgoQABgpAbgcQAYgZAfAAQAVAAAUANQAUANALAWQAMAWAAAYQAAAZgMAXQgLAWgTAMQgTANgXAAQgiAAgYgcgAgegkQgMAOAAAWQAAAYAMAOQAMAPASAAQASAAANgPQAMgPAAgXQAAgWgMgOQgMgPgTAAQgSAAgMAPg");
	this.shape_29.setTransform(1221.7,-5.8);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FF0000").s().p("AgTB9IAAj5IAnAAIAAD5g");
	this.shape_30.setTransform(1208.5,-9.3);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FF0000").s().p("Ag7BEQgWgbAAgpQAAgoAVgbQAWgbAfAAQAOAAAMAGQANAHAKAMIAAgUIAoAAIAACzIgoAAIAAgTQgLANgMAFQgMAGgNAAQgfAAgWgbgAgeglQgMAPAAAWQAAAXAMAPQANAPASAAQASAAAMgOQANgPAAgYQAAgXgNgOQgMgPgSAAQgTAAgMAPg");
	this.shape_31.setTransform(1194.6,-5.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FF0000").s().p("AgTB/IAAizIAnAAIAACzgAgRhMQgHgJAAgMQAAgMAHgJQAHgIAKAAQALAAAHAIQAIAJAAAMQAAAMgIAJQgHAIgLAAQgKAAgHgIg");
	this.shape_32.setTransform(1181.5,-9.5);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FF0000").s().p("AhUB6IAAjzIAwAAQAuAAAXANQAXANAPAeQAOAeAAAmQAAAcgIAYQgIAYgPAPQgOAQgRAGQgRAGgpAAgAgsBNIATAAQAbAAANgIQANgHAIgRQAIgSAAgZQAAgmgTgVQgSgTglAAIgOAAg");
	this.shape_33.setTransform(1167.9,-9);

	this.instance_4 = new lib.Image_12();
	this.instance_4.parent = this;
	this.instance_4.setTransform(43.7,262.3,0.48,0.543);

	this.instance_5 = new lib.Image_1_6();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-7.2,178.2,0.48,0.543);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFB74D").s().p("Egt5AE3IAApuMBb0AAAIpVJug");
	this.shape_34.setTransform(292.4,586.8,1,1.131);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FF6F00").s().p("EhkPAA9IAAh5MDIfAAAIAAB5g");
	this.shape_35.setTransform(638.5,548.8,1,1.131);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFEE9").s().p("EhkAA4IMAAAhwPMDIBAAAMAAABwPg");
	this.shape_36.setTransform(640,360.6,1,1.131);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.instance_5},{t:this.instance_4},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.instance_3}]}).wait(42));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.2,-84.9,1322.9,988.3);


(lib.head = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#913636").s().p("AgRAZQg0gFglgUIgLgVQA6AXAuADQArAEBYgiQgDATgHAGQgKAJgfAIQgeAJghAAIgVgBg");
	this.shape.setTransform(678.6,421.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

	// eyes
	this.instance = new lib.eyes();
	this.instance.parent = this;
	this.instance.setTransform(676.7,393.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CF8558").s().p("AArBwQgjgGgYgYQgcgagBghQAAgHAFgCQA8gZAHhjQAAAAAAgBQAAAAAAAAQAAAAABAAQAAgBAAAAQABAAAAABQAAAAAAAAQABAAAAAAQAAABAAAAQAGA7ggAzIgRASQgMALACAMQAEAUAXAUQANAKAcARQAAABAAAAQABAAAAABQAAAAAAABQAAAAAAAAIgCACIgBgBg");
	this.shape_1.setTransform(672.2,401.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#401304").s().p("Ag9AjQgEgKABgJQAAgMAFgOQAHgRAJgHQASgNAyAKQATAEAKALQAMALAAAQQAAAigHgCQgGgDABgdQABgOgLgJQgKgHgYgFQgmgIgSAXQgJALgCAXIAAAXIgBAAQgBAAgCgHg");
	this.shape_2.setTransform(686.2,392.5);

	this.instance_1 = new lib.ClipGroup_0();
	this.instance_1.parent = this;
	this.instance_1.setTransform(686.5,394.5,1,1,0,0,0,6.4,5.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgDA1QgVgDgLgFIgOgHQgHgEgBgIIgEgaQgBgeALgMQAOgPAlADQAnAEANAUQAOAVgFAZQgFAXgQALQgJAFgPAAQgJAAgKgCg");
	this.shape_3.setTransform(686.3,394.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#401304").s().p("AAwAuQADgpgLgQQgJgNgQgIQgPgHgPACQgeADABAbIADAWQACAPgDABQgHAEgEgkQgCgRAHgMQAIgNAPgCQAPgCASAEQAUAFAIAIQAIAHAGARQAEAOABAMQAAAJgDAKQgCAHgBAAIgBAAg");
	this.shape_4.setTransform(666,392.1);

	this.instance_2 = new lib.ClipGroup();
	this.instance_2.parent = this;
	this.instance_2.setTransform(666.4,393.4,1,1,0,0,0,5.4,5.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AAOAzIgfgEQgXgGgCgNIgHgyQADgjAuAGQAoAGAJAnIABAlQgGAPgHAEQgEADgHAAIgMgCg");
	this.shape_5.setTransform(666,393.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#932C09").s().p("AA8AdQgTgLgdgCQgdgCgeAMQgdAMgDgBQgNgDAVguQAFgKAXgIQAZgIAcACQAcABAVAJQASAIAEAJQAFAJgBARQgBASgHACIAAAAQgDAAgOgIg");
	this.shape_6.setTransform(686.4,382.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#932C09").s().p("AgKAUQgcgDgaAAQACgbADgUQAZgFAhACQAjADAJAJQANANAFAPQAHARgHAKQgdgIgqgGg");
	this.shape_7.setTransform(662.9,381.8);

	this.instance_3 = new lib.Image();
	this.instance_3.parent = this;
	this.instance_3.setTransform(656,367,0.48,0.48);

	this.instance_4 = new lib.Image_1_1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(651,339,0.48,0.48);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#932C09").s().p("Ag1BeQgSgTAAgWQAAgPgHguQgCgrAbgXQAbgYA3gGQAbgEAXACQADAggNArQgNAlgWAjQgVAjgLAMQgLAPgMAEIgGABQgMAAgOgOg");
	this.shape_8.setTransform(709.7,415.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8},{t:this.instance_4},{t:this.instance_3},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.instance_2},{t:this.shape_4},{t:this.shape_3},{t:this.instance_1},{t:this.shape_2},{t:this.shape_1}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.head, new cjs.Rectangle(651,339,73.6,97.1), null);


(lib.manwalk = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Armature_9
	this.instance = new lib.hh2("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(1043.6,336.9,0.997,0.997,6.9,0,0,13.2,-22.2);

	this.instance_1 = new lib.hh1("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(1025.2,249.5,0.999,0.999,-3.6,0,0,-12.3,-40.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:-3.6,x:1025.2,regX:-12.3,y:249.5}},{t:this.instance,p:{regX:13.2,rotation:6.9,x:1043.6,y:336.9,regY:-22.2}}]}).to({state:[{t:this.instance_1,p:{regY:-40.7,rotation:-2.4,x:1025,regX:-12.3,y:249.5}},{t:this.instance,p:{regX:13.1,rotation:6.6,x:1042.1,y:337.9,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:-1.3,x:1024.9,regX:-12.2,y:249.7}},{t:this.instance,p:{regX:13.2,rotation:6.4,x:1040.7,y:338.9,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:-0.2,x:1024.8,regX:-12.2,y:249.7}},{t:this.instance,p:{regX:13.2,rotation:6.1,x:1039.1,y:339.9,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:1,x:1024.6,regX:-12.2,y:249.8}},{t:this.instance,p:{regX:13.2,rotation:5.8,x:1037.6,y:340.9,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:2.1,x:1024.4,regX:-12.3,y:249.8}},{t:this.instance,p:{regX:13.2,rotation:5.5,x:1035.9,y:341.6,regY:-22.3}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:3.3,x:1024.4,regX:-12.2,y:249.9}},{t:this.instance,p:{regX:13.2,rotation:5.3,x:1034.3,y:342.7,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:4.4,x:1024.2,regX:-12.2,y:250}},{t:this.instance,p:{regX:13.2,rotation:5,x:1032.7,y:343.6,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.7,rotation:5.6,x:1024,regX:-12.2,y:249.9}},{t:this.instance,p:{regX:13.2,rotation:4.7,x:1031,y:344.5,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:6.7,x:1023.9,regX:-12.2,y:250.1}},{t:this.instance,p:{regX:13.1,rotation:4.5,x:1029.1,y:345.4,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:7.9,x:1023.8,regX:-12.2,y:250.2}},{t:this.instance,p:{regX:13.2,rotation:4.2,x:1027.4,y:346.2,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:9,x:1023.6,regX:-12.2,y:250.3}},{t:this.instance,p:{regX:13.2,rotation:3.9,x:1025.7,y:346.9,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:8.6,x:1023.6,regX:-12.2,y:250.3}},{t:this.instance,p:{regX:13.2,rotation:4.2,x:1026.3,y:347.1,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:8.2,x:1023.6,regX:-12.2,y:250.3}},{t:this.instance,p:{regX:13.2,rotation:4.4,x:1026.9,y:347.2,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:7.7,x:1023.6,regX:-12.2,y:250.3}},{t:this.instance,p:{regX:13.2,rotation:4.6,x:1027.5,y:347.3,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:7.3,x:1023.5,regX:-12.2,y:250.3}},{t:this.instance,p:{regX:13.2,rotation:4.9,x:1028.1,y:347.3,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:6.8,x:1023.5,regX:-12.2,y:250.3}},{t:this.instance,p:{regX:13.1,rotation:5.1,x:1028.5,y:347.5,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:6.4,x:1023.5,regX:-12.2,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:5.3,x:1029.2,y:347.5,regY:-22.3}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:5.9,x:1023.4,regX:-12.3,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:5.6,x:1029.8,y:347.5,regY:-22.3}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:5.3,x:1023.5,regX:-12.2,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:6,x:1030.9,y:347.6,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:4.7,x:1023.5,regX:-12.2,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:6.3,x:1032,y:347.5,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:4,x:1023.5,regX:-12.2,y:250.3}},{t:this.instance,p:{regX:13.2,rotation:6.7,x:1033,y:347.4,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:3.4,x:1023.5,regX:-12.2,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:7.1,x:1034.1,y:347.3,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.7,rotation:2.8,x:1023.4,regX:-12.3,y:250.3}},{t:this.instance,p:{regX:13.2,rotation:7.5,x:1035.2,y:347.2,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:2.1,x:1023.3,regX:-12.3,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:7.9,x:1036.2,y:347.1,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:1.5,x:1023.5,regX:-12.2,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:8.3,x:1037.2,y:346.9,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:0.9,x:1023.6,regX:-12.2,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:8.7,x:1038.3,y:346.7,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:0.3,x:1023.5,regX:-12.2,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:9.1,x:1039.4,y:346.6,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:-0.4,x:1023.6,regX:-12.2,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:9.4,x:1040.4,y:346.4,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:-1,x:1023.5,regX:-12.2,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:9.8,x:1041.5,y:346.2,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:-1.6,x:1023.5,regX:-12.2,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:10.2,x:1042.5,y:346,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:-2.3,x:1023.5,regX:-12.2,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:10.6,x:1043.6,y:345.8,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.7,rotation:-2.9,x:1023.5,regX:-12.2,y:250.3}},{t:this.instance,p:{regX:13.1,rotation:11,x:1044.5,y:345.5,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:-3.5,x:1023.6,regX:-12.2,y:250.3}},{t:this.instance,p:{regX:13.2,rotation:11.4,x:1045.6,y:345.3,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:-4.2,x:1023.6,regX:-12.2,y:250.4}},{t:this.instance,p:{regX:13.2,rotation:11.8,x:1046.7,y:345.1,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.7,rotation:-4.8,x:1023.6,regX:-12.2,y:250.2}},{t:this.instance,p:{regX:13.1,rotation:12.2,x:1047.6,y:344.8,regY:-22.2}}]},1).to({state:[{t:this.instance_1,p:{regY:-40.6,rotation:-5.4,x:1023.6,regX:-12.2,y:250.3}},{t:this.instance,p:{regX:13.2,rotation:12.6,x:1048.8,y:344.5,regY:-22.2}}]},1).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFECD2").s().p("AkABuQhLgPgbgzQgbgzArgIQArgIAVg2IAdhKQAmASAcArIAQAIQBTAgBigBQAHAPBWAAQBWAAAMADQAMADAhglIASAAQAsABAngOIAIAUIAAABQABAgAJAhIAAAVIAAAFQgEANgCARIgJASQgSAogpAdQg1ADg2AAQjfAAjdgqg");
	this.shape.setTransform(970.6,350.5,1,0.844);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(37));

	// righthand stop
	this.instance_2 = new lib.hand();
	this.instance_2.parent = this;
	this.instance_2.setTransform(905.4,213.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(37));

	// head
	this.instance_3 = new lib.head();
	this.instance_3.parent = this;
	this.instance_3.setTransform(982,168.2,1,1,0,0,0,687.8,387.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(37));

	// Armature_5
	this.instance_4 = new lib.l3("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(1017.7,586.7,0.997,0.997,0,0,0,12.3,-8.5);

	this.instance_5 = new lib.l2("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(1004,485.7,0.998,0.998,0,0,0,-3.1,-39.5);

	this.instance_6 = new lib.l1("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(981.5,366.9,0.999,0.999,0,0,0,-9.8,-54);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6,p:{rotation:0,x:981.5,y:366.9,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.5,rotation:0,x:1004,y:485.7}},{t:this.instance_4,p:{regY:-8.5,rotation:0,x:1017.7,y:586.7,regX:12.3}}]}).to({state:[{t:this.instance_6,p:{rotation:2.3,x:981.2,y:367,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-1.8,x:999.6,y:486.7}},{t:this.instance_4,p:{regY:-8.6,rotation:-0.8,x:1016.3,y:586.9,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:4.6,x:981,y:367,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-3.6,x:995.2,y:487.4}},{t:this.instance_4,p:{regY:-8.4,rotation:-1.7,x:1014.9,y:587.3,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:6.9,x:980.9,y:367.2,regX:-9.7,regY:-53.9}},{t:this.instance_5,p:{regY:-39.4,rotation:-5.5,x:990.7,y:487.9}},{t:this.instance_4,p:{regY:-8.5,rotation:-2.5,x:1013.5,y:587.2,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:9.1,x:980.6,y:367.2,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-7.3,x:986.2,y:488.3}},{t:this.instance_4,p:{regY:-8.4,rotation:-3.4,x:1011.9,y:586.9,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:11.4,x:980.3,y:367.2,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-9.1,x:981.7,y:488.6}},{t:this.instance_4,p:{regY:-8.5,rotation:-4.2,x:1010.6,y:586.2,regX:12.4}}]},1).to({state:[{t:this.instance_6,p:{rotation:13.7,x:980.2,y:367.3,regX:-9.7,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-10.9,x:977.1,y:488.7}},{t:this.instance_4,p:{regY:-8.6,rotation:-5.1,x:1008.8,y:585.2,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:16,x:979.9,y:367.4,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-12.8,x:972.7,y:488.6}},{t:this.instance_4,p:{regY:-8.5,rotation:-5.9,x:1007.3,y:584.3,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:18.3,x:979.6,y:367.4,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-14.6,x:968.2,y:488.4}},{t:this.instance_4,p:{regY:-8.5,rotation:-6.8,x:1005.7,y:583,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:20.6,x:979.4,y:367.5,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-16.4,x:963.7,y:488}},{t:this.instance_4,p:{regY:-8.4,rotation:-7.6,x:1004,y:581.6,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:22.9,x:979.2,y:367.6,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.5,rotation:-18.2,x:959.1,y:487.4}},{t:this.instance_4,p:{regY:-8.5,rotation:-8.4,x:1002.3,y:579.7,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:25.2,x:979,y:367.6,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-20.1,x:954.7,y:486.9}},{t:this.instance_4,p:{regY:-8.5,rotation:-9.3,x:1000.6,y:577.7,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:24.8,x:979,y:367.6,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-16.5,x:955,y:487}},{t:this.instance_4,p:{regY:-8.5,rotation:-6.1,x:995.1,y:580.6,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:24.5,x:979.2,y:367.6,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-12.9,x:955.2,y:487}},{t:this.instance_4,p:{regY:-8.4,rotation:-2.9,x:989.3,y:583.2,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:24.2,x:979.3,y:367.6,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-9.4,x:955.3,y:487.1}},{t:this.instance_4,p:{regY:-8.6,rotation:0.3,x:983.4,y:585.1,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:23.9,x:979.4,y:367.6,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:-5.8,x:955.6,y:487.1}},{t:this.instance_4,p:{regY:-8.5,rotation:3.5,x:977.5,y:587,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:23.6,x:979.6,y:367.6,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.5,rotation:-2.2,x:955.7,y:487.1}},{t:this.instance_4,p:{regY:-8.5,rotation:6.6,x:971.5,y:588.3,regX:12.4}}]},1).to({state:[{t:this.instance_6,p:{rotation:23.3,x:979.7,y:367.6,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:1.3,x:956,y:487.3}},{t:this.instance_4,p:{regY:-8.5,rotation:9.8,x:965.1,y:589.2,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:23,x:979.9,y:367.5,regX:-9.7,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:4.9,x:956.2,y:487.4}},{t:this.instance_4,p:{regY:-8.5,rotation:13,x:959,y:589.7,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:21.7,x:979.9,y:367.5,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:4.6,x:958.8,y:487.7}},{t:this.instance_4,p:{regY:-8.6,rotation:12.3,x:962.2,y:589.9,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:20.4,x:980,y:367.5,regX:-9.8,regY:-53.9}},{t:this.instance_5,p:{regY:-39.4,rotation:4.3,x:961.5,y:488}},{t:this.instance_4,p:{regY:-8.5,rotation:11.6,x:965.4,y:590.3,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:19.1,x:980.1,y:367.4,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:4.1,x:964.1,y:488.4}},{t:this.instance_4,p:{regY:-8.6,rotation:10.9,x:968.8,y:590.5,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:17.9,x:980.2,y:367.4,regX:-9.7,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:3.8,x:966.7,y:488.6}},{t:this.instance_4,p:{regY:-8.5,rotation:10.1,x:972,y:590.7,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:16.6,x:980.2,y:367.3,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.5,rotation:3.5,x:969.4,y:488.5}},{t:this.instance_4,p:{regY:-8.5,rotation:9.4,x:975.4,y:590.9,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:15.3,x:980.3,y:367.2,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.5,rotation:3.3,x:972.1,y:488.6}},{t:this.instance_4,p:{regY:-8.6,rotation:8.7,x:978.7,y:590.8,regX:12.4}}]},1).to({state:[{t:this.instance_6,p:{rotation:14,x:980.4,y:367.2,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:3,x:974.7,y:488.8}},{t:this.instance_4,p:{regY:-8.5,rotation:8,x:981.8,y:590.8,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:12.8,x:980.5,y:367.2,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:2.7,x:977.4,y:488.8}},{t:this.instance_4,p:{regY:-8.5,rotation:7.2,x:985.1,y:590.7,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:11.5,x:980.6,y:367.1,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:2.4,x:980.1,y:488.7}},{t:this.instance_4,p:{regY:-8.4,rotation:6.5,x:988.4,y:590.7,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:10.2,x:980.7,y:367.1,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:2.2,x:982.7,y:488.6}},{t:this.instance_4,p:{regY:-8.5,rotation:5.8,x:991.7,y:590.4,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:8.9,x:980.8,y:367.1,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:1.9,x:985.3,y:488.5}},{t:this.instance_4,p:{regY:-8.5,rotation:5.1,x:995,y:590.1,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:7.7,x:980.9,y:367,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:1.6,x:988,y:488.3}},{t:this.instance_4,p:{regY:-8.5,rotation:4.3,x:998.4,y:589.8,regX:12.4}}]},1).to({state:[{t:this.instance_6,p:{rotation:6.4,x:980.9,y:366.9,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:1.4,x:990.7,y:488}},{t:this.instance_4,p:{regY:-8.5,rotation:3.6,x:1001.6,y:589.5,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:5.1,x:981,y:366.9,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:1.1,x:993.3,y:487.6}},{t:this.instance_4,p:{regY:-8.5,rotation:2.9,x:1004.9,y:589,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:3.8,x:981.1,y:366.9,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:0.8,x:995.9,y:487.2}},{t:this.instance_4,p:{regY:-8.5,rotation:2.2,x:1008.1,y:588.5,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:2.5,x:981.2,y:366.8,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:0.5,x:998.5,y:486.8}},{t:this.instance_4,p:{regY:-8.5,rotation:1.4,x:1011.3,y:588,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:1.3,x:981.3,y:366.8,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:0.3,x:1001.2,y:486.3}},{t:this.instance_4,p:{regY:-8.6,rotation:0.7,x:1014.5,y:587.3,regX:12.3}}]},1).to({state:[{t:this.instance_6,p:{rotation:0,x:981.4,y:366.7,regX:-9.8,regY:-54}},{t:this.instance_5,p:{regY:-39.4,rotation:0,x:1003.7,y:485.7}},{t:this.instance_4,p:{regY:-8.5,rotation:0,x:1017.7,y:586.6,regX:12.3}}]},1).wait(1));

	// body
	this.instance_7 = new lib.Image_1();
	this.instance_7.parent = this;
	this.instance_7.setTransform(948,219,0.544,0.52);

	this.instance_8 = new lib.Image_1_2();
	this.instance_8.parent = this;
	this.instance_8.setTransform(964.3,195.1,0.544,0.52);

	this.instance_9 = new lib.Image_2_1();
	this.instance_9.parent = this;
	this.instance_9.setTransform(930,202,0.556,0.521);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_9},{t:this.instance_8},{t:this.instance_7}]}).wait(37));

	// Armature_7
	this.instance_10 = new lib.r3("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(918.2,591.4,0.996,0.996,4.7,0,0,6.8,-2.8);

	this.instance_11 = new lib.r2("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(940.4,465.3,0.994,0.994,4.7,0,0,3.2,-46.4);

	this.instance_12 = new lib.r1("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(961.6,380.6,0.997,0.997,4.7,0,0,5,-28.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_12,p:{rotation:4.7,x:961.6,y:380.6,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:4.7,x:940.4,y:465.3,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.8,rotation:4.7,x:918.2,y:591.4,regX:6.8}}]}).to({state:[{t:this.instance_12,p:{rotation:5.5,x:961.3,y:379.5,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:2.6,x:939.9,y:464.1,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:1.6,x:922,y:590.8,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:6.4,x:961.1,y:378.4,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:0.5,x:939.3,y:462.9,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-1.4,x:925.9,y:589.9,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:7.2,x:961,y:377.4,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-1.5,x:938.7,y:461.7,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-4.5,x:929.9,y:588.9,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:8,x:960.8,y:376.3,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-3.6,x:938.2,y:460.4,regX:3.3,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-7.5,x:933.8,y:587.7,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:8.9,x:960.6,y:375.2,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-5.7,x:937.4,y:459.2,regX:3.1,regY:-46.4}},{t:this.instance_10,p:{regY:-2.8,rotation:-10.6,x:937.7,y:586.2,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:6.6,x:960.7,y:375.2,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-5.3,x:940,y:460,regX:3.1,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-11.1,x:940.4,y:587.2,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:4.4,x:960.8,y:375.2,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-5,x:942.7,y:460.7,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-11.6,x:943.2,y:588,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:2.2,x:960.9,y:375.1,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-4.6,x:945.4,y:461.3,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-12.1,x:945.9,y:588.7,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:-0.1,x:961,y:375.1,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-4.2,x:948.1,y:462,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-12.7,x:948.7,y:589.4,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:-2.3,x:961.1,y:375.1,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-3.9,x:950.8,y:462.5,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-13.2,x:951.6,y:589.9,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:-4.6,x:961.1,y:375.1,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-3.5,x:953.6,y:462.9,regX:3.3,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-13.7,x:954.5,y:590.5,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:-3.6,x:960.7,y:374.8,regX:4.9,regY:-28.6}},{t:this.instance_11,p:{rotation:-8.3,x:953,y:461.9,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-17.8,x:964.3,y:588.9,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:-2.6,x:960.6,y:374.6,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-13.1,x:952.5,y:460.8,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-21.8,x:974,y:586.4,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:-1.6,x:960.5,y:374.4,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-17.8,x:952,y:460,regX:3.2,regY:-46.3}},{t:this.instance_10,p:{regY:-2.7,rotation:-25.9,x:983.7,y:583.2,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:-0.6,x:960.3,y:374.2,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-22.6,x:951.6,y:458.8,regX:3.3,regY:-46.4}},{t:this.instance_10,p:{regY:-2.8,rotation:-29.9,x:993,y:579,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:0.4,x:960,y:374,regX:5,regY:-28.5}},{t:this.instance_11,p:{rotation:-27.4,x:951,y:458,regX:3.1,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-34,x:1002.2,y:574.2,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:1.4,x:959.8,y:373.7,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-32.2,x:950.8,y:456.9,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-38,x:1011.1,y:568.6,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:2.4,x:959.6,y:373.4,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-37,x:950.4,y:456,regX:3.2,regY:-46.3}},{t:this.instance_10,p:{regY:-2.7,rotation:-42.1,x:1019.5,y:562.2,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:2.5,x:959.7,y:373.8,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-34.7,x:949.8,y:456.5,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-39.5,x:1014.5,y:565.7,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:2.6,x:959.8,y:374.3,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-32.3,x:949.3,y:457,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-36.9,x:1009.4,y:568.9,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:2.8,x:959.9,y:374.6,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-30,x:948.7,y:457.5,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-34.3,x:1004.2,y:571.9,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:2.9,x:960,y:375.1,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-27.7,x:948.2,y:458.1,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.8,rotation:-31.7,x:998.9,y:574.7,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:3,x:960.2,y:375.4,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-25.4,x:947.6,y:458.8,regX:3.1,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-29.1,x:993.5,y:577.3,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:3.1,x:960.3,y:375.8,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-23.1,x:947.1,y:459.2,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-26.5,x:987.9,y:579.8,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:3.2,x:960.4,y:376.4,regX:5,regY:-28.5}},{t:this.instance_11,p:{rotation:-20.8,x:946.5,y:459.8,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-23.9,x:982.4,y:582,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:3.4,x:960.5,y:376.6,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-18.5,x:946,y:460.3,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-21.3,x:976.6,y:584,regX:6.7}}]},1).to({state:[{t:this.instance_12,p:{rotation:3.5,x:960.6,y:377,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-16.2,x:945.5,y:460.8,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-18.8,x:971,y:585.7,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:3.6,x:960.7,y:377.4,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-13.9,x:945,y:461.3,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-16.2,x:965.3,y:587.3,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:3.7,x:960.6,y:377.8,regX:4.9,regY:-28.6}},{t:this.instance_11,p:{rotation:-11.6,x:944.4,y:461.9,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-13.6,x:959.5,y:588.6,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:3.8,x:960.8,y:378.2,regX:4.9,regY:-28.6}},{t:this.instance_11,p:{rotation:-9.3,x:944.1,y:462.4,regX:3.3,regY:-46.4}},{t:this.instance_10,p:{regY:-2.8,rotation:-11,x:953.6,y:589.6,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:4,x:960.9,y:378.6,regX:4.9,regY:-28.6}},{t:this.instance_11,p:{rotation:-7,x:943.3,y:463,regX:3.1,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-8.4,x:947.8,y:590.6,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:4.1,x:961.1,y:379,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-4.6,x:942.7,y:463.4,regX:3.1,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-5.8,x:942,y:591.2,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:4.2,x:961.2,y:379.4,regX:5,regY:-28.6}},{t:this.instance_11,p:{rotation:-2.3,x:942.3,y:463.9,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-3.2,x:936.1,y:591.7,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:4.3,x:961.2,y:379.8,regX:4.9,regY:-28.6}},{t:this.instance_11,p:{rotation:0,x:941.9,y:464.4,regX:3.3,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:-0.6,x:930.1,y:591.9,regX:6.7}}]},1).to({state:[{t:this.instance_12,p:{rotation:4.5,x:961.3,y:380.3,regX:4.9,regY:-28.5}},{t:this.instance_11,p:{rotation:2.3,x:941.3,y:464.9,regX:3.3,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:2,x:924.3,y:591.9,regX:6.8}}]},1).to({state:[{t:this.instance_12,p:{rotation:4.6,x:961.4,y:380.7,regX:4.9,regY:-28.5}},{t:this.instance_11,p:{rotation:4.6,x:940.6,y:465.4,regX:3.2,regY:-46.4}},{t:this.instance_10,p:{regY:-2.7,rotation:4.6,x:918.5,y:591.6,regX:6.8}}]},1).wait(1));

	// righthand
	this.instance_13 = new lib.Image_2();
	this.instance_13.parent = this;
	this.instance_13.setTransform(894,224,0.48,0.48);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(37));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(888.9,119.6,179.2,498.8);


(lib.mov1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/*
		
		Make Dragging item name as drag1, drag2 ..... drag10.
		Make Dropping drop places as  drop1, drop2 ..... drop10.
		Drop items must be name as correct order
		
		*/
		
		//this.confirm2.visible=false;
		//this.answer2.visible=false;
		
		
		createjs.Touch.enable(stage);
		
		////adding the button current target name ex .name |Sanka|
		
		var count = 1; // Add the number of drops here |Sanka|
		
		
		var clip = this;
		
		var clicked = 0;
		
		//var aud1 = createjs.Sound.play("audio");
		
		
		
		
		var drag = [];
		var drop = [];
		
		/*
		var dragx = [];
		var dragy = [];
		for (var i=1; i<=count; i++){
			var dragg = clip["drag"+i];
			dragx[i-1] = dragg.x;
			dragy[i-1] = dragg.y;
		}
		
		*/
		
		this.enterans.visible = false;
		//this.answermov.visible =false;
		//this.answer.mouseEnabled = false;
		
		//for (var i = 1; i <= 12; i++) {
			//var green = clip["green"+i];
			//green.visible = false;
		//}
		
		
		for (var i = 1; i <= count; i++) {
		
			var dragname = clip["drag" + i];
			dragname.name = i;
		
			var dropname = clip["drop" + i];
			dropname.name = i;
		
			//var correct = clip["correct"+i];
			//correct.visible = false;
		}
		
		
		
		
		
		
		var dragx = [];
		var dragy = [];
		for (var goi = 1; goi <= count; goi++) {
			drag[goi] = clip["drag" + goi];
			drop[goi] = clip["drop" + goi];
		
		
			drag[goi].on("mousedown", onMouseDown.bind(this));
			drag[goi].on("pressmove", onMouseMove.bind(this));
			drag[goi].on("pressup", onMouseUp.bind(this));
			drag[goi].on("mouseover", onMouseOver.bind(this));
		
		
		
			dragx[goi - 1] = drag[goi].x;
			dragy[goi - 1] = drag[goi].y;
		
		}
		
		
		
		
		
		function onMouseDown(evt) {
			console.log("algito");
		
			var item = evt.currentTarget;
		    this.setChildIndex(item, this.getNumChildren() - 1); // Set object to top
			
			
			
			//var tik = createjs.Sound.play("dragaud");
			
			item.offset = {
				x: 0,
				y: 0
			};
		
			var pt = item.parent.globalToLocal(evt.stageX, evt.stageY);
			item.offset.x = pt.x - item.x;
			item.offset.y = pt.y - item.y;
			item.drag = true;
		
		
		}
		
		//////////////////////////////All Going On here\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		
		
		
		function onMouseUp(evt) {
			var item = evt.currentTarget.name;
		
			var dragitem = evt.currentTarget;
			//alert(item);
		
			var pt = [];
			for (var i = 1; i <= count; i++) {
				pt[i - 1] = drag[item].localToLocal(10, 0, drop[i]);
		
			}
		
			//this.setChildIndex( dragitem, this.getNumChildren()-1); // Set object to top
		     
			
			var p = Object(this.parent);
			
			
			for (var i = 1; i <= count; i++) {
		
				if (drop[item].hitTest(pt[i - 1].x, pt[i - 1].y)) {
					drag[item].x = drop[i].x;
					drag[item].y = drop[i].y;
					
					//alert("aaaaaaa");
				    
					p.gotoAndStop(1);
					//var tdro = createjs.Sound.play("dropaud");
					var csound = createjs.Sound.play("click");
					
					break;
				} else {
					drag[item].x = dragx[item - 1];
					drag[item].y = dragy[item - 1];
		
				}
		
			}
			/////////////Cheking already dropped items and relocate |Sanka|				
			for (var i = 1; i <= count; i++) {
				if (drag[i] != dragitem) {
					if (drag[i].x == drag[item].x && drag[i].y == drag[item].y) {
						drag[i].x = dragx[i - 1];
						drag[i].y = dragy[i - 1];
		
					}
				}
			}
		
		
		}
		//////////////////////////////////////////////////////////////////////////////////////////
		
		
		
		
		
		function onMouseMove(evt) {
			console.log("algito3");
		
		
			var item = evt.currentTarget;
			if (item.drag) {
				var pt = item.parent.globalToLocal(evt.stageX, evt.stageY);
				item.x = pt.x - item.offset.x;
				item.y = pt.y - item.offset.y;
			}
		
		}
		
		
		
		/////// Set Dragging item to top \\\\\\\\\\\
		function onMouseOver(evt) {
			//var item = evt.currentTarget.name;
			//var dragitem = evt.currentTarget;
			//this.setChildIndex(dragitem, this.getNumChildren() - 1); // Set object to top
		
		}
		
		
		this.relocate = function() {
		
		
			drag[1].x = dragx[0];
			drag[1].y = dragy[0];
			
			
		}
		
		
		
		
		/*
		
		
		this.nxt.addEventListener("click", nxt_btn.bind(this));
		
		
		function nxt_btn(evt) {
			var chk = 0;
		
		   // var clk = createjs.Sound.play("clicks");
		
			for (var i = 1; i <= count; i++) {
				if (drag[i].x == dragx[i - 1] && drag[i].y == dragy[i - 1]) {
					chk = chk + 1;
				} else {
		
					chk = chk - 1;
		
				}
			}
		
		
			alert(chk);
		
			if (chk == -1) {
				
				
				
				for (var i = 1; i <= count; i++) {
					
					var draggg = clip["drag" + i];
					draggg.mouseEnabled = false;
		
				}
		
				var dropgreen = [];
		
		
				
				
						if (drag[1].x == drop[1].x && drag[1].y == drop[1].y) {
		
							alert("aaa")
							//var dragg = clip["drag" + i];
							//dragg.gotoAndStop(1);
		
		
						}
		
					
			
		
		
		
		
		
		
		
		
		
		
		
		
		
		
			} else {
				this.enterans.visible = true;
				this.enterans.gotoAndPlay(0);
				this.confirm2.visible=false;
			}
		}
		
		
		*/
	}
	this.frame_41 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(41).call(this.frame_41).wait(1));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("Ehj/AAjIAAhFMDH/AAAIAABFg");
	this.shape.setTransform(639.9,761.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(42));

	// enterans
	this.enterans = new lib.enterans();
	this.enterans.parent = this;
	this.enterans.setTransform(1205.9,979.9);

	this.timeline.addTween(cjs.Tween.get(this.enterans).wait(42));

	// Layer 1
	this.instance = new lib.Birthday("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(668.5,137.2,0.668,0.668);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,alpha:1},9).wait(33));

	// Layer 1
	this.instance_1 = new lib.Image_9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(1130.4,-33.2,0.48,0.48);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("AgmBrQgQgGgLgLQgLgMgGgQIAtAAQAFAHAKAEQAJADAMAAQAQAAAJgFQALgFADgIQAEgIAAgTQgKALgLAEQgMAEgOAAQggAAgWgWQgWgYAAgiQAAgmAXgXQAWgVAdAAQAMAAANAFQAMAFAMALIAAgRIApAAIAACIQAAAogRATQgWAagrAAQgXAAgQgGgAgdg+QgMAMgBAUQABAUAMAMQANANASgBQASAAAMgMQANgMAAgUQAAgUgNgMQgMgNgTAAQgSAAgMANg");
	this.shape_1.setTransform(1240.9,39.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("Ag6A8QgYgZAAgjQAAgkAbgZQAYgWAfAAQAWAAATAMQAUALALATQAMAUgBAVQABAWgMAUQgLAUgTALQgTALgXAAQgiAAgYgYgAgfggQgLANgBATQABAVALANQANANASAAQASAAANgNQANgNAAgVQAAgTgNgNQgMgNgTAAQgSAAgNANg");
	this.shape_2.setTransform(1221.7,36.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("AgTBuIAAjbIAnAAIAADbg");
	this.shape_3.setTransform(1208.5,33.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("Ag7A8QgWgYgBgkQABgkAVgXQAWgYAfAAQANAAANAGQAMAFALALIAAgSIApAAIAACfIgpAAIAAgRQgMALgLAFQgMAFgMAAQggAAgWgYgAgeghQgMANAAAUQAAAVAMANQANANARAAQAUAAAMgNQAMgNAAgVQAAgUgMgNQgMgNgUAAQgRAAgNANg");
	this.shape_4.setTransform(1194.7,36.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AgTBwIAAieIAnAAIAACegAgRhEQgIgHAAgLQAAgKAIgIQAHgHAKgBQAKABAIAHQAHAHAAAMQAAALgHAHQgIAHgKABQgKAAgHgJg");
	this.shape_5.setTransform(1181.5,33.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AhUBsIAAjXIAwAAQAuAAAXAMQAXAMAPAaQAOAaAAAiQAAAZgIAVQgIAVgPANQgOAOgRAFQgRAGgpAAgAgsBEIATAAQAbAAANgHQANgGAIgPQAIgQAAgWQAAghgTgTQgSgRglAAIgOAAg");
	this.shape_6.setTransform(1167.9,33.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.instance_1}]}).wait(42));

	// Layer 1
	this.instance_2 = new lib.aro();
	this.instance_2.parent = this;
	this.instance_2.setTransform(330.6,489.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(42));

	// Layer 1
	this.drag1 = new lib.drag1();
	this.drag1.parent = this;
	this.drag1.setTransform(666.7,396.8,1,1,0,0,0,0.1,-17.1);

	this.timeline.addTween(cjs.Tween.get(this.drag1).wait(42));

	// Layer 1
	this.drop1 = new lib.drop1();
	this.drop1.parent = this;
	this.drop1.setTransform(1051.7,373.5);
	this.drop1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.drop1).wait(30).to({_off:false},0).wait(12));

	// Layer 19
	this.drop1_1 = new lib.manwalk();
	this.drop1_1.parent = this;
	this.drop1_1.setTransform(174.7,-7);

	this.timeline.addTween(cjs.Tween.get(this.drop1_1).to({x:73.2,y:7.5},29).to({_off:true},1).wait(12));

	// Layer 1
	this.instance_3 = new lib.light();
	this.instance_3.parent = this;
	this.instance_3.setTransform(207.8,96.6,1,1,0,0,0,0,-0.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(42));

	// Layer 1
	this.instance_4 = new lib.ClipGroup_2();
	this.instance_4.parent = this;
	this.instance_4.setTransform(1222.9,-15.3,1,1.137,0,0,0,92.6,63.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#F7931E","#F24E18"],[0,1],-0.3,-1.9,3.9,4.4).s().p("AgnAFIBIgwIAHBXg");
	this.shape_7.setTransform(1253.9,-36.3,1,1.137);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#EE1E80").s().p("AgsgNIBZgjIgMBhg");
	this.shape_8.setTransform(1250.1,-23.6,1,1.137);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#662D91","#7B278A","#B21779","#C01375"],[0,0.282,0.867,1],-4.8,0,4.9,0).s().p("AgvgPIBfgjIgUBlg");
	this.shape_9.setTransform(1261.6,-26.4,1,1.137);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AgmB6QgQgHgLgMQgLgNgFgTIAsAAQAGAHAJAEQAJAFANAAQAPAAAKgHQAJgFAEgJQAEgJABgWQgLAMgMAGQgLAEgOABQggAAgWgbQgWgaAAgoQAAgrAXgZQAVgYAeAAQAMgBANAHQAMAFANAOIAAgVIAnAAIAACbQAAAtgQAWQgWAdgrAAQgXABgQgHgAgdhHQgNAPAAAWQAAAWANAOQAMAOATAAQATAAALgOQAMgNABgXQgBgXgMgOQgMgOgTAAQgSAAgMAOg");
	this.shape_10.setTransform(1240.6,-5.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("Ag6BEQgZgcABgoQgBgpAcgcQAYgZAfAAQAVAAAVANQATANALAWQALAWAAAYQAAAagLAWQgLAWgTANQgTAMgXAAQgiAAgYgbgAgegkQgNAOAAAWQAAAYANAOQAMAPASAAQASAAANgPQAMgPAAgXQAAgWgMgPQgMgOgTAAQgSAAgMAPg");
	this.shape_11.setTransform(1221.5,-8.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AgTB+IAAj6IAnAAIAAD6g");
	this.shape_12.setTransform(1208.3,-12);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("Ag7BEQgXgbAAgoQAAgpAWgbQAWgbAfAAQANAAANAGQAMAGALANIAAgUIApAAIAAC0IgpAAIAAgTQgMAMgLAGQgMAFgMAAQgfAAgXgbgAgeglQgMAPAAAWQAAAXAMAQQANAPASAAQASAAAMgPQANgPAAgYQAAgXgNgOQgMgPgSAAQgTAAgMAPg");
	this.shape_13.setTransform(1194.4,-8.6);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AgTCAIAAi0IAnAAIAAC0gAgRhNQgIgIABgNQgBgMAIgIQAHgJAKAAQAKAAAIAJQAIAJgBAMQABAMgIAIQgIAJgKAAQgKAAgHgJg");
	this.shape_14.setTransform(1181.3,-12.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AhUB7IAAj1IAwAAQAuAAAXAOQAXANAPAeQAOAdAAAnQAAAdgIAXQgIAYgPAQQgOAPgRAHQgRAFgpABgAgsBNIATAAQAbAAANgHQANgHAIgSQAIgRAAgaQAAglgTgWQgSgTglgBIgOAAg");
	this.shape_15.setTransform(1167.7,-11.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#F3E5F5").s().p("A9hOdIAA85MA7DAAAIAAc5g");
	this.shape_16.setTransform(326.6,506.3,1,1.137);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#D51F52").s().p("AifEVIBBopICuAAIBQIpg");
	this.shape_17.setTransform(96.8,406.7,1,1.137);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#146600").s().p("ACOEpIAAgBIAAgBIgMgZQgNgTgRgKQgKgGgRgIIgcgMQgngXggg0QgegvgRg5QgNgqgIg/QgKhFgFgjQgWiCg7hLQCxCfCSDPQAXAgALAVQARAfADAaQAEAbgKAbQgEALgRAgQgVAoACAiIAxBqIgJAEgACGEDQgQhSgwhwQgthkghguQAZAwAQAjIASAqIATAqQAQAoAwCFIAAAAg");
	this.shape_18.setTransform(76.9,336.5,1,1.137);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#377E16").s().p("AAOGOQgOgrgogiQgggbgLgNQgWgbgJgiQgPg2AQhrQAwlCBikfQgWB4ApCnQALAtAYBXQAVBPAIA5QALBMgIBJQgIBQgcAwQgGAKgPAUQgPATgGAMQgNAXgDAbIAAAkIAAACIAAABIABB2IgLABgAAPF/QgOilgDhMIgDg8IgBg8QgBgmAChTQgMBJAFCPQAFCfAWBrIAAAAg");
	this.shape_19.setTransform(96.7,322.6,1,1.137);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#3E3E3E").s().p("Aj8AJIAAgSIH5AAIAAASg");
	this.shape_20.setTransform(224.9,328.1,1,1.137);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#3E3E3E").s().p("Aj8AJIAAgRIH5AAIAAARg");
	this.shape_21.setTransform(224.9,332.9,1,1.137);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#3E3E3E").s().p("Aj8AKIAAgSIH5AAIAAASg");
	this.shape_22.setTransform(224.9,337.8,1,1.137);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#3E3E3E").s().p("Aj8AKIAAgTIH5AAIAAATg");
	this.shape_23.setTransform(224.9,342.7,1,1.137);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#3E3E3E").s().p("Aj8AKIAAgSIH5AAIAAASg");
	this.shape_24.setTransform(224.9,347.6,1,1.137);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#3E3E3E").s().p("Aj8AKIAAgTIH5AAIAAATg");
	this.shape_25.setTransform(224.9,352.4,1,1.137);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#3E3E3E").s().p("AijB3IAtimQAIgfAagUQAagUAhAAIAzAAQAhAAAaAUQAaAUAIAfIAtCmg");
	this.shape_26.setTransform(227.5,377.1,1,1.137);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#565656").s().p("AmqEiIAAo3QAAgFAEgEQADgDAFAAIM9AAQAFAAADADQAEAEAAAFIAAI3g");
	this.shape_27.setTransform(226.5,342.9,1,1.137);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#565656").s().p("AhYAlIAAggQAAgRALgMQANgMARAAIBfAAQASAAALAMQANANAAAQIAAAgg");
	this.shape_28.setTransform(174.9,387,1,1.137);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AiYAsQgSAAgNgNQgNgNAAgSQAAgRANgNQANgNASAAIFdAAIAABXg");
	this.shape_29.setTransform(332.1,382.7,1,1.137);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#E8AF1E").s().p("AiuA+QgZAAgSgSQgTgSAAgaQAAgZATgRQASgTAZAAIGbAAIAAASIgmAAIAABXIAmAAIAAASg");
	this.shape_30.setTransform(331.9,382.7,1,1.137);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AjEAsIAAhXIFdAAQASAAANANQANANAAARQAAASgNANQgNANgSAAg");
	this.shape_31.setTransform(342.5,369.7,1,1.137);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#4D31DE").s().p("AjrA+IAAgSIAlAAIAAhXIglAAIAAgSIGaAAQAZAAASASQATASgBAZQABAagTASQgSASgZAAg");
	this.shape_32.setTransform(342.8,369.7,1,1.137);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#E8AF1E").s().p("AgPAUIAAgdQAAgEADgDQADgDAEAAIALAAQAEAAADADQADADAAAEIAAAdg");
	this.shape_33.setTransform(419.9,371.8,1,1.137);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#E8AF1E").s().p("AhLA4IAAg+QAAgUAOgPQAPgOAUAAIA1AAQAUAAAPAOQAOAPAAAUIAAA+g");
	this.shape_34.setTransform(420,379.6,1,1.137);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#E8AF1E").s().p("AhdAQIAAgfIC6AAIAAAfg");
	this.shape_35.setTransform(420.2,388.5,1,1.137);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#131313").s().p("Ag1A2IAAhrIBrAAIAABrg");
	this.shape_36.setTransform(451.3,428.1,1,1.137);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#E86136").s().p("Ag1A2IAAhrIBrAAIAABrg");
	this.shape_37.setTransform(470.9,428.1,1,1.137);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#131313").s().p("Ag1A2IAAhrIBrAAIAABrg");
	this.shape_38.setTransform(490.5,428.1,1,1.137);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#E1BEE7").s().p("A/mAvIAAhdMA/NAAAIAABdg");
	this.shape_39.setTransform(325.8,395.8,1,1.137);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#131313").s().p("EgiCAL5IAA3xMBEFAAAIAAXxg");
	this.shape_40.setTransform(261.6,524.9,1,1.137);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AgIAaQgKgHgDgNQgDgMADgLQAFgLAJgCQAIgDAJAIQAIAHAEANQADAMgEALQgEALgIACIgFABQgGAAgGgGg");
	this.shape_41.setTransform(69.7,190,1,1.137);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgugXIBLgUIASBDIhLAUg");
	this.shape_42.setTransform(77.1,220.9,1,1.137);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#92327A").s().p("AhujBIBugdIBvGfIhvAeg");
	this.shape_43.setTransform(73.4,204.9,1,1.137);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgWAXQgKgKAAgNQAAgMAKgKQAKgKAMAAQANAAAKAKQAKAKAAAMQAAANgKAKQgKAKgNAAQgMAAgKgKg");
	this.shape_44.setTransform(51.5,190.2,1,1.137);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("Ag3AjIAAhFIBuAAIAABFg");
	this.shape_45.setTransform(51.7,222.3,1,1.137);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#4D31DE").s().p("AhQDYIAAmvIChAAIAAGvg");
	this.shape_46.setTransform(51.8,205.8,1,1.137);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AgWAXQgKgKAAgNQAAgMAKgKQAJgKANAAQANAAAKAKQAKAKgBAMQABANgKAKQgKAKgNAAQgNAAgJgKg");
	this.shape_47.setTransform(32.4,190.2,1,1.137);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FFFFFF").s().p("Ag2AjIAAhFIBtAAIAABFg");
	this.shape_48.setTransform(32.6,222.3,1,1.137);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#E8AF1E").s().p("AhQDYIAAmvIChAAIAAGvg");
	this.shape_49.setTransform(32.7,205.8,1,1.137);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgZAaQgLgLABgPQgBgOALgLQALgLAOAAQAPAAALALQAKALAAAOQAAAPgKALQgLALgPAAQgOAAgLgLg");
	this.shape_50.setTransform(10.1,185.2,1,1.137);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("Ag+AnIAAhOIB8AAIAABOg");
	this.shape_51.setTransform(10.3,221.3,1,1.137);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#332400").s().p("AhaDzIAAnlIC1AAIAAHlg");
	this.shape_52.setTransform(10.5,202.7,1,1.137);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#131313").s().p("AoGAVIAAgpIQNAAIAAApg");
	this.shape_53.setTransform(44.8,232.7,1,1.137);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFB74D").s().p("EgtyAE4IAApvMBblAAAIpTJvg");
	this.shape_54.setTransform(292.9,586.6,1,1.137);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FF6F00").s().p("Ehj/AA9IAAh5MDH/AAAIAAB5g");
	this.shape_55.setTransform(639.8,548.3,1,1.137);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFEE9").s().p("EhkAA4RMAAAhwhMDIAAAAMAAABwhg");
	this.shape_56.setTransform(639.8,358.7,1,1.137);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.instance_4}]}).wait(42));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7,-88,1520,917.9);


// stage content:
(lib.slide_02 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.stop();
		
		
		
		var csound = createjs.Sound.stop("menu");
		var csound = createjs.Sound.play("menu"); 	
		csound.on("complete", audComplete.bind(this));
		
		
		
		function audComplete(event) {
			//main.play_btn.addEventListener("click", getaudio);
			//alert("aaaaa");
			var csound = createjs.Sound.play("menu");
			csound.on("complete", audComplete.bind(this));
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// mov1
	this.mov1 = new lib.mov1();
	this.mov1.parent = this;
	this.mov1.setTransform(0.4,1.4);

	this.mov2 = new lib.mov2();
	this.mov2.parent = this;
	this.mov2.setTransform(0,0.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.mov1}]}).to({state:[{t:this.mov2}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(633.3,297.4,1520,917.9);
// library properties:
lib.properties = {
	width: 1280,
	height: 768,
	fps: 25,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/Image.png?1545875232737", id:"Image"},
		{src:"images/Image_1.png?1545875232737", id:"Image_1"},
		{src:"images/Image_2.png?1545875232737", id:"Image_2"},
		{src:"images/Image_3.png?1545875232737", id:"Image_3"},
		{src:"images/Image_4.png?1545875232737", id:"Image_4"},
		{src:"images/Image_5.png?1545875232737", id:"Image_5"},
		{src:"images/Image_6.png?1545875232737", id:"Image_6"},
		{src:"images/Image_7.png?1545875232737", id:"Image_7"},
		{src:"images/Image_8.png?1545875232737", id:"Image_8"},
		{src:"images/Image_9.png?1545875232737", id:"Image_9"},
		{src:"images/Image_10.png?1545875232737", id:"Image_10"},
		{src:"images/Image_11.png?1545875232737", id:"Image_11"},
		{src:"images/Image_12.png?1545875232737", id:"Image_12"},
		{src:"images/Image_0.png?1545875232737", id:"Image_0"},
		{src:"images/Image_1_1.png?1545875232737", id:"Image_1_1"},
		{src:"images/Image_1_2.png?1545875232737", id:"Image_1_2"},
		{src:"images/Image_1_3.png?1545875232737", id:"Image_1_3"},
		{src:"images/Image_1_4.png?1545875232737", id:"Image_1_4"},
		{src:"images/Image_1_5.png?1545875232738", id:"Image_1_5"},
		{src:"images/Image_1_6.png?1545875232738", id:"Image_1_6"},
		{src:"images/Image_2_1.png?1545875232738", id:"Image_2_1"},
		{src:"images/Image_2_2.png?1545875232738", id:"Image_2_2"},
		{src:"images/Image_2_3.png?1545875232738", id:"Image_2_3"},
		{src:"images/Image_2_4.png?1545875232738", id:"Image_2_4"},
		{src:"sounds/menu.mp3?1545875232738", id:"menu"},
		{src:"sounds/click.mp3?1545875232738", id:"click"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;